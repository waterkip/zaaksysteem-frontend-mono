import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
//@ts-ignore
import Loader from '@mintlab/ui/App/Zaaksysteem/Loader';
import { ObjectTypeType } from '../../types/ObjectType.types';
import { useObjectTypeEditStyle } from './ObjectTypeEdit.style';

export type ObjectTypeEditPropsType = {
  objectType?: ObjectTypeType;
  loading: boolean;
  uuid: string;
  fetchObjectType: (uuid: string) => void;
};

const ObjectTypeEdit: React.ComponentType<ObjectTypeEditPropsType> = ({
  objectType,
  loading,
  uuid,
  fetchObjectType,
}) => {
  const [t] = useTranslation('objectTypeManagement');
  const classes = useObjectTypeEditStyle();

  useEffect(() => {
    fetchObjectType(uuid);
  }, []);

  if (loading || !objectType) {
    return <Loader delay={200} />;
  }

  return (
    <div className={classes.wrapper}>{`${t('objectType')}: ${
      objectType.uuid
    }`}</div>
  );
};

export default ObjectTypeEdit;
