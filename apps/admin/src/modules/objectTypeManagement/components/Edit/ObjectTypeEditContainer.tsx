import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AJAX_STATE_PENDING } from '../../../../library/redux/ajax/createAjaxConstants';
import { fetchObjectTypeAction } from '../../store/objectType/objectType.actions';
import { ObjectTypeManagementRootStateType } from '../../store/objectTypeManagement.reducer';
import ObjectTypeEdit, { ObjectTypeEditPropsType } from './ObjectTypeEdit';

type PropsFromStateType = Pick<
  ObjectTypeEditPropsType,
  'objectType' | 'loading'
>;

type mapStateToPropsType = ObjectTypeManagementRootStateType;

const mapStateToProps = ({
  objectTypeManagement: {
    objectType: { objectType, state },
  },
}: mapStateToPropsType): PropsFromStateType => ({
  objectType,
  loading: state === AJAX_STATE_PENDING,
});

type PropsFromDispatchType = Pick<ObjectTypeEditPropsType, 'fetchObjectType'>;

const mapDispatchToProps = (dispatch: Dispatch): PropsFromDispatchType => ({
  fetchObjectType: uuid => dispatch(fetchObjectTypeAction(uuid) as any),
});

const ObjectTypeEditContainer = connect<
  PropsFromStateType,
  PropsFromDispatchType,
  {},
  mapStateToPropsType
>(
  mapStateToProps,
  mapDispatchToProps
)(ObjectTypeEdit);

export default ObjectTypeEditContainer;
