import i18next from 'i18next';
import {
  CHECKBOX_GROUP,
  TEXTAREA,
} from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { FormDefinitionStep } from '@zaaksysteem/common/src/components/form/types';
import { ObjectTypeFormShapeType } from '../../ObjectTypeForm.types';

export function getStep5(
  t: i18next.TFunction
): FormDefinitionStep<ObjectTypeFormShapeType> {
  return {
    title: t('objectTypeManagement:form.steps.5.title'),
    fields: [
      {
        name: 'components_changed',
        type: CHECKBOX_GROUP,
        value: [],
        applyBackgroundColor: false,
        choices: [
          {
            label: t(
              'objectTypeManagement:form.fields.components_changed.choices.generic'
            ),
            value: 'generic',
          },
          {
            label: t(
              'objectTypeManagement:form.fields.components_changed.choices.attributes'
            ),
            value: 'attributes',
          },
          {
            label: t(
              'objectTypeManagement:form.fields.components_changed.choices.relations'
            ),
            value: 'relations',
          },
          {
            label: t(
              'objectTypeManagement:form.fields.components_changed.choices.rights'
            ),
            value: 'rights',
          },
        ],
        required: true,
        label: t('objectTypeManagement:form.fields.components_changed.label'),
      },
      {
        name: 'changes',
        type: TEXTAREA,
        isMultiline: true,
        value: '',
        required: true,
        label: t('objectTypeManagement:form.fields.changes.label'),
        help: t('objectTypeManagement:form.fields.changes.help'),
        placeholder: t('objectTypeManagement:form.fields.changes.placeholder'),
      },
    ],
  };
}

export default getStep5;
