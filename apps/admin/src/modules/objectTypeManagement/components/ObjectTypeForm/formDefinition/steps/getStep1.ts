import i18next from 'i18next';
import { TEXT } from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { FormDefinitionStep } from '@zaaksysteem/common/src/components/form/types';
import { ObjectTypeFormShapeType } from '../../ObjectTypeForm.types';

export function getStep1(
  t: i18next.TFunction
): FormDefinitionStep<ObjectTypeFormShapeType> {
  return {
    title: t('objectTypeManagement:form.steps.1.title'),
    description: t('objectTypeManagement:form.steps.1.description'),
    fields: [
      {
        name: 'name',
        type: TEXT,
        value: '',
        required: true,
        label: t('objectTypeManagement:form.fields.name.label'),
        help: t('objectTypeManagement:form.fields.name.help'),
        placeholder: t('objectTypeManagement:form.fields.name.placeholder'),
      },
      {
        name: 'title',
        type: TEXT,
        value: '',
        required: true,
        label: t('objectTypeManagement:form.fields.title.label'),
        help: t('objectTypeManagement:form.fields.title.help'),
        placeholder: t('objectTypeManagement:form.fields.title.placeholder'),
      },
      {
        name: 'external_reference',
        type: TEXT,
        value: '',
        label: t('objectTypeManagement:form.fields.external_reference.label'),
        help: t('objectTypeManagement:form.fields.external_reference.help'),
        placeholder: t(
          'objectTypeManagement:form.fields.external_reference.placeholder'
        ),
      },
    ],
  };
}

export default getStep1;
