import React from 'react';
import { useTranslation } from 'react-i18next';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import { FormValuesType } from '@zaaksysteem/common/src/components/form/types';
import { getObjectTypeFormDefinition } from './formDefinition/ObjectTypeForm.formDefinition';
import { useObjectTypeFormStyles } from './ObjectTypeForm.style';
import ObjectTypeStepControls from './ObjectTypeStepControls/ObjectTypeStepControls';
import { ObjectTypeFormShapeType } from './ObjectTypeForm.types';
import { ObjectTypeProgressBar } from './ObjectTypeProgressBar/ObjectTypeProgressBar';
import { ObjectTypeFormStep } from './ObjectTypeFormStep/ObjectTypeFormStep';

export type ObjectTypeFormPropsType = {
  busy?: boolean;
  onChange?: (values: FormValuesType<ObjectTypeFormShapeType>) => {};
  onSubmit: (values: FormValuesType<ObjectTypeFormShapeType>) => void;
};

export const ObjectTypeForm: React.ComponentType<ObjectTypeFormPropsType> = ({
  onSubmit,
  onChange,
  busy = false,
}) => {
  const [t] = useTranslation('objectTypeManagement');
  const classes = useObjectTypeFormStyles();
  const formDefinition = getObjectTypeFormDefinition({ t });
  const {
    fields,
    activeStep,
    activeStepValid,
    handleNextStep,
    handlePreviousStep,
    hasNextStep,
    hasPreviousStep,
    steps,
    formik: { submitForm },
  } = useForm<ObjectTypeFormShapeType>({
    formDefinition,
    onSubmit,
    onChange,
  });

  return (
    <div className={classes.wrapper}>
      <div className={classes.formWrapper}>
        <div className={classes.innerWrapper}>
          <div>
            <ObjectTypeProgressBar steps={steps} />
          </div>

          <ObjectTypeFormStep step={activeStep} fields={fields} />
        </div>
      </div>

      <div className={classes.buttonWrapper}>
        <div className={classes.innerWrapper}>
          <ObjectTypeStepControls
            activeStepValid={activeStepValid}
            handleNextStep={handleNextStep}
            handlePreviousStep={handlePreviousStep}
            hasNextStep={hasNextStep}
            hasPreviousStep={hasPreviousStep}
            submitForm={submitForm}
            busy={busy}
          />
        </div>
      </div>
    </div>
  );
};

export default ObjectTypeForm;
