import { ObjectTypeManagementRootStateType } from '../objectTypeManagement.reducer';
import { CreateStateType } from '../create/create.reducer';

export const createStateSelector = (
  state: ObjectTypeManagementRootStateType
): CreateStateType => state.objectTypeManagement.create;
