import { combineReducers } from 'redux';
import objectType, {
  ObjectTypeStateType,
} from './objectType/objectType.reducer';
import { create, CreateStateType } from './create/create.reducer';
import {
  unsavedChanges,
  UnsavedChangesStateType,
} from './unsavedChanges/unsavedChanges.reducer';

interface ObjectTypeManagementStateType {
  objectType: ObjectTypeStateType;
  create: CreateStateType;
  unsavedChanges: UnsavedChangesStateType;
}

export interface ObjectTypeManagementRootStateType {
  objectTypeManagement: ObjectTypeManagementStateType;
}

export const objectTypeManagement = combineReducers<
  ObjectTypeManagementStateType
>({
  objectType,
  create,
  unsavedChanges,
});

export default objectTypeManagement;
