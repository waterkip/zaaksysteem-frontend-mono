import { Reducer } from 'redux';
import {
  AJAX_STATE_INIT,
  AjaxState,
} from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import { handleAjaxStateChange } from '@zaaksysteem/common/src/library/redux/ajax/handleAjaxStateChange';
import { AjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { ObjectTypeType } from '../../types/ObjectType.types';
import { OBJECT_TYPE_FETCH } from './objectType.constants';

export interface ObjectTypeStateType {
  state: AjaxState;
  objectType?: ObjectTypeType;
}

const initialState: ObjectTypeStateType = {
  state: AJAX_STATE_INIT,
};

const handleFetchSuccess = (
  state: ObjectTypeStateType,
  action: AjaxAction<any>
): ObjectTypeStateType => {
  const { response } = action.payload;

  if (!response) {
    return state;
  }

  const objectType = response.result[0];
  const { id } = objectType;

  return {
    ...state,
    objectType: {
      uuid: id,
    },
  };
};

export const objectType: Reducer<ObjectTypeStateType, AjaxAction> = (
  state = initialState,
  action
) => {
  const { type } = action;
  const handleFetchObjectTypeStateChange = handleAjaxStateChange(
    OBJECT_TYPE_FETCH
  );

  switch (type) {
    case OBJECT_TYPE_FETCH.PENDING:
    case OBJECT_TYPE_FETCH.ERROR:
      return handleFetchObjectTypeStateChange(state, action);
    case OBJECT_TYPE_FETCH.SUCCESS:
      return handleFetchObjectTypeStateChange(
        handleFetchSuccess(state, action as AjaxAction<any>),
        action
      );
    default:
      return state;
  }
};

export default objectType;
