import { createAjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { OBJECT_TYPE_FETCH } from './objectType.constants';

const fetchObjectTypeAjaxAction = createAjaxAction(OBJECT_TYPE_FETCH);

export const fetchObjectTypeAction = (uuid: string) => {
  return fetchObjectTypeAjaxAction({
    url: `/api/object/${uuid}`,
    method: 'GET',
  });
};
