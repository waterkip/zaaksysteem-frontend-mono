//@ts-ignore
import uuidv4 from 'uuid/v4';
import { createAjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { APICaseManagement } from '@zaaksysteem/generated';
import { ObjectTypeFormShapeType } from '../../components/ObjectTypeForm/ObjectTypeForm.types';
import { OBJECT_TYPE_CREATE } from './create.constants';

const createObjectTypeAjaxAction = createAjaxAction(OBJECT_TYPE_CREATE);

export type CreateObjectTypePayloadType = ObjectTypeFormShapeType & {
  folderUuid?: string;
};

export const createObjectTypeAction = (
  payload: CreateObjectTypePayloadType
) => {
  const data: APICaseManagement.CreateCustomObjectTypeRequestBody = {
    name: payload.name,
    uuid: uuidv4(),
    title: payload.title,
    status: 'active',
  };

  return createObjectTypeAjaxAction({
    url: `/api/v2/cm/custom_object_type/create_custom_object_type`,
    method: 'POST',
    data,
    payload,
  });
};
