import { objectTypeManagement } from './objectTypeManagement.reducer';
import { objectTypeManagementMiddleware } from './objectTypeManagement.middleware';

export function getObjectTypeManagementModule() {
  return {
    id: 'objectType',
    reducerMap: {
      objectTypeManagement,
    },
    middlewares: [objectTypeManagementMiddleware],
  };
}
