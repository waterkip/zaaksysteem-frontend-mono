import { RouterRootState } from 'connected-react-router';
import { Middleware } from 'redux';
import { APICaseManagement } from '@zaaksysteem/generated';
import { MiddlewareHelper } from '@zaaksysteem/common/src/types/MiddlewareHelper';
import { AjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { invoke } from '../../../store/route/route.actions';
import { ROUTE_INVOKE } from '../../../store/route/route.constants';
import { showDialog } from '../../../store/ui/ui.actions';
import { DIALOG_DISCARD_CHANGES } from '../../../constants/dialogs.constants';
import { ObjectTypeManagementRootStateType } from './objectTypeManagement.reducer';
import { OBJECT_TYPE_CREATE } from './create/create.constants';
import { CreateObjectTypePayloadType } from './create/create.actions';

const handleObjectTypeCreateSuccess: MiddlewareHelper<
  ObjectTypeManagementRootStateType,
  AjaxAction<
    APICaseManagement.CreateCustomObjectTypeResponseBody,
    CreateObjectTypePayloadType
  >
> = (store, next, action) => {
  next(action);

  const { folderUuid = '' } = action.payload;

  store.dispatch(invoke({ path: `/admin/catalogus/${folderUuid}` }));
};

const handleRouteInvoke: MiddlewareHelper<ObjectTypeManagementRootStateType> = (
  store,
  next,
  action
) => {
  const {
    objectTypeManagement: { unsavedChanges },
  } = store.getState();
  const {
    payload: { force },
  } = action as any;

  if (!force && unsavedChanges) {
    store.dispatch(
      showDialog({
        dialogType: DIALOG_DISCARD_CHANGES,
        options: (action as any).payload,
      })
    );

    return next({ ...action, cancel: true });
  }

  return next(action);
};

export const objectTypeManagementMiddleware: Middleware<
  {},
  ObjectTypeManagementRootStateType & RouterRootState
> = store => next => action => {
  switch (action.type) {
    case OBJECT_TYPE_CREATE.SUCCESS:
      return handleObjectTypeCreateSuccess(store, next, action);

    case ROUTE_INVOKE:
      return handleRouteInvoke(store, next, action);

    default:
      return next(action);
  }
};

export default objectTypeManagementMiddleware;
