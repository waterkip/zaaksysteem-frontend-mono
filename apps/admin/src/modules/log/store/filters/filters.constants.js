import { createAjaxConstants } from '../../../../library/redux/ajax/createAjaxConstants';

export const FILTERS_APPLY = 'LOG:FILTERS_APPLY';
export const FILTERS_FETCH_USER_LABEL = createAjaxConstants(
  'LOG:FILTERS_FETCH_USER_LABEL'
);
