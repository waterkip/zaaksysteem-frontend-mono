import { TEXT } from '@zaaksysteem/common/src/components/form/constants/fieldTypes';

const formDefinition = [
  {
    name: 'reason',
    type: TEXT,
    value: '',
    required: true,
    label: 'changeOnlineStatus:fields.reason.label',
    placeholder: 'changeOnlineStatus:fields.reason.placeholder',
  },
];

export default formDefinition;
