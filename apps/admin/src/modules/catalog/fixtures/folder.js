import { TEXT } from '@zaaksysteem/common/src/components/form/constants/fieldTypes';

const formDefinition = [
  {
    name: 'name',
    type: TEXT,
    value: '',
    required: true,
    label: 'folder:fields.name.label',
    placeholder: 'folder:fields.name.label',
  },
];

export default formDefinition;
