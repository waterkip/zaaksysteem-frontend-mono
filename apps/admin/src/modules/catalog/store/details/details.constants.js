import { createAjaxConstants } from '../../../../library/redux/ajax/createAjaxConstants';

export const CATALOG_FETCH_DETAILS = createAjaxConstants(
  'CATALOG_FETCH_DETAILS'
);
export const CATALOG_TOGGLE_DETAIL_VIEW = 'CATALOG_TOGGLE_DETAIL_VIEW';
