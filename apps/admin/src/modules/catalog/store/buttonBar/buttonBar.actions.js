import { CATALOG_DUPLICATE_CASETYPE } from './buttonBar.constants';
import { CATALOG_EDIT_CASETYPE } from './buttonBar.constants';
import { CATALOG_EDIT_OBJECTTYPE } from './buttonBar.constants';
import { CATALOG_EXPORT_CASETYPE } from './buttonBar.constants';

export const catalogDuplicateCaseType = () => ({
  type: CATALOG_DUPLICATE_CASETYPE,
});

export const catalogEditCaseType = () => ({
  type: CATALOG_EDIT_CASETYPE,
});

export const catalogEditObjectType = () => ({
  type: CATALOG_EDIT_OBJECTTYPE,
});

export const catalogExportCaseType = () => ({
  type: CATALOG_EXPORT_CASETYPE,
});
