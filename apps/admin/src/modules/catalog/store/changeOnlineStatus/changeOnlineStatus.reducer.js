import { handleAjaxStateChange } from '../../../../library/redux/ajax/handleAjaxStateChange';
import { AJAX_STATE_INIT } from '../../../../library/redux/ajax/createAjaxConstants';
import { CATALOG_CHANGE_ONLINE_STATUS } from './changeOnlineStatus.constants';

export const initialState = {
  state: AJAX_STATE_INIT,
};

export function changeOnlineStatus(state = initialState, action) {
  const handleAjaxState = handleAjaxStateChange(CATALOG_CHANGE_ONLINE_STATUS);

  switch (action.type) {
    case CATALOG_CHANGE_ONLINE_STATUS.PENDING:
    case CATALOG_CHANGE_ONLINE_STATUS.ERROR:
    case CATALOG_CHANGE_ONLINE_STATUS.SUCCESS:
      return handleAjaxState(state, action);

    default:
      return state;
  }
}

export default changeOnlineStatus;
