/**
 * @param {Object} theme
 * @return {JSS}
 */
export const optionStylesheet = ({
  palette,
  mintlab: { greyscale, shadows },
}) => ({
  wrapper: {
    background: palette.common.white,
    padding: '6px',
    display: 'flex',
    alignItems: 'center',
    borderRadius: '4px',
    marginBottom: '16px',
    boxShadow: shadows.flat,
  },
  dragging: {
    backgroundColor: greyscale.dark,
  },
  handle: {
    color: greyscale.darker,
  },
  delete: {
    color: greyscale.darkest,
  },
  label: {
    flex: 1,
    marginLeft: '12px',
    overflow: 'hidden',
  },
});
