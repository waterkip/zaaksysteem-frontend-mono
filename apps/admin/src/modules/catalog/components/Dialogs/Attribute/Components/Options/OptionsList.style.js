/**
 * @param {Object} theme
 * @return {JSS}
 */
export const optionsListStylesheet = ({ palette: { primary } }) => ({
  list: {
    borderRadius: '5px',
    transition: 'background-color 0.3s ease',
  },
  draggingOver: {
    backgroundColor: primary.lightest,
  },
  hidden: {
    display: 'none',
  },
});
