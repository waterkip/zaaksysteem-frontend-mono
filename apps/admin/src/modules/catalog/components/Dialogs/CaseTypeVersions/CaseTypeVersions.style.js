/**
 * @param {Object} theme
 * @return {JSS}
 */
export const caseTypeVersionsStyleSheet = () => ({
  dialogContent: {
    height: '600px',
    width: '500px',
    overflow: 'scroll',
    overflowX: 'hidden',
    padding: '0',
  },
});
