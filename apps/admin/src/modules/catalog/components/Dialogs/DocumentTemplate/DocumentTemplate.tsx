import React from 'react';
import { FormikValues } from 'formik';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import { FormDefinitionField } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import {
  showFields,
  hideFields,
  setRequired,
  setOptional,
  updateField,
  Rule,
} from '../../../../../components/Form/Rules';

type IntegrationType = {
  label: string;
  module: string;
  value: string;
};

const DocumentTemplate: React.FunctionComponent<any> = ({
  saveAction,
  formDefinition,
  id,
  t,
  saving,
  initializing,
  hide,
}) => {
  const handleOnSubmit = (values: FormikValues) => saveAction({ values });

  const matchesIntegration = (module: string) => (
    value: string,
    field: FormDefinitionField & { integrations: IntegrationType[] }
  ) => {
    const integration = field.integrations.find(
      thisIntegration => thisIntegration.value === value
    );
    return integration && integration.module === module;
  };

  const rules: any[] = [
    new Rule('integration_uuid')
      .when((value: string) => value !== 'default')
      .then(showFields(['integration_reference']))
      .then(setRequired(['integration_reference']))
      .else(hideFields(['integration_reference']))
      .else(setOptional(['integration_reference'])),
    new Rule('integration_uuid')
      .when((value: string) => value === 'default')
      .then(showFields(['file']))
      .else(hideFields(['file'])),
    new Rule('integration_uuid').when(matchesIntegration('xential')).then(
      updateField('integration_reference', {
        label: t('documentTemplate:fields.templateExternalName.labelXential'),
      })
    ),
    new Rule('integration_uuid').when(matchesIntegration('stuf_dcr')).then(
      updateField('integration_reference', {
        label: t('documentTemplate:fields.templateExternalName.labelStufDCR'),
      })
    ),
  ];

  const title = t('documentTemplate:dialog.title', {
    action: id ? t('common:edit') : t('common:create'),
  });

  return (
    <FormDialog
      formDefinition={formDefinition}
      onSubmit={handleOnSubmit}
      title={title}
      scope="catalog-document-template-dialog"
      rules={rules}
      icon="insert_drive_file"
      isInitialValid={id ? true : false}
      initializing={initializing}
      saving={saving}
      onClose={hide}
    />
  );
};

export default DocumentTemplate;
