import React from 'react';
import { asArray } from '@mintlab/kitchen-sink/source/array';

/**
 * @reactProps {string|array} value
 * @return {ReactElement}
 */
const Default = ({ value }) => <span>{asArray(value).join(', ')}</span>;

export default Default;
