import React from 'react';
import { withStyles } from '@material-ui/styles';
import Button from '@mintlab/ui/App/Material/Button';
import { H3 } from '@mintlab/ui/App/Material/Typography';
import Loader from '@mintlab/ui/App/Zaaksysteem/Loader';
import Render from '@mintlab/ui/App/Abstract/Render';
import ColoredIcon from '../../../../components/Icons/ColoredIcon';
import { detailViewStyleSheet } from './DetailView.style';
import Details from './Details/Details';

/**
 * @reactProps {Object} classes
 * @reactProps {Function} closeDetailView
 * @reactProps {Object} detailInfo
 * @reactProps {string} detailInfo.icon
 * @reactProps {string} detailInfo.name
 * @reactProps {string} detailInfo.type
 * @reactProps {Object} detailInfo.details
 * @reactProps {string} detailInfo.version
 * @reactProps {boolean} loading
 * @reactProps {Function} t
 * @reactProps {Function} doNavigate
 * @return {ReactElement}
 */
const DetailView = ({
  classes,
  toggleDetailView,
  detailInfo: { icon, name, type, details, version },
  loading,
  t,
  doNavigate,
}) => (
  <div className={classes.detailView}>
    <div className={classes.header}>
      <div className={classes.headerIcon}>
        <ColoredIcon size="large" value={icon} />
      </div>
      <H3
        classes={{
          root: classes.title,
        }}
      >
        {name}
      </H3>
      <div>
        <Button
          action={toggleDetailView}
          presets={['icon', 'large']}
          scope="catalog-detail:close"
        >
          close
        </Button>
      </div>
    </div>
    <Render condition={type === 'case_type' && version}>
      <div className={classes.subHeader}>
        <div className={classes.version}>
          {`${t('catalog:detailView:versionTitle')} ${version}`}
        </div>
      </div>
    </Render>
    <div className={classes.content}>
      {loading ? (
        <Loader />
      ) : (
        <Render condition={details}>
          <Details
            details={details}
            t={t}
            doNavigate={doNavigate}
            type={type}
          />
        </Render>
      )}
    </div>
  </div>
);

export default withStyles(detailViewStyleSheet)(DetailView);
