import React from 'react';
import { DynamicModuleLoader } from 'redux-dynamic-modules-react';
import { getCatalogModule } from './store/catalog.module';
import CatalogContainer from './components/CatalogContainer';

type CatalogRouteType = {
  segments: [string];
};

const CatalogModule: React.ComponentType<CatalogRouteType> = ({ segments }) => {
  const [id] = segments;

  return (
    <DynamicModuleLoader modules={[getCatalogModule(id)]}>
      <CatalogContainer />
    </DynamicModuleLoader>
  );
};

export default CatalogModule;
