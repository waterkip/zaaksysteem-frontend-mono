export const caseTypeVersions = {
  caseTypeVersions: {
    dialog: {
      title: 'Versiebeheer',
      titleActivate: 'Versie {{version}} activeren',
      activate: 'Activeren',
    },
    fields: {
      reason: {
        label: 'Reden',
      },
    },
    details: {
      display_name: 'Gebruiker',
      modified_components: 'Aangepast',
      change_note: 'Opmerkingen',
    },
  },
};
