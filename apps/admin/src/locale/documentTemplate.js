export const documentTemplate = {
  documentTemplate: {
    dialog: {
      title: 'Sjabloon {{action}}',
    },
    fields: {
      name: {
        label: 'Naam sjabloon',
      },
      integration_uuid: {
        label: 'Type sjabloon',
        defaultTypeLabel: 'Zaaksysteem',
      },
      help: {
        label: 'Toelichting',
      },
      commit_message: {
        label: 'Opmerking',
        help: 'Korte omschrijving van de reden van aanmaak / wijziging.',
      },
      file: {
        label: 'Sjabloon',
        help: 'Geldige bestandsformaten: .ODT',
      },
      templateExternalName: {
        labelXential: 'UUID van het sjabloon',
        labelStufDCR: 'Naam van het sjabloon in DCR',
      },
    },
  },
};
