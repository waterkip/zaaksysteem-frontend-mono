/**
 * @param {Object} theme
 * @return {JSS}
 */
export const dialogRendererStylesheet = () => ({
  paper: {
    width: '600px',
  },
});
