/**
 * @param {Object} theme
 * @return {JSS}
 */
export const sharedStylesheet = ({ mintlab: { greyscale, radius } }) => ({
  sheet: {
    border: `2px solid ${greyscale.light}`,
    'background-color': greyscale.light,
    'border-top-left-radius': radius.sheet,
    'border-top-right-radius': radius.sheet,
    overflowY: 'scroll',
  },
});
