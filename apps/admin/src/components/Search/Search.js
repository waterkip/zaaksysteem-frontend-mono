import React, { useState, useRef, useEffect } from 'react';
import { withStyles } from '@material-ui/styles';
import { NewGenericTextField } from '@mintlab/ui/App/Material/TextField';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import Render from '@mintlab/ui/App/Abstract/Render';
import Icon from '@mintlab/ui/App/Material/Icon';
import FormRenderer from '@zaaksysteem/common/src/components/form/FormRenderer';
import { cloneWithout, callOrNothingAtAll } from '@mintlab/kitchen-sink/source';
import {
  queryToObject,
  objectToQuery,
} from '../../modules/catalog/library/searchQuery';
import { searchStyleSheet } from './Search.style';

const renderField = ({ FieldComponent, name, ...rest }) => {
  const props = {
    ...cloneWithout(rest, 'type', 'mode'),
    compact: true,
    name,
    key: `attribute-form-component-${name}`,
    scope: `attribute-form-component-${name}`,
  };

  return (
    <FormControlWrapper {...props}>
      <FieldComponent {...props} />
    </FormControlWrapper>
  );
};

const Search = ({
  query = '',
  formDefinition = [],
  rules,
  classes,
  placeholder,
  onClear,
  onSearch,
  scope,
}) => {
  const [value, setValue] = useState(query);
  const [open] = useState(false);
  const previousQuery = useRef();
  const queryObject = queryToObject(value);
  const formDefinitionWithValues = formDefinition.map(item =>
    queryObject[item.name] ? { ...item, value: queryObject[item.name] } : item
  );
  const clearValue = () => {
    setValue('');
    callOrNothingAtAll(onClear);
  };
  const handleChange = event => setValue(event.target.value);
  const handleKeyPress = event => {
    const { key } = event;

    if (key.toLowerCase() === 'enter' && value && value.length > 0) {
      callOrNothingAtAll(onSearch, [value]);
    }
  };
  const icon = (
    <div className={classes.icon}>
      <Icon size="small" color="inherit">
        search
      </Icon>
    </div>
  );
  const closeAction = value || query ? clearValue : null;

  useEffect(() => {
    if (previousQuery.current !== query) {
      setValue(query);
    }

    previousQuery.current = query;
  });

  return (
    <div className={classes.wrapper}>
      <NewGenericTextField
        disabled={open}
        value={value}
        onChange={handleChange}
        placeholder={placeholder}
        onKeyPress={handleKeyPress}
        startAdornment={icon}
        closeAction={closeAction}
        scope={scope}
      />
      <Render condition={formDefinition.length > 0}>
        {open && (
          <FormRenderer formDefinition={formDefinitionWithValues} rules={rules}>
            {({ fields, values }) => {
              setValue(objectToQuery(values));
              return <React.Fragment>{fields.map(renderField)}</React.Fragment>;
            }}
          </FormRenderer>
        )}
      </Render>
    </div>
  );
};

export default withStyles(searchStyleSheet)(Search);
