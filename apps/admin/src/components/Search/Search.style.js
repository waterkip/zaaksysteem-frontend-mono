/**
 * @param {Object} theme
 * @return {JSS}
 */
export const searchStyleSheet = ({ mintlab: { greyscale } }) => ({
  wrapper: {
    maxWidth: 700,
    margin: '0 auto',
  },
  icon: {
    color: greyscale.evenDarker,
  },
});
