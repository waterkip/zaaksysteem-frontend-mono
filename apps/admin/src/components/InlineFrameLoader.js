import React from 'react';
import Loader from '@mintlab/ui/App/Zaaksysteem/Loader';
import Render from '@mintlab/ui/App/Abstract/Render';
import { extract } from '@mintlab/kitchen-sink/source';
import InlineFrame from './InlineFrame';
import styleSheet from './InlineFrameLoader.module.css';

/**
 * A loader component for {@link InlineFrame}.
 *
 * @param {Object} props
 * @return {ReactElement}
 */
export const InlineFrameLoader = props => {
  const [loading, iframeProps] = extract('loading', props);
  const visibility = loading ? 'hidden' : '';

  // ZS-INFO: The `InlineFrame` component cannot be a child
  // of the `Loader` component because it needs to be rendered
  // first in order to set the store's `loading` property.
  return (
    <div className={styleSheet.placeHolder}>
      <Render condition={loading}>
        <div className={styleSheet.loader}>
          <Loader active={true} />
        </div>
      </Render>
      <div
        className={styleSheet.wrapper}
        style={{
          visibility,
        }}
      >
        <InlineFrame {...iframeProps} />
      </div>
    </div>
  );
};
