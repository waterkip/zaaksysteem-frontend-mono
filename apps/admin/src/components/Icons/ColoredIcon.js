import React from 'react';
import { withStyles } from '@material-ui/styles';
import Icon from '@mintlab/ui/App/Material/Icon';
import { coloredIconStyleSheet } from './ColoredIcon.style';

const iconNames = {
  folder: 'folder',
  case_type: 'ballot',
  object_type: 'layers',
  custom_object_type: 'layers',
  attribute: 'extension',
  email_template: 'mail_outline',
  document_template: 'insert_drive_file',
  select_all: 'select_all',
};

const getIconName = type => iconNames[type] || 'error';

/**
 * @reactProps {Object} classes
 * @reactProps {string} value
 * @reactProps {string} size
 * @return {ReactElement}
 */
export const ColoredIcon = ({ classes, value, size }) => (
  <div className={classes[value]}>
    <Icon
      classes={{
        root: classes.icon,
      }}
      size={size}
    >
      {getIconName(value)}
    </Icon>
  </div>
);

export default withStyles(coloredIconStyleSheet)(ColoredIcon);
