/* eslint-disable valid-jsdoc */
import { request } from '../../fetch';

/**
 * @param {Object} response
 * @return {Promise}
 */
const parseResponseBody = response => {
  let clone = response.clone();

  return response
    .json()
    .then(body => body)
    .catch(() => clone.text().then(body => body));
};

/**
 * @param {{PENDING: string, ERROR: string, SUCCESS: string}} constants
 * @returns {(options: { method: string, url: string, data: Object, payload: Object, headers: Object})
 *  => (dispatch: Function)
 *    => Promise
 * }
 */
export const createAjaxAction = constants => ({
  method,
  url,
  data,
  payload,
  headers,
}) => dispatch => {
  dispatch({
    type: constants.PENDING,
    ajax: true,
    payload: {
      ...payload,
    },
  });

  const merge = body => ({
    ajax: true,
    payload: {
      ...payload,
      response: body,
    },
  });

  return request(method, url, data, headers)
    .then(response => {
      return parseResponseBody(response).then(body => {
        dispatch({
          ...merge(body),
          type: constants.SUCCESS,
        });
      });
    })
    .catch(response => {
      return parseResponseBody(response).then(body => {
        dispatch({
          ...merge(body),
          type: constants.ERROR,
          error: true,
          statusCode: response.status,
        });
      });
    });
};
