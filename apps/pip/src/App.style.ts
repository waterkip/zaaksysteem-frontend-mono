import { makeStyles } from '@material-ui/core';

export const useAppStyle = makeStyles(({ typography }) => ({
  app: {
    fontFamily: typography.fontFamily,
    height: '100%',
  },
}));
