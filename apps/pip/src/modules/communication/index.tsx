import React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import CommunicationModule from '@zaaksysteem/communication-module';

export interface CommunicationRouteParamsType {
  userId: string;
}

export interface CommunicationPropsType
  extends RouteComponentProps<CommunicationRouteParamsType> {}

const Communication: React.ComponentType<CommunicationPropsType> = ({
  match,
}) => (
  <CommunicationModule
    contactUuid={match.params.userId}
    rootPath={match.url}
    context="pip"
    capabilities={{
      allowSplitScreen: false,
      canAddAttachmentToCase: false,
      canAddSourceFileToCase: false,
      canAddThreadToCase: false,
      canCreateMijnOverheid: false,
      canCreateContactMoment: false,
      canCreatePipMessage: true,
      canCreateEmail: false,
      canCreateNote: false,
      canDeleteMessage: false,
      canImportMessage: false,
      canSelectCase: true,
      canSelectContact: false,
      canFilter: false,
      canOpenPDFPreview: false,
    }}
  />
);

export default Communication;
