# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.21.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.21.2...@zaaksysteem/main@0.21.3) (2020-04-07)

**Note:** Version bump only for package @zaaksysteem/main





## [0.21.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.21.1...@zaaksysteem/main@0.21.2) (2020-04-06)

**Note:** Version bump only for package @zaaksysteem/main





## [0.21.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.21.0...@zaaksysteem/main@0.21.1) (2020-04-03)

**Note:** Version bump only for package @zaaksysteem/main





# [0.21.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.20.0...@zaaksysteem/main@0.21.0) (2020-04-02)


### Features

* **Communication:** MINTY-3432 - add attachment PDF preview ([5bebd92](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5bebd92))





# [0.20.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.19.6...@zaaksysteem/main@0.20.0) (2020-04-01)


### Features

* **ObjectView:** Implement first iteration of Object View ([daa2da5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/daa2da5))





## [0.19.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.19.5...@zaaksysteem/main@0.19.6) (2020-04-01)

**Note:** Version bump only for package @zaaksysteem/main





## [0.19.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.19.4...@zaaksysteem/main@0.19.5) (2020-04-01)

**Note:** Version bump only for package @zaaksysteem/main





## [0.19.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.19.3...@zaaksysteem/main@0.19.4) (2020-03-31)

**Note:** Version bump only for package @zaaksysteem/main





## [0.19.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.19.2...@zaaksysteem/main@0.19.3) (2020-03-31)

**Note:** Version bump only for package @zaaksysteem/main





## [0.19.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.19.1...@zaaksysteem/main@0.19.2) (2020-03-30)

**Note:** Version bump only for package @zaaksysteem/main





## [0.19.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.19.0...@zaaksysteem/main@0.19.1) (2020-03-30)

**Note:** Version bump only for package @zaaksysteem/main





# [0.19.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.18.3...@zaaksysteem/main@0.19.0) (2020-03-30)


### Features

* **FormRenderer:** Improve performance and readability of `FormRenderer` component ([f310425](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f310425))
* **FormRenderer:** Refactor formRenderer to hook and update Formik to latest version 2.1.4 ([c5d1e71](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c5d1e71))





## [0.18.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.18.2...@zaaksysteem/main@0.18.3) (2020-03-27)

**Note:** Version bump only for package @zaaksysteem/main





## [0.18.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.18.1...@zaaksysteem/main@0.18.2) (2020-03-20)

**Note:** Version bump only for package @zaaksysteem/main





## [0.18.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.18.0...@zaaksysteem/main@0.18.1) (2020-03-19)

**Note:** Version bump only for package @zaaksysteem/main





# [0.18.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.17.2...@zaaksysteem/main@0.18.0) (2020-03-19)


### Features

* **DocumentExplorer:** MINTY-3300 - add initial PDF preview features, misc. ([a131814](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a131814))
* **DocumentExplorer:** MINTY-3300 - change Loader, exports ([b25d770](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b25d770))
* **DocumentExplorer:** MINTY-3300 - split up DocumentExplorer functionality, create new Intake entrypoint, add Selectable state to table ([5bae0d3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5bae0d3))
* **Intake:** MINTY-3437 - add search component ([2bb171b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/2bb171b))





## [0.17.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.17.1...@zaaksysteem/main@0.17.2) (2020-03-17)

**Note:** Version bump only for package @zaaksysteem/main





## [0.17.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.17.0...@zaaksysteem/main@0.17.1) (2020-03-17)

**Note:** Version bump only for package @zaaksysteem/main





# [0.17.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.16.10...@zaaksysteem/main@0.17.0) (2020-03-05)


### Features

* **CaseDocuments:** add misc features ([8845760](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8845760))
* **Communication:** MINTY-3241 Re-enable adding notes and contactmoments on closed cases ([b21dd23](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b21dd23))
* **DocumentPreview:** MINTY-2960 Add case document preview to dev dashboard ([0f6fb22](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/0f6fb22))





## [0.16.10](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.16.9...@zaaksysteem/main@0.16.10) (2020-03-03)

**Note:** Version bump only for package @zaaksysteem/main





## [0.16.9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.16.8...@zaaksysteem/main@0.16.9) (2020-02-24)

**Note:** Version bump only for package @zaaksysteem/main





## [0.16.8](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.16.7...@zaaksysteem/main@0.16.8) (2020-02-21)

**Note:** Version bump only for package @zaaksysteem/main





## [0.16.7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.16.6...@zaaksysteem/main@0.16.7) (2020-02-17)

**Note:** Version bump only for package @zaaksysteem/main





## [0.16.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.16.5...@zaaksysteem/main@0.16.6) (2020-02-12)

**Note:** Version bump only for package @zaaksysteem/main





## [0.16.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.16.4...@zaaksysteem/main@0.16.5) (2020-02-12)

**Note:** Version bump only for package @zaaksysteem/main





## [0.16.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.16.3...@zaaksysteem/main@0.16.4) (2020-02-12)

**Note:** Version bump only for package @zaaksysteem/main





## [0.16.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.16.2...@zaaksysteem/main@0.16.3) (2020-02-10)

**Note:** Version bump only for package @zaaksysteem/main





## [0.16.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.16.1...@zaaksysteem/main@0.16.2) (2020-02-07)

**Note:** Version bump only for package @zaaksysteem/main

## [0.16.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.16.0...@zaaksysteem/main@0.16.1) (2020-02-06)

**Note:** Version bump only for package @zaaksysteem/main

# [0.16.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.15.10...@zaaksysteem/main@0.16.0) (2020-02-06)

### Features

- **Communication:** MINTY-2962 Extract Pip form and reuse the same form for create and reply ([42848aa](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/42848aa))

## [0.15.10](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.15.9...@zaaksysteem/main@0.15.10) (2020-02-06)

**Note:** Version bump only for package @zaaksysteem/main

## [0.15.9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.15.8...@zaaksysteem/main@0.15.9) (2020-01-30)

**Note:** Version bump only for package @zaaksysteem/main

## [0.15.8](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.15.7...@zaaksysteem/main@0.15.8) (2020-01-30)

**Note:** Version bump only for package @zaaksysteem/main

## [0.15.7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.15.6...@zaaksysteem/main@0.15.7) (2020-01-23)

**Note:** Version bump only for package @zaaksysteem/main

## [0.15.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.15.5...@zaaksysteem/main@0.15.6) (2020-01-23)

**Note:** Version bump only for package @zaaksysteem/main

## [0.15.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.15.4...@zaaksysteem/main@0.15.5) (2020-01-22)

**Note:** Version bump only for package @zaaksysteem/main

## [0.15.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.15.3...@zaaksysteem/main@0.15.4) (2020-01-14)

**Note:** Version bump only for package @zaaksysteem/main

## [0.15.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.15.1...@zaaksysteem/main@0.15.3) (2020-01-14)

**Note:** Version bump only for package @zaaksysteem/main

## [0.15.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.15.1...@zaaksysteem/main@0.15.2) (2020-01-13)

**Note:** Version bump only for package @zaaksysteem/main

## [0.15.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.15.0...@zaaksysteem/main@0.15.1) (2020-01-13)

**Note:** Version bump only for package @zaaksysteem/main

# [0.15.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.14.0...@zaaksysteem/main@0.15.0) (2020-01-10)

### Features

- **Communication:** MINTY-2665 Use `ContactFinder` component to search for email recipients ([c0a4ad6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c0a4ad6))

# [0.14.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.13.0...@zaaksysteem/main@0.14.0) (2020-01-09)

### Bug Fixes

- (CaseManagement) truncate task title ([c5d397b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c5d397b))
- **Tasks:** MINTY-2769 - fix redirect logic in middleware ([1d09b68](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/1d09b68))
- **Tasks:** MINTY-2816 - change get_task_list endpoint to filter ([f127e36](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f127e36))
- **Tasks:** MINTY-2822 - fix displayname + typing issues ([6f5c722](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6f5c722))

### Features

- **CaseManagement:** add task creator ([939fbbb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/939fbbb))
- **ContactFinder:** MINTY-2618 - add type filter support for ContactFinder ([d837b4b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/d837b4b))
- **DatePicker:** MINTY-2601 - add onClose ability to DatePicker ([732401e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/732401e))
- **Tasks:** add misc. style/UX improvements ([b619af4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b619af4))
- **Tasks:** Add the basic setup for the task module ([ecc9d4c](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ecc9d4c))
- **Tasks:** MINTY-1575 - add basic form fields, DatePicker ([07ed5e5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/07ed5e5))
- **Tasks:** MINTY-1575 - add edit, delete, unlock functionality ([8dca70b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8dca70b))
- **Tasks:** MINTY-1575 - fix build/import problems ([23c0700](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/23c0700))
- **Tasks:** MINTY-2672 - call postmessage for every action ([99bd8fb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/99bd8fb))
- **Tasks:** MINTY-2695 - cancel adding of task on blur ([99f96fb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/99f96fb))
- **Tasks:** MINTY-2695 - process some MR feedback ([8bd89cc](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8bd89cc))
- **Tasks:** MINTY-2695 - rewrite internal logic to immediate responding to user actions ([c0f74c2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c0f74c2))

# [0.13.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.12.6...@zaaksysteem/main@0.13.0) (2020-01-09)

### Bug Fixes

- **Communication:** MINTY-2262 - allow e-mail addresses over multiple lines ([db44093](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/db44093))
- **UI:** Fix aria-labels on snackbar and dialogs ([775a287](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/775a287))

### Features

- **Case:** Allow status of case to determine capabilities of communication module ([1a7a781](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/1a7a781))
- **Communication:** MINTY-1798 Add `canImportMessage` capability ([c10c88e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c10c88e))
- **Communication:** MINTY-1925 Make deleting a message controllable via a capability ([704ee17](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/704ee17))
- **Communication:** MINTY-2034 Call `/get_thread_list` with correct params according to context of communication-module ([7fef0f2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7fef0f2))
- **i18n:** Move common locale namespace to `@zaaksysteem/common` package ([4a36e92](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4a36e92))
- **Message:** Allow source files of messages to be added to the case documents ([6819f60](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6819f60))
- **Message:** Make deletion of messages depended on case status ([c1968df](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c1968df))
- **Thread:** Allow actions on case-messages to be performed outside of case-context ([2608cbc](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/2608cbc))
- **Thread:** Allow threads to be linked to cases from main view ([44a8a51](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/44a8a51))

## [0.12.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.12.5...@zaaksysteem/main@0.12.6) (2019-12-19)

**Note:** Version bump only for package @zaaksysteem/main

## [0.12.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.12.4...@zaaksysteem/main@0.12.5) (2019-11-21)

**Note:** Version bump only for package @zaaksysteem/main

## [0.12.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.12.3...@zaaksysteem/main@0.12.4) (2019-10-29)

**Note:** Version bump only for package @zaaksysteem/main

## [0.12.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.12.2...@zaaksysteem/main@0.12.3) (2019-10-25)

**Note:** Version bump only for package @zaaksysteem/main

## [0.12.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.12.1...@zaaksysteem/main@0.12.2) (2019-10-22)

**Note:** Version bump only for package @zaaksysteem/main

## [0.12.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.12.0...@zaaksysteem/main@0.12.1) (2019-10-17)

**Note:** Version bump only for package @zaaksysteem/main

# [0.12.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.11.3...@zaaksysteem/main@0.12.0) (2019-10-17)

### Features

- **Communciation:** MINTY-1790 Add communication module on `/main/customer-contact` route ([c90005c](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c90005c))
- **Communication:** MINTY-1790 Add contacts and customer-contact to main dashboard for testing purposes ([fb27a9a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/fb27a9a))
- **Snackbar:** MINTY-1788 Implement snackbar in pip and main app ([dcbe4da](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/dcbe4da))

## [0.11.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.11.2...@zaaksysteem/main@0.11.3) (2019-09-30)

**Note:** Version bump only for package @zaaksysteem/main

## [0.11.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.11.1...@zaaksysteem/main@0.11.2) (2019-09-26)

**Note:** Version bump only for package @zaaksysteem/main

## [0.11.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.11.0...@zaaksysteem/main@0.11.1) (2019-09-26)

**Note:** Version bump only for package @zaaksysteem/main

# [0.11.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.10.0...@zaaksysteem/main@0.11.0) (2019-09-09)

### Bug Fixes

- **Typescript:** fix misc. errors ([a5eb790](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a5eb790))

### Features

- **Apps:** Implement typescript ([583155f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/583155f))
- **Note:** Implement typescript ([47808bc](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/47808bc))
- **SwitchViewButton:** Implement typescript ([b119c2d](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b119c2d))
- **Tabs:** Implement typescript ([7eabf36](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7eabf36))
- **Thread:** Implement typescript ([896ada2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/896ada2))
- **ThreadPlaceholder:** Implement typescript ([be532c7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/be532c7))
- **TypeScript:** Add tsconfig.json, which is needed to enable typescript ([da89a6b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/da89a6b))

# [0.10.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.9.1...@zaaksysteem/main@0.10.0) (2019-09-06)

### Bug Fixes

- **Communication:** restrict rendering contentarea when on threads-only route ([747c4ce](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/747c4ce))

### Features

- **Communication:** MINTY-1289 Add link to case when thread is displayed in the context of a contact ([ed0bd1e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ed0bd1e))
- **Communication:** MINTY-1294 Implement `CaseFinder` component in add `ContactMoment` form ([27af541](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/27af541))
- **Communication:** MINTY-1402 - add support for saving a note ([4fd9e80](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4fd9e80))

## [0.9.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.9.0...@zaaksysteem/main@0.9.1) (2019-09-05)

**Note:** Version bump only for package @zaaksysteem/main

# [0.9.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.8.1...@zaaksysteem/main@0.9.0) (2019-08-29)

### Bug Fixes

- **Communication:** fix invalid value when saving a contact ([4ea871b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4ea871b))
- **Communication:** MINTY-1386: fix uuid of contact when saving ([771f520](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/771f520))
- **Communication:** MINTY-1391 - display thread content with newlines ([f754156](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f754156))
- **Communication:** Update thread unwrapping to the fixed API ([3604122](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/3604122))

### Features

- **Communication:** add person/company icons to Contactfinder, some styling ([a2f2da2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a2f2da2))
- **Communication:** Add thread views for contactmoment and note ([e60867e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/e60867e))
- **Communication:** Add view for notes ([4481b91](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4481b91))
- **Communication:** change breakpoints so medium will show thread list ([1239fba](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/1239fba))
- **Communication:** Implement API v2 for threadview ([c5f2689](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c5f2689))
- **Communication:** Implement placeholder for the view side ([4271423](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4271423))
- **Communication:** MINTY-1115 Add `SearchField` to `Theads` ([5ec462a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5ec462a))
- **Communication:** MINTY-1115 Add `Threads` filter component ([409738f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/409738f))
- **Communication:** MINTY-1115 Add direction icon to indicate incoming or outgoing messages ([6b8e4fa](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6b8e4fa))
- **Communication:** MINTY-1115 Move add and refresh button to `Threads` list ([e806749](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/e806749))
- **Communication:** MINTY-1126 Style add button ([594ab7a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/594ab7a))
- **Communication:** MINTY-1260 - save Contactmoment, misc. styling and refactoring ([1c1b872](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/1c1b872))
- **Communication:** MINTY-1289 Add communication module to contact route ([5ab6ba4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5ab6ba4))
- **Communication:** Process MR feedback ([8b17f80](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8b17f80))
- **Communication:** tweak some misc. styling ([e1a16b5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/e1a16b5))
- **Communication:** Tweak some styling ([bb1158d](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/bb1158d))
- **ErrorHandling:** MINTY-1126 Centralized error handling displaying a `Alert` for every failed async action ([d65d459](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/d65d459))
- **UI:** Upgrade to MUI4 ([ac921f6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ac921f6))

## [0.8.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.8.0...@zaaksysteem/main@0.8.1) (2019-08-27)

**Note:** Version bump only for package @zaaksysteem/main

# [0.8.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.7.2...@zaaksysteem/main@0.8.0) (2019-08-22)

### Features

- **apps:** Prevent automatic opening of browser tabs on start ([4740421](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4740421))

## [0.7.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.7.1...@zaaksysteem/main@0.7.2) (2019-08-12)

**Note:** Version bump only for package @zaaksysteem/main

## [0.7.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.7.0...@zaaksysteem/main@0.7.1) (2019-08-06)

**Note:** Version bump only for package @zaaksysteem/main

# [0.7.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.6.0...@zaaksysteem/main@0.7.0) (2019-08-01)

### Features

- **Iframe:** MINTY-1121 When running in an iframe, notify top window of location changes ([4618672](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4618672))

# [0.6.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.5.0...@zaaksysteem/main@0.6.0) (2019-07-31)

### Features

- **Communication:** MINTY-1126 Add case reducer which provides information needed for communication module & add temporary dashboard, useful for navigating to open cases ([6d70c7d](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6d70c7d))

# [0.5.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.4.1...@zaaksysteem/main@0.5.0) (2019-07-31)

### Features

- **Communication:** add general Communication setup and Thread list ([7cb92e6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7cb92e6))

## [0.4.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.4.0...@zaaksysteem/main@0.4.1) (2019-07-30)

**Note:** Version bump only for package @zaaksysteem/main

# [0.4.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.3.0...@zaaksysteem/main@0.4.0) (2019-07-29)

### Features

- **Dates:** MINTY-1120 Add localized date formatting lib `fecha` ([84b6693](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/84b6693))

# [0.3.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.2.0...@zaaksysteem/main@0.3.0) (2019-07-29)

### Features

- **i18n:** MINTY-1120 Add i18next to setup and define APP_CONTEXT_ROOT variable on build time. ([a3e6a4c](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a3e6a4c))

# [0.2.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/main@0.1.0...@zaaksysteem/main@0.2.0) (2019-07-25)

### Features

- **main:** MINTY-1237 Add session state to @zaaksysteem/common and implement in app ([575898f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/575898f))

# 0.1.0 (2019-07-25)

### Features

- **main:** MINTY-1232 Add `connected-react-router` package to link router to redux store ([a673309](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a673309))

## [0.1.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/compare/@zaaksysteem/case-management@0.1.0...@zaaksysteem/case-management@0.1.1) (2019-07-23)

**Note:** Version bump only for package @zaaksysteem/case-management

# 0.1.0 (2019-07-18)

### Features

- **CI:** MINTY-1120 Add gitlab CI file ([f4e971e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/f4e971e))
- **Publishing:** MINTY-1120 Implement lerna publish command voor npm packages ([35934cc](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/35934cc))
- **Setup:** MINTY-1120 Add eslint and prettier ([0e6b94c](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/0e6b94c))
- **Setup:** MINTY-1120 Add storybook ([1dc9406](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/1dc9406))
- **Setup:** MINTY-1120 Centralize create react app overrides in common package ([3cdc9d7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/3cdc9d7))
- **Setup:** MINTY-1120 Setup monorepo for all apps and packges using lerna and yarn workspaces ([6bd626e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/6bd626e))
