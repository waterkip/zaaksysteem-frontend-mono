import { createAjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { FETCH_OPEN_CASES } from './dashboard.contstants';

const fetchOpenCasesAction = createAjaxAction(FETCH_OPEN_CASES);

const createQuery = (userId: string): string => {
  const query = `SELECT+case.status,+case.number,+case.casetype.name,+case.subject,+case.requestor.name,+case.casetype.id,+case.requestor.id,+case.requestor.uuid,+case.assignee.id+FROM+case+WHERE+case.number+>+0+NUMERIC+ORDER+BY+case.number+DESC`;

  return query.replace(/\s/g, '+');
};

export const fetchOpenCases = (userId: string) =>
  fetchOpenCasesAction({
    url: `/api/object/search?zapi_num_rows=10&zapi_page=1&zql=${createQuery(
      userId
    )}`,
    method: 'GET',
  });
