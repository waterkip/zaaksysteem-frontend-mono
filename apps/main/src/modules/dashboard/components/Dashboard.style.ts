import { makeStyles } from '@material-ui/core';

export const useDashboardStyles = makeStyles({
  wrapper: {
    display: 'flex',
    flexWrap: 'wrap',
    padding: 15,
  },
  item: { padding: 15, flex: 1, minWidth: 400 },
  itemTitle: {
    padding: 15,
    backgroundColor: '#eee',
    margin: 0,
  },
  itemContent: {
    border: '1px solid #eee',
    padding: 15,
    marginBottom: 10,
  },
  itemSectionTitle: {
    borderBottom: '1px solid #d8d8d8',
    paddingBottom: 10,
  },
});
