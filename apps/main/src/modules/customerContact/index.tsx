import React from 'react';
import CustomerContact from './components/CustomerContact';

export default function CustomerContactModule() {
  return <CustomerContact />;
}
