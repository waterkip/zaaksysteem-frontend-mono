import { createAjaxConstants } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';

export const CASE_FETCH = createAjaxConstants('CASE_FETCH');
