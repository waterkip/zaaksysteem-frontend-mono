import { combineReducers } from 'redux';
import { StateFromReducer } from '@zaaksysteem/common/src/library/redux/ajax/ajaxTools';
import context from './context/tasks.context.reducer';
import tasksReducer from './tasks/tasks.reducer';

export interface TasksRootStateType {
  tasks: {
    context: StateFromReducer<typeof context>;
    list: StateFromReducer<typeof tasksReducer>;
  };
}

export const tasks = combineReducers({
  context,
  list: tasksReducer,
});

export default tasks;
