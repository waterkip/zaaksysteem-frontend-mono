import { makeStyles } from '@material-ui/core';

export const useTaskCreatorStyles = makeStyles(
  ({ palette, typography }: any) => ({
    container: {},
    button: {
      ...typography.button,
      textTransform: 'unset',
      fontSize: 14,
      color: palette.primary.main,
    },
  })
);
