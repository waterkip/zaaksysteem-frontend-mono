import { makeStyles } from '@material-ui/core';

export const useTasksPlaceholderStyle = makeStyles(() => ({
  container: {
    paddingTop: 20,
  },
}));
