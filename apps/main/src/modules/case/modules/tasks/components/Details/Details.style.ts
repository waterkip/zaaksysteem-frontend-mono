import { makeStyles } from '@material-ui/core';

export const useDetailStyles = makeStyles(() => ({
  formControlWrapper: {
    marginBottom: 20,
  },
  scrollWrapper: {
    flex: '1 1 auto',
    overflowY: 'scroll',
    paddingRight: 13,
  },
}));
