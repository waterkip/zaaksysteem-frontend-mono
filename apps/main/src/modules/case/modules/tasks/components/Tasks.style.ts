import { makeStyles } from '@material-ui/core';

const wrapper = {
  padding: '13px 0 13px 13px',
  height: '100vh',
};

export const useTasksStyles = makeStyles(() => ({
  wrapper,
  wrapperDetails: {
    ...wrapper,
    display: 'flex',
    flexDirection: 'column',
  },
}));
