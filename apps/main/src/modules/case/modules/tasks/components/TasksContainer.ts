import { connect } from 'react-redux';
import { TasksRootStateType } from '../store/tasks.reducer';
import { TasksPropsType, Tasks } from './Tasks';

type PropsFromStateType = Pick<TasksPropsType, 'rootPath'>;

const mapStateToProps = ({
  tasks: {
    context: { rootPath },
  },
}: TasksRootStateType): PropsFromStateType => {
  return { rootPath };
};

const TasksContainer = connect<PropsFromStateType, {}, {}, TasksRootStateType>(
  mapStateToProps
)(Tasks);

export default TasksContainer;
