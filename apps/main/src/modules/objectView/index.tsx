import React from 'react';
import { RouteComponentProps } from 'react-router';
import { DynamicModuleLoader } from 'redux-dynamic-modules-react';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import locale from './locale/objectView.locale';
import { getObjectViewModule } from './store/objectView.module';
import ObjectView from './components/ObjectView';

const ObjectViewModule: React.ComponentType<RouteComponentProps<{
  uuid: string;
}>> = ({
  match: {
    params: { uuid },
  },
}) => {
  return (
    <DynamicModuleLoader modules={[getObjectViewModule(uuid)]}>
      <I18nResourceBundle resource={locale} namespace="objectView">
        <ObjectView />
      </I18nResourceBundle>
    </DynamicModuleLoader>
  );
};
export default ObjectViewModule;
