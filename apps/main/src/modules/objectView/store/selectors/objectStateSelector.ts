import { ObjectViewRootStateType } from '../objectView.reducer';
import { ObjectStateType } from '../object/object.reducer';

export const objectStateSelector = (
  state: ObjectViewRootStateType
): ObjectStateType => state.objectView.object;
