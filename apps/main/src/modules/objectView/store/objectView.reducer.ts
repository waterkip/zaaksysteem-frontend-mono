import { combineReducers } from 'redux';
import object, { ObjectStateType } from './object/object.reducer';

interface ObjectViewStateType {
  object: ObjectStateType;
}

export interface ObjectViewRootStateType {
  objectView: ObjectViewStateType;
}

const objectViewReducer = combineReducers<ObjectViewStateType>({
  object,
});

export default objectViewReducer;
