import { createAjaxConstants } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';

export const FETCH_OBJECT = createAjaxConstants('FETCH_OBJECT');
