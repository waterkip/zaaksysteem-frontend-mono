export type ObjectType = {
  uuid: string;
  registrationDate?: string;
  lastModified?: string;
  status?: 'active' | 'inactive' | 'draft';
  customFields: any;
};
