import React from 'react';
//@ts-ignore
import { H4, Body1 } from '@mintlab/ui/App/Material/Typography';
import { useAttributeGroupStyles } from './AttributeGroup.style';
import Attribute, { AttributePropsType } from './Attribute';

export type AttributeGroupPropsType = {
  title: string;
  description: string;
  attributes: AttributePropsType[];
};

const AttributesList: React.FunctionComponent<AttributeGroupPropsType> = ({
  title,
  description,
  attributes,
}) => {
  const classes = useAttributeGroupStyles();

  return (
    <div className={classes.wrapper}>
      <div className={classes.attributeListHeader}>
        <H4 classes={{ root: classes.attributeListTitle }}>{title}</H4>
        <Body1 classes={{ root: classes.attributeListDescription }}>
          {description}
        </Body1>
      </div>
      <div className={classes.attributeListContent}>
        {attributes.map(({ label, value }: AttributePropsType) => (
          <Attribute key={label} label={label} value={value} />
        ))}
      </div>
    </div>
  );
};

export default AttributesList;
