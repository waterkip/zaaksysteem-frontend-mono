import { makeStyles } from '@material-ui/core';

export const useAttributeStyles = makeStyles(() => ({
  wrapper: {
    display: 'flex',
    padding: `20px 0`,
  },
  label: {
    width: '50%',
  },
  value: {
    width: '50%',
  },
}));
