import { makeStyles } from '@material-ui/core';

export const useAttributesStyles = makeStyles(({ typography }) => ({
  wrapper: { width: '100%', padding: 20 },
  header: {
    padding: 20,
  },
}));
