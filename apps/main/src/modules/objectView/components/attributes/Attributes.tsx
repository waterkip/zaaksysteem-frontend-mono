import React from 'react';
import fecha from 'fecha';
import { useTranslation } from 'react-i18next';
//@ts-ignore
import { H2 } from '@mintlab/ui/App/Material/Typography';
//@ts-ignore
import Loader from '@mintlab/ui/App/Zaaksysteem/Loader';
import { ObjectType } from '../../types/ObjectView.types';
import { useAttributesStyles } from './Attributes.style';
import AttributeGroup, { AttributeGroupPropsType } from './AttributeGroup';
import { AttributePropsType } from './Attribute';

export type AttributeType = {
  type: string;
  name: string;
  value: string;
};

export type AttributesPropsType = {
  object: ObjectType;
};

const Attributes: React.FunctionComponent<AttributesPropsType> = ({
  object,
}) => {
  const classes = useAttributesStyles();
  const [t] = useTranslation(['objectView']);

  if (!object) {
    return <Loader />;
  }

  const { uuid, status, registrationDate, lastModified, customFields } = object;

  const customAttributes = customFields
    ? Object.entries(customFields).map(
        ([key, value]: any): AttributeType => ({
          type: key,
          name: key,
          value,
        })
      )
    : [];

  const systemAttributes = [
    {
      name: 'uuid',
      type: 'text',
      value: uuid,
    },
    ...(status
      ? [
          {
            name: 'status',
            type: 'status',
            value: status,
          },
        ]
      : []),
    ...(registrationDate
      ? [
          {
            name: 'registrationDate',
            type: 'date',
            value: registrationDate,
          },
        ]
      : []),
    ...(lastModified
      ? [
          {
            name: 'lastModified',
            type: 'date',
            value: lastModified,
          },
        ]
      : []),
  ];

  const formatAttributes = ({
    type,
    name,
    value,
  }: AttributeType): AttributePropsType => {
    const formatAsDate = (value: string) =>
      fecha.format(new Date(value as string), t('common:dates.dateFormat'));
    const systemAttributeTranslations = t<{ [key: string]: string }>(
      `attributes.systemAttributes`,
      {
        returnObjects: true,
      }
    );
    const label = systemAttributeTranslations[name] || name;

    return {
      label,
      value,
      ...(type === 'date' ? { value: formatAsDate(value) } : {}),
      ...(type === 'status' ? { value: t(`status.${value}`) } : {}),
    };
  };

  const attributeGroupDefinition: AttributeGroupPropsType[] = [
    {
      title: t('attributes.customAttributes.title'),
      description: t('attributes.customAttributes.description'),
      attributes: customAttributes.map(formatAttributes),
    },
    {
      title: t('attributes.systemAttributes.title'),
      description: t('attributes.systemAttributes.description'),
      attributes: systemAttributes.map(formatAttributes),
    },
  ];

  return (
    <div className={classes.wrapper}>
      <H2 classes={{ root: classes.header }}>{t('attributes.title')}</H2>
      {attributeGroupDefinition.map((attributeGroupDefinition, index) => (
        <AttributeGroup key={index} {...attributeGroupDefinition} />
      ))}
    </div>
  );
};

export default Attributes;
