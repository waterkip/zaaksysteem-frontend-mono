import React from 'react';
import { useSelector } from 'react-redux';
//@ts-ignore
import Loader from '@mintlab/ui/App/Zaaksysteem/Loader';
import { AJAX_STATE_PENDING } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import { objectStateSelector } from '../store/selectors/objectStateSelector';
import { ObjectType } from '../types/ObjectView.types';
import { useObjectViewStyles } from './ObjectView.style';
import Attributes from './attributes/Attributes';

export interface ObjectViewPropsType {
  object?: ObjectType;
  loading: boolean;
}

const ObjectView: React.FunctionComponent = () => {
  const { object, state: objectViewState } = useSelector(objectStateSelector);
  const classes = useObjectViewStyles();

  if (objectViewState === AJAX_STATE_PENDING || !object) {
    return <Loader />;
  }

  return (
    <div className={classes.wrapper}>
      <Attributes object={object} />
    </div>
  );
};

export default ObjectView;
