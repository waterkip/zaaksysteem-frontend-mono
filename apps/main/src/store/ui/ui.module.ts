import createUIReducer from '@zaaksysteem/common/src/store/ui/ui.reducer';

export const getUIModule = () => ({
  id: 'ui',
  reducerMap: {
    ui: createUIReducer(),
  },
});

export default getUIModule;
