import React from 'react';
import { connect } from 'react-redux';
import { Route, Switch } from 'react-router-dom';
//@ts-ignore
import Loader from '@mintlab/ui/App/Zaaksysteem/Loader';
import { AJAX_STATE_VALID } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
//@ts-ignore
import ErrorBoundary from '@zaaksysteem/common/src/components/ErrorBoundary/ErrorBoundary';
import { SessionRootStateType } from '@zaaksysteem/common/src/store/session/session.reducer';
import { useAppRootStyle } from './App.style';

const CaseModule = React.lazy(() =>
  import(/* webpackChunkName: "case" */ './modules/case')
);

const DashboardModule = React.lazy(() =>
  import(/* webpackChunkName: "dashboard" */ './modules/dashboard')
);

const ContactModule = React.lazy(() =>
  import(/* webpackChunkName: "contact" */ './modules/contact')
);

const CustomerContactModule = React.lazy(() =>
  import(/* webpackChunkName: "customer-contact" */ './modules/customerContact')
);

const Intake = React.lazy(() =>
  import(/* webpackChunkName: "intake" */ './components/Intake/Intake')
);

const ObjectViewModule = React.lazy(() =>
  import(/* webpackChunkName: "object" */ './modules/objectView')
);

export interface RoutesPropsType {
  ready: boolean;
  prefix: string;
  userId: string;
}

const Routes: React.ComponentType<RoutesPropsType> = ({
  ready,
  prefix,
  userId,
}) => {
  const classes = useAppRootStyle();
  if (!ready) {
    return <Loader delay="200" />;
  }

  return (
    <div className={classes.app}>
      <ErrorBoundary>
        <Switch>
          <Route path={`${prefix}/case/:caseId`} component={CaseModule} />
          <Route
            path={`${prefix}/contact/:contactId`}
            component={ContactModule}
          />
          <Route path={`${prefix}/object/:uuid`} component={ObjectViewModule} />
          <Route
            path={`${prefix}/customer-contact`}
            component={CustomerContactModule}
          />
          <Route path={`${prefix}/intake`} component={Intake} />
          <Route
            path="*"
            render={props => <DashboardModule {...props} userId={userId} />}
          />
        </Switch>
      </ErrorBoundary>
    </div>
  );
};

type PropsFromState = Pick<RoutesPropsType, 'ready' | 'userId'>;

const mapStateToProps = ({
  session: { state, data },
}: SessionRootStateType): PropsFromState => {
  if (state !== AJAX_STATE_VALID || !data || !data.logged_in_user) {
    return {
      ready: false,
      userId: '',
    };
  }

  return {
    ready: true,
    userId: data.logged_in_user.id,
  };
};

export default connect<PropsFromState, {}, {}, SessionRootStateType>(
  mapStateToProps
)(Routes);
