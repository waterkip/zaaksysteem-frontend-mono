import { makeStyles } from '@material-ui/core';

export const useAppRootStyle = makeStyles(({ typography }: any) => ({
  app: {
    fontFamily: typography.fontFamily,
    height: 'inherit',
    minHeight: 'inherit',
  },
}));
