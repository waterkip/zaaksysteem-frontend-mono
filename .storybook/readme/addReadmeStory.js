import React from 'react';
import { storiesOf, getStorybook } from '@storybook/react';
import { MarkdownFile } from './MarkdownFile';
import { createStoryPath } from './library/utils';

export function addReadmeStory(file, resolvedModule) {
  const { fileName, path } = createStoryPath(file);
  const story = storiesOf(`README / ${path}`, resolvedModule);
  const markdownSource = resolvedModule.default;
  const storybook = getStorybook();

  const existingStory = storybook.find(
    addedStory => addedStory.kind === story.kind
  );
  const storyIsAlreadyAdded = existingStory
    ? existingStory.stories.find(({ name }) => name === fileName)
    : false;

  if (!storyIsAlreadyAdded) {
    story.addDecorator(story => <div id="documentation">{story()}</div>);

    story.add(fileName, () => <MarkdownFile source={markdownSource} />);
  }
}
