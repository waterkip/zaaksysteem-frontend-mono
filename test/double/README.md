# Test doubles

> Monkey patches for JSDOM and test replacements for application code.
> Do not reuse any modules from the system under test here.

- Short version: https://martinfowler.com/bliki/TestDouble.html
- Long version: http://xunitpatterns.com/Test%20Double.html
