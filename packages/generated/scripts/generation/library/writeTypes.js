/* eslint-disable no-console */
const fs = require('fs');
const { promisify } = require('util');

const writeFileAsync = promisify(fs.writeFile);

function writeFile(output, outputPath) {
  return writeFileAsync(outputPath, output);
}

async function writeTypes(types, outputPath, environmentUrl) {
  types.forEach(([domain, paths, entities]) => {
    const banner = `/*
* Generated on: ${new Date()}
* Environment used: ${environmentUrl}
*
* This file was automatically generated. DO NOT MODIFY IT BY HAND.
* Instead, rerun generation of ${domain} domain.
*/

/* eslint-disable */`;
    const allTypes = [...paths, ...entities];
    const output = allTypes.join('\n');
    const namespacedOutput = `
${banner}

export namespace API${domain} {
${output}
}
    `;

    const path = `${outputPath}API${domain}.types.ts`;
    writeFile(namespacedOutput, path);

    console.log(`- written ${allTypes.length} types in ${path}`);
  });
}

module.exports = writeTypes;
