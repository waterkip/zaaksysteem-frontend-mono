const fetch = require('node-fetch');
const $RefParser = require('json-schema-ref-parser');
const handle = require('../../library/promiseHandle');

function createNullableType(data) {
  if (data.type === 'object') {
    return {
      oneOf: [{ type: 'null' }, data],
    };
  } else {
    return {
      ...data,
      type: [data.type, 'null'],
    };
  }
}

/*
 * The nullable property of OpenAPI spec 3.0 is not supported by the parser,
 * manually converting it to be JSON schema 4.0 compliant
 */
function parseNullable(schema) {
  return Object.entries(schema).reduce((acc, [key, value]) => {
    const parsedValue = value.nullable ? createNullableType(value) : value;

    return {
      ...acc,
      [key]:
        typeof parsedValue === 'object' && !Array.isArray(parsedValue)
          ? parseNullable(parsedValue)
          : value,
    };
  }, {});
}

async function fetchAndResolveApiSpec(environmentUrl, baseUrl, fileName) {
  const url = `${environmentUrl}${baseUrl}${fileName}`;
  console.log('Reading spec file', url);
  const [response, responseError] = await handle(fetch(url));

  if (responseError)
    throw Error(`Error fetching schema ${url}: ${responseError}`);

  const [json, jsonError] = await handle(response.json());

  if (jsonError)
    throw Error(`Error parsing JSON for schema ${url}: ${responseError}`);

  const nullableSchema = parseNullable(json);

  return new Promise(resolve => {
    $RefParser.dereference(
      nullableSchema,
      {
        resolve: {
          file: {
            read(file, callback) {
              const [filename] = file.url.split('/').reverse();

              fetch(`${environmentUrl}${baseUrl}${filename}`)
                .then(fileResponse => fileResponse.json())
                .then(fileJson => callback(null, parseNullable(fileJson)))
                .catch(error => callback(error));
            },
          },
        },
      },
      (err, data) => {
        if (err) {
          throw err;
        }

        resolve(data);
      }
    );
  });
}

async function fetchAndResolveApiSpecs(environmentUrl, schemas) {
  const schemaPromises = Object.entries(schemas).map(
    ([domain, { paths, entities, baseUrl }]) => {
      return Promise.all([
        Promise.resolve(domain),
        fetchAndResolveApiSpec(environmentUrl, baseUrl, paths),
        fetchAndResolveApiSpec(environmentUrl, baseUrl, entities),
      ]);
    }
  );

  const [specs, specsError] = await handle(Promise.all(schemaPromises));

  if (specsError)
    throw Error(`Error fetching/resolving api specs: ${specsError}`);

  return specs;
}

module.exports = fetchAndResolveApiSpecs;
