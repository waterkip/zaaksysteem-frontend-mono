/* eslint-disable no-unused-vars, no-console */
const { compile } = require('json-schema-to-typescript');
const handle = require('../../library/promiseHandle');

const sanitizeName = name => `${name}`.replace('Entity', '');

function createOperationTypeName(operationId, postFix) {
  const typeName = `${operationId}`
    .split('_')
    .map(
      ([firstLetter, ...remaining]) =>
        `${firstLetter.toUpperCase()}${remaining.join('')}`
    )
    .join('');

  return sanitizeName(`${typeName}${postFix}`);
}

function shouldRemoveTitle(schema, name, value) {
  return (
    typeof schema.description === 'undefined' &&
    name === 'title' &&
    typeof value === 'string'
  );
}

/**
 * Rename nested title properties to description, because using the title
 * property will create a new type. And since titles are not unique we end
 * up with duplicated types. It's fairly safe to rename the title properties
 * to description properties since they seem to be used in that way.
 */
function renameTitleProperties(schema, remove = false) {
  return Object.entries(schema).reduce((acc, [name, value]) => {
    const shouldRemove = shouldRemoveTitle(schema, name, value) && remove;
    const propertyName = shouldRemove ? 'description' : name;

    return {
      ...acc,
      [propertyName]:
        !Array.isArray(value) && value === Object(value)
          ? renameTitleProperties(value, true)
          : value,
    };
  }, {});
}

async function compileSchema(schema, name) {
  const fixedSchema = renameTitleProperties(schema);

  const [type, typeError] = await handle(
    compile(fixedSchema, name, {
      declareExternallyReferenced: false,
      unreachableDefinitions: false,
      bannerComment: '',
    })
  );

  if (typeError) throw Error(`Error compiling schema ${name}: ${typeError}`);

  return type;
}

function compileResponseBody(schema, operationId) {
  const { allOf, ...restSchema } = schema;
  const toCompile = Object.keys(restSchema).length ? restSchema : schema;

  return compileSchema(
    toCompile,
    createOperationTypeName(operationId, 'ResponseBody')
  );
}

function compileRequestBody(requestBody, operationId) {
  const { schema } =
    requestBody.content['multipart/form-data'] ||
    requestBody.content['application/json'];
  const { allOf, ...restSchema } = schema;

  const toCompile = Object.keys(restSchema).length ? restSchema : schema;
  return compileSchema(
    toCompile,
    createOperationTypeName(operationId, 'RequestBody')
  );
}

function compileRequestParams(schema, operationId) {
  const flattenedSchema = schema.reduce(
    (acc, item) => {
      return {
        ...acc,
        properties: {
          ...acc.properties,
          [item.name]: item.schema,
        },
        required: item.required ? [...acc.required, item.name] : acc.required,
      };
    },
    { properties: {}, required: [] }
  );

  return compileSchema(
    flattenedSchema,
    createOperationTypeName(operationId, 'RequestParams')
  );
}

function hasSuccessfulResponse(responses) {
  return (
    responses &&
    responses[200] &&
    responses[200].content &&
    responses[200].content['application/json'] &&
    responses[200].content['application/json'].schema
  );
}

function compileRequestResponseTypes(schema) {
  const promises = Object.values(schema.paths).reduce((acc, pathValue) => {
    const allOperations = Object.values(pathValue).reduce(
      (operations, operationValue) => {
        try {
          const requestParams = operationValue.parameters
            ? compileRequestParams(
                operationValue.parameters,
                operationValue.operationId
              )
            : null;
          const responseBody = hasSuccessfulResponse(operationValue.responses)
            ? compileResponseBody(
                operationValue.responses[200].content['application/json']
                  .schema,
                operationValue.operationId
              )
            : null;
          const requestBody = operationValue.requestBody
            ? compileRequestBody(
                operationValue.requestBody,
                operationValue.operationId
              )
            : null;

          return [...operations, requestParams, responseBody, requestBody];
        } catch (error) {
          console.log(error);
          throw Error(error);
        }
      },
      []
    );

    return [...acc, ...allOperations];
  }, []);

  return promises.filter(Boolean);
}

function compileEntityTypes(schema) {
  const promises = Object.values(schema).reduce((schemas, value) => {
    return [
      ...schemas,
      ...Object.entries(value).reduce((entities, [key, schemaItem]) => {
        return key === 'schemas'
          ? [...entities, ...compileEntityTypes(value)]
          : [...entities, compileSchema(schemaItem, sanitizeName(key))];
      }, []),
    ];
  }, []);

  return promises.filter(Boolean);
}

async function compileTypes(schemas) {
  const typesPromises = schemas.map(([domain, paths, entities]) => {
    return Promise.all([
      Promise.resolve(domain),
      Promise.all(compileRequestResponseTypes(paths)),
      Promise.all(compileEntityTypes(entities)),
    ]);
  });

  const [types, typesError] = await handle(Promise.all(typesPromises));

  if (typesError) throw Error(`Error compiling types ${typesError}`);

  return types;
}

module.exports = compileTypes;
