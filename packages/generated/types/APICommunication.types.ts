/*
 * Generated on: Mon Mar 30 2020 10:47:54 GMT+0200 (Central European Summer Time)
 * Environment used: https://development.zaaksysteem.nl
 *
 * This file was automatically generated. DO NOT MODIFY IT BY HAND.
 * Instead, rerun generation of Communication domain.
 */

/* eslint-disable */

export namespace APICommunication {
  export interface ThreadListRequestParams {
    filter?: {
      contact_uuid?: string;
      case_uuid?: string;
      message_types?: 'external' | 'contact_moment' | 'note';
    };
    [k: string]: any;
  }

  export interface ThreadListResponseBody {
    data: {
      id: string;
      type: 'message_thread';
      meta: {
        created: string;
        last_modified: string;
        number_of_messages: number;
        summary: string;
        unread_employee_count: number;
        unread_pip_count: number;
        attachment_count: number;
      };
      attributes: {
        thread_type: 'note' | 'contact_moment' | 'pip_message' | 'email';
        last_message:
          | {
              message_type: 'note';
              slug: string;
              created_name: string;
              created: string;
            }
          | {
              message_type: 'contact_moment';
              slug: string;
              direction: 'incoming' | 'outgoing';
              channel:
                | 'assignee'
                | 'frontdesk'
                | 'phone'
                | 'mail'
                | 'email'
                | 'webform'
                | 'social_media'
                | 'external_application';
              created_name: string;
              recipient_name: string;
              created: string;
            }
          | {
              message_type: 'email';
              slug: string;
              subject: string;
              created_name: string;
              created: string;
            }
          | {
              message_type: 'pip_message';
              slug: string;
              subject: string;
              created_name: string;
              created: string;
            };
        [k: string]: any;
      };
      relationships?: {
        contact?: {
          data: {
            id: string;
            type: 'contact';
            attributes: {
              type: 'employee' | 'person' | 'organization';
              name: string;
              address: string | null;
              email_address: string | null;
            };
          };
        };
        case?: {
          data: {
            id: string;
            type: 'case';
            attributes: {
              display_id: number;
              description: string | null;
              case_type_name: string;
              status: 'new' | 'open' | 'stalled' | 'resolved';
            };
          };
        };
      };
      links: {
        thread_messages: {
          href: string;
        };
      };
    }[];
  }

  export interface SearchContactRequestParams {
    keyword: string;
    filter?: {
      type?: ('employee' | 'person' | 'organization')[];
    };
    [k: string]: any;
  }

  export interface SearchContactResponseBody {
    data: {
      id: string;
      type: 'contact';
      attributes: {
        type: 'employee' | 'person' | 'organization';
        name: string;
        address: string | null;
        email_address: string | null;
      };
    }[];
  }

  export interface GetMessageListRequestParams {
    thread_uuid: string;
    [k: string]: any;
  }

  export interface GetMessageListResponseBody {
    data: (
      | {
          id: string;
          type: 'contact_moment';
          meta: {
            last_modified?: string;
            created: string;
            message_date?: string;
            summary: string;
          };
          attributes: {
            content: string;
            direction: 'incoming' | 'outgoing';
            channel:
              | 'assignee'
              | 'frontdesk'
              | 'phone'
              | 'mail'
              | 'email'
              | 'webform'
              | 'social_media'
              | 'external_application';
          };
          relationships: {
            created_by: {
              data: {
                id: string;
                type: 'contact';
                attributes: {
                  type: 'employee' | 'person' | 'organization';
                  name: string;
                  address: string | null;
                  email_address: string | null;
                };
              };
            };
            recipient: {
              data: {
                id: string;
                type: 'contact';
                attributes: {
                  type: 'employee' | 'person' | 'organization';
                  name: string;
                  address: string | null;
                  email_address: string | null;
                };
              };
            };
            thread: {
              data: {
                id: string;
                type: 'message_thread';
              };
            };
          };
        }
      | {
          id: string;
          type: 'note';
          meta: {
            last_modified?: string;
            created: string;
            message_date?: string;
            summary: string;
          };
          attributes: {
            content: string;
          };
          relationships: {
            created_by: {
              data: {
                id: string;
                type: 'contact';
                attributes: {
                  type: 'employee' | 'person' | 'organization';
                  name: string;
                  address: string | null;
                  email_address: string | null;
                };
              };
            };
            thread: {
              data: {
                id: string;
                type: 'message_thread';
              };
            };
          };
        }
      | {
          id: string;
          type: 'external_message';
          meta: {
            last_modified?: string;
            created: string;
            message_date?: string;
            read_employee: string | null;
            read_pip: string | null;
            summary: string;
            attachment_count: number;
          };
          attributes: {
            message_type: 'pip' | 'email';
            content: string;
            subject: string | null;
            participants?: {
              role: 'to' | 'from' | 'cc' | 'bcc';
              display_name: string;
              address: string;
            }[];
            is_imported?: boolean;
          };
          relationships: {
            created_by?: {
              data: {
                id: string;
                type: 'contact';
                attributes: {
                  type: 'employee' | 'person' | 'organization';
                  name: string;
                  address: string | null;
                  email_address: string | null;
                };
              };
            };
            thread: {
              data: {
                id: string;
                type: 'message_thread';
              };
            };
            attachments: {
              data: {
                id: string;
                type: 'Attachment';
                attributes: {
                  name: string;
                  md5: string;
                  mimetype: string;
                  size: number;
                  date_created: string;
                };
                links: {
                  download: string;
                  preview?: string;
                };
              }[];
            };
          };
        }
    )[];
    relationships?: {
      case?: {
        data: {
          id: string;
          type: 'case';
          attributes: {
            display_id: number;
            description: string | null;
            case_type_name: string;
            status: 'new' | 'open' | 'stalled' | 'resolved';
          };
        };
      };
      [k: string]: any;
    };
  }

  export interface ImportEmailMessageResponseBody {
    data: {
      success: boolean;
    };
  }

  export interface ImportEmailMessageRequestBody {
    file_uuid: string;
    case_uuid: string;
  }

  export interface CreateContactMomentResponseBody {
    data: {
      success: boolean;
    };
  }

  export interface CreateContactMomentRequestBody {
    contact_moment_uuid?: string;
    thread_uuid: string;
    case_uuid?: string | null;
    contact_uuid: string;
    channel:
      | 'assignee'
      | 'frontdesk'
      | 'phone'
      | 'mail'
      | 'email'
      | 'webform'
      | 'social_media'
      | 'external_application';
    content: string;
    direction: 'incoming' | 'outgoing';
  }

  export interface CreateNoteResponseBody {
    data: {
      success: boolean;
    };
  }

  export type CreateNoteRequestBody = {
    note_uuid: string;
    thread_uuid: string;
    content: string;
  } & (
    | {
        case_uuid: string;
      }
    | {
        contact_uuid: string;
      }
  );

  export interface SearchCaseRequestParams {
    search_term: string;
    minimum_permission: 'search' | 'read' | 'write' | 'manage';
    filter?: {
      status?: ('new' | 'open' | 'stalled' | 'resolved')[];
    };
    limit?: number;
    [k: string]: any;
  }

  export interface SearchCaseResponseBody {
    data: {
      id: string;
      type: 'case';
      attributes: {
        display_id: number;
        description: string | null;
        case_type_name: string;
        status: 'new' | 'open' | 'stalled' | 'resolved';
      };
    }[];
  }

  export interface CreateExternalMessageResponseBody {
    data: {
      success: boolean;
    };
  }

  export interface CreateExternalMessageRequestBody {
    case_uuid: string;
    content: string;
    message_uuid: string;
    thread_uuid: string;
    message_type?: 'pip' | 'email';
    direction?: 'incoming' | 'outgoing' | 'unspecified';
    subject: string;
    attachments: string[];
    participants?: {
      role: 'to' | 'from' | 'cc' | 'bcc';
      display_name: string;
      address: string;
    }[];
  }

  export interface GetCaseListForContactRequestParams {
    contact_uuid: string;
    [k: string]: any;
  }

  export interface GetCaseListForContactResponseBody {
    data: {
      id: string;
      type: 'case';
      attributes: {
        display_id: number;
        description: string | null;
        case_type_name: string;
        status: 'new' | 'open' | 'stalled' | 'resolved';
      };
    }[];
  }

  export interface DownloadAttachmentRequestParams {
    id: string;
    [k: string]: any;
  }

  export interface LinkThreadToCaseResponseBody {
    data: {
      success: boolean;
    };
  }

  export interface LinkThreadToCaseRequestBody {
    case_uuid: string;
    thread_uuid: string;
    type: 'email';
  }

  export interface DeleteMessageResponseBody {
    data: {
      success: boolean;
    };
  }

  export interface DeleteMessageRequestBody {
    message_uuid: string;
  }

  export interface MarkMessagesReadResponseBody {
    data: {
      success: boolean;
    };
  }

  export interface MarkMessagesReadRequestBody {
    /**
     * List of uuid of messages on thread to mark as read.
     */
    message_uuids: string[];
    /**
     * Context of the request.
     */
    context: 'pip' | 'employee';
  }

  export interface CreateArchiveFromMessageUuidResponseBody {
    data: {
      success: boolean;
    };
  }

  export interface CreateArchiveFromMessageUuidRequestBody {
    /**
     * The message UUID of the message to archive as document
     */
    message_uuid: string;
  }

  export interface PreviewAttachmentRequestParams {
    id: string;
    [k: string]: any;
  }

  export interface MessageThread {
    id: string;
    type: 'message_thread';
    meta: {
      created: string;
      last_modified: string;
      number_of_messages: number;
      summary: string;
      unread_employee_count: number;
      unread_pip_count: number;
      attachment_count: number;
    };
    attributes: {
      thread_type: 'note' | 'contact_moment' | 'pip_message' | 'email';
      last_message:
        | {
            message_type: 'note';
            slug: string;
            created_name: string;
            created: string;
          }
        | {
            message_type: 'contact_moment';
            slug: string;
            direction: 'incoming' | 'outgoing';
            channel:
              | 'assignee'
              | 'frontdesk'
              | 'phone'
              | 'mail'
              | 'email'
              | 'webform'
              | 'social_media'
              | 'external_application';
            created_name: string;
            recipient_name: string;
            created: string;
          }
        | {
            message_type: 'email';
            slug: string;
            subject: string;
            created_name: string;
            created: string;
          }
        | {
            message_type: 'pip_message';
            slug: string;
            subject: string;
            created_name: string;
            created: string;
          };
      [k: string]: any;
    };
    relationships?: {
      contact?: {
        data: {
          id: string;
          type: 'contact';
          attributes: {
            type: 'employee' | 'person' | 'organization';
            name: string;
            address: string | null;
            email_address: string | null;
          };
        };
      };
      case?: {
        data: {
          id: string;
          type: 'case';
          attributes: {
            display_id: number;
            description: string | null;
            case_type_name: string;
            status: 'new' | 'open' | 'stalled' | 'resolved';
          };
        };
      };
    };
    links: {
      thread_messages: {
        href: string;
      };
    };
  }

  export type Message =
    | {
        id: string;
        type: 'contact_moment';
        meta: {
          last_modified?: string;
          created: string;
          message_date?: string;
          summary: string;
        };
        attributes: {
          content: string;
          direction: 'incoming' | 'outgoing';
          channel:
            | 'assignee'
            | 'frontdesk'
            | 'phone'
            | 'mail'
            | 'email'
            | 'webform'
            | 'social_media'
            | 'external_application';
        };
        relationships: {
          created_by: {
            data: {
              id: string;
              type: 'contact';
              attributes: {
                type: 'employee' | 'person' | 'organization';
                name: string;
                address: string | null;
                email_address: string | null;
              };
            };
          };
          recipient: {
            data: {
              id: string;
              type: 'contact';
              attributes: {
                type: 'employee' | 'person' | 'organization';
                name: string;
                address: string | null;
                email_address: string | null;
              };
            };
          };
          thread: {
            data: {
              id: string;
              type: 'message_thread';
            };
          };
        };
      }
    | {
        id: string;
        type: 'note';
        meta: {
          last_modified?: string;
          created: string;
          message_date?: string;
          summary: string;
        };
        attributes: {
          content: string;
        };
        relationships: {
          created_by: {
            data: {
              id: string;
              type: 'contact';
              attributes: {
                type: 'employee' | 'person' | 'organization';
                name: string;
                address: string | null;
                email_address: string | null;
              };
            };
          };
          thread: {
            data: {
              id: string;
              type: 'message_thread';
            };
          };
        };
      }
    | {
        id: string;
        type: 'external_message';
        meta: {
          last_modified?: string;
          created: string;
          message_date?: string;
          read_employee: string | null;
          read_pip: string | null;
          summary: string;
          attachment_count: number;
        };
        attributes: {
          message_type: 'pip' | 'email';
          content: string;
          subject: string | null;
          participants?: {
            role: 'to' | 'from' | 'cc' | 'bcc';
            display_name: string;
            address: string;
          }[];
          is_imported?: boolean;
        };
        relationships: {
          created_by?: {
            data: {
              id: string;
              type: 'contact';
              attributes: {
                type: 'employee' | 'person' | 'organization';
                name: string;
                address: string | null;
                email_address: string | null;
              };
            };
          };
          thread: {
            data: {
              id: string;
              type: 'message_thread';
            };
          };
          attachments: {
            data: {
              id: string;
              type: 'Attachment';
              attributes: {
                name: string;
                md5: string;
                mimetype: string;
                size: number;
                date_created: string;
              };
              links: {
                download: string;
                preview?: string;
              };
            }[];
          };
        };
      };

  export interface Contact {
    id: string;
    type: 'contact';
    attributes: {
      type: 'employee' | 'person' | 'organization';
      name: string;
      address: string | null;
      email_address: string | null;
    };
  }

  export interface Case {
    id: string;
    type: 'case';
    attributes: {
      display_id: number;
      description: string | null;
      case_type_name: string;
      status: 'new' | 'open' | 'stalled' | 'resolved';
    };
  }

  export interface MessageNote {
    id: string;
    type: 'note';
    meta: {
      last_modified?: string;
      created: string;
      message_date?: string;
      summary: string;
    };
    attributes: {
      content: string;
    };
    relationships: {
      created_by: {
        data: {
          id: string;
          type: 'contact';
          attributes: {
            type: 'employee' | 'person' | 'organization';
            name: string;
            address: string | null;
            email_address: string | null;
          };
        };
      };
      thread: {
        data: {
          id: string;
          type: 'message_thread';
        };
      };
    };
  }

  export interface MessageContactMoment {
    id: string;
    type: 'contact_moment';
    meta: {
      last_modified?: string;
      created: string;
      message_date?: string;
      summary: string;
    };
    attributes: {
      content: string;
      direction: 'incoming' | 'outgoing';
      channel:
        | 'assignee'
        | 'frontdesk'
        | 'phone'
        | 'mail'
        | 'email'
        | 'webform'
        | 'social_media'
        | 'external_application';
    };
    relationships: {
      created_by: {
        data: {
          id: string;
          type: 'contact';
          attributes: {
            type: 'employee' | 'person' | 'organization';
            name: string;
            address: string | null;
            email_address: string | null;
          };
        };
      };
      recipient: {
        data: {
          id: string;
          type: 'contact';
          attributes: {
            type: 'employee' | 'person' | 'organization';
            name: string;
            address: string | null;
            email_address: string | null;
          };
        };
      };
      thread: {
        data: {
          id: string;
          type: 'message_thread';
        };
      };
    };
  }

  export interface MessageExternal {
    id: string;
    type: 'external_message';
    meta: {
      last_modified?: string;
      created: string;
      message_date?: string;
      read_employee: string | null;
      read_pip: string | null;
      summary: string;
      attachment_count: number;
    };
    attributes: {
      message_type: 'pip' | 'email';
      content: string;
      subject: string | null;
      participants?: {
        role: 'to' | 'from' | 'cc' | 'bcc';
        display_name: string;
        address: string;
      }[];
      is_imported?: boolean;
    };
    relationships: {
      created_by?: {
        data: {
          id: string;
          type: 'contact';
          attributes: {
            type: 'employee' | 'person' | 'organization';
            name: string;
            address: string | null;
            email_address: string | null;
          };
        };
      };
      thread: {
        data: {
          id: string;
          type: 'message_thread';
        };
      };
      attachments: {
        data: {
          id: string;
          type: 'Attachment';
          attributes: {
            name: string;
            md5: string;
            mimetype: string;
            size: number;
            date_created: string;
          };
          links: {
            download: string;
            preview?: string;
          };
        }[];
      };
    };
  }

  export interface Attachment {
    id: string;
    type: 'Attachment';
    attributes: {
      name: string;
      md5: string;
      mimetype: string;
      size: number;
      date_created: string;
    };
    links: {
      download: string;
      preview?: string;
    };
  }

  export interface Thread {
    id: string;
    type: 'message_thread';
  }

  export interface MessageParticipant {
    role: 'to' | 'from' | 'cc' | 'bcc';
    display_name: string;
    address: string;
  }
}
