import { CommunicationRootStateType } from '../communication.reducer';

export const getNotRelatedOrRelatedCaseNotResolved = (
  state: CommunicationRootStateType
) => {
  const {
    communication: {
      thread: { caseRelation },
    },
  } = state;

  return caseRelation?.status !== 'resolved';
};
