import { createAjaxConstants } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
export const IMPORT_MESSAGE = createAjaxConstants(
  'COMMUNICATION_IMPORT_MESSAGE'
);
