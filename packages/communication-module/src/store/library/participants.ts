import { APICommunication } from '@zaaksysteem/generated';
import { NestedFormValue } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import {
  ExternalMessageParticipantsType,
  ParticipantType,
} from '../../types/Message.types';

const createParticipant = (role: 'to' | 'cc' | 'bcc' | 'from') => (
  participant: ParticipantType
): APICommunication.MessageParticipant => ({
  address: participant.email,
  role,
  display_name: participant.name || participant.email,
});

export const toParticipantsObject = (
  participants: APICommunication.MessageParticipant[]
): ExternalMessageParticipantsType => {
  return participants.reduce<ExternalMessageParticipantsType>(
    (acc, { address, role, display_name }) => {
      const participant = {
        email: address,
        name: display_name,
      };

      return {
        ...acc,
        [role]: role === 'from' ? participant : [...acc[role], participant],
      };
    },
    {
      to: [],
      cc: [],
      bcc: [],
    }
  );
};

export const toParticipantsArray = (
  participants: ExternalMessageParticipantsType
): APICommunication.MessageParticipant[] => {
  const from: APICommunication.MessageParticipant[] = participants.from
    ? [createParticipant('from')(participants.from)]
    : [];

  return [
    ...from,
    ...participants.to.map(createParticipant('to')),
    ...participants.cc.map(createParticipant('cc')),
    ...participants.bcc.map(createParticipant('bcc')),
  ];
};

export const createReplyParticipants = (
  participants:
    | ExternalMessageParticipantsType
    | APICommunication.MessageParticipant[],
  sender?: ParticipantType
): ExternalMessageParticipantsType => {
  const participantObject = Array.isArray(participants)
    ? toParticipantsObject(participants)
    : participants;
  const { bcc, cc, to, from } = participantObject;
  const toWithoutSender = sender
    ? to.filter(({ email }) => email !== sender.email)
    : to;

  return {
    to: from ? [from, ...toWithoutSender] : toWithoutSender,
    cc,
    bcc,
    from: sender,
  };
};

export const mapToParticipant = (
  participant: NestedFormValue
): ParticipantType => ({
  email: participant.value.toString(),
  name: participant.label
    ? participant.label.toString()
    : participant.value.toString(),
});
