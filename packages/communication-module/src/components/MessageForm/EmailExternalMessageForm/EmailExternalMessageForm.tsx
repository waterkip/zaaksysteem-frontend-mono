import React, { useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import {
  PartialFormValuesType,
  FormValuesType,
  NestedFormValue,
} from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import mapValuesToFormDefinition from '@zaaksysteem/common/src/components/form/library/mapValuesToFormDefinition';
import { asArray } from '@mintlab/kitchen-sink/source';
import GenericMessageForm from '../GenericMessageForm/GenericMessageForm';
import { SaveMessageFormValuesType } from '../../../types/Message.types';
import { GenericMessageFormPropsType } from '../GenericMessageForm/GenericMessageForm.types';
import { getEmailFormDefinition } from './emailExternalMessageForm.formDefinition';

export type EmailExternalMessageFormProps<
  Values = SaveMessageFormValuesType
> = Pick<
  GenericMessageFormPropsType<Values>,
  'save' | 'busy' | 'cancel' | 'top' | 'enablePreview'
> & {
  caseUuid?: string;
  contactUuid?: string;
  initialValues?: PartialFormValuesType<SaveMessageFormValuesType>;
  selectedRecipientType?: string;
};

const flattenEmail = (item: NestedFormValue): string => {
  if (item.label && item.label !== item.value) {
    return `${item.label} <${item.value}>`;
  }

  return item.value.toString();
};

const mapPreviewValues = (
  values: FormValuesType<SaveMessageFormValuesType>
): FormValuesType<SaveMessageFormValuesType> => {
  const mappedValues = {
    ...values,
    to: values.to ? asArray(values.to).map(flattenEmail) : values.to,
    cc: values.cc ? asArray(values.cc).map(flattenEmail) : values.cc,
    bcc: values.bcc ? asArray(values.bcc).map(flattenEmail) : values.bcc,
  };

  return mappedValues;
};

export const EmailExternalMessageForm: React.ComponentType<EmailExternalMessageFormProps> = props => {
  const { caseUuid, contactUuid, initialValues, selectedRecipientType } = props;
  const [t] = useTranslation('communication');
  const formDefinition = useMemo(() => {
    return getEmailFormDefinition({
      caseUuid,
      contactUuid,
      selectedRecipientType,
    });
  }, [caseUuid, contactUuid, selectedRecipientType]);
  const formDefinitionWithValues = useMemo(() => {
    return initialValues
      ? mapValuesToFormDefinition<SaveMessageFormValuesType>(
          initialValues,
          formDefinition
        )
      : formDefinition;
  }, []);

  return (
    <GenericMessageForm
      {...props}
      mapPreviewValues={mapPreviewValues}
      formDefinition={formDefinitionWithValues}
      primaryButtonLabelKey={t('forms.send')}
      formName="email-form"
    />
  );
};
