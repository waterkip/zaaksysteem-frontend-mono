import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import {
  FormDefinition,
  FormDefinitionField,
} from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { CaseDocumentSelectConfigType } from '@zaaksysteem/common/src/components/form/fields/CaseDocumentSelect/CaseDocumentSelect';
import {
  CASE_SELECTOR,
  EMAIL_RECIPIENT,
} from '../../../library/communicationFieldTypes.constants';
import { SaveMessageFormValuesType } from '../../../types/Message.types';
import { EmailRecipientConfigType } from '../GenericMessageForm/fields/EmailRecipient/EmailRecipient';
import { CaseSelectorConfigType } from '../GenericMessageForm/fields/CaseSelector/CaseSelector';

type EmailFormDefinitionPayloadType = {
  caseUuid?: string;
  contactUuid?: string;
  selectedRecipientType?: string;
};

export const getEmailFormDefinition = ({
  caseUuid,
  selectedRecipientType,
  contactUuid,
}: EmailFormDefinitionPayloadType): FormDefinition<SaveMessageFormValuesType> => [
  ...(contactUuid && !caseUuid
    ? [
        <
          FormDefinitionField<SaveMessageFormValuesType, CaseSelectorConfigType>
        >{
          name: 'case_uuid',
          type: CASE_SELECTOR,
          value: '',
          required: true,
          placeholder: 'communication:addFields.selectCase',
          label: 'communication:addFields.selectCase',
          isSearchable: false,
          config: {
            contactUuid,
          },
        },
      ]
    : [
        <FormDefinitionField<SaveMessageFormValuesType>>{
          name: 'case_uuid',
          value: caseUuid,
          type: fieldTypes.TEXT,
          hidden: true,
        },
      ]),
  <FormDefinitionField<SaveMessageFormValuesType, EmailRecipientConfigType>>{
    name: 'to',
    type: EMAIL_RECIPIENT,
    value: [],
    format: 'email',
    allowMagicString: true,
    multiValue: true,
    minItems: 1,
    required: true,
    refValue: 'case_uuid',
    config: { selectedRecipientType },
  },
  {
    name: 'cc',
    type: fieldTypes.MULTI_VALUE_TEXT,
    format: 'email',
    allowMagicString: true,
    value: [],
    multiValue: true,
    required: false,
    placeholder: 'CC:',
  },
  {
    name: 'bcc',
    type: fieldTypes.MULTI_VALUE_TEXT,
    format: 'email',
    allowMagicString: true,
    value: [],
    multiValue: true,
    required: false,
    placeholder: 'BCC:',
  },
  {
    name: 'subject',
    type: fieldTypes.TEXT,
    value: '',
    required: true,
    placeholder: 'communication:addFields.subject',
  },
  {
    name: 'content',
    type: fieldTypes.TEXTAREA,
    multi: true,
    value: '',
    required: true,
    placeholder: 'communication:addFields.message',
    isMultiline: true,
    rows: 7,
  },
  ...(caseUuid
    ? [
        <
          FormDefinitionField<
            SaveMessageFormValuesType,
            CaseDocumentSelectConfigType
          >
        >{
          name: 'attachments',
          type: fieldTypes.CASE_DOCUMENT_SELECT,
          value: null,
          required: false,
          format: 'file',
          uploadDialog: false,
          multiValue: true,
          placeholder: 'communication:addFields.attachmentsPlaceholder',
          applyBackgroundColor: true,
          config: { caseUuid },
        },
      ]
    : [
        <FormDefinitionField<SaveMessageFormValuesType>>{
          name: 'attachments',
          type: fieldTypes.FILE_SELECT,
          value: null,
          required: false,
          format: 'file',
          uploadDialog: true,
          multiValue: true,
        },
      ]),
];

export default getEmailFormDefinition;
