import { makeStyles } from '@material-ui/core';

export const useMessageFormStyle = makeStyles({
  addFormWrapper: {
    maxWidth: '700px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    fontSize: '14px',
    width: '100%',
    padding: 30,
    margin: '0 auto',
  },
});
