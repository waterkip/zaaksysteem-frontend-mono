import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(
  ({ mintlab: { greyscale, radius } }: any) => ({
    row: {
      display: 'flex',
      flexDirection: 'row',
      paddingTop: 12,
      alignItems: 'center',
      '&:first-child': {
        paddingTop: 0,
      },
    },
    label: {
      flex: '0 1',
      whiteSpace: 'nowrap',
      paddingLeft: 14,
      minWidth: 150,
    },
    field: {
      flex: 1,
    },
    withBackground: {
      borderRadius: radius.defaultFormElement,
      backgroundColor: greyscale.light,
    },
  })
);
