import { ReactElement } from 'react';
import {
  FormDefinition,
  FormValuesType,
} from '@zaaksysteem/common/src/components/form/types/formDefinition.types';

export interface GenericMessageFormPropsType<Values = any> {
  save: (values: Values) => void;
  cancel: () => void;
  busy: boolean;
  enablePreview?: boolean;
  formDefinition: FormDefinition<Values>;
  formName: string;
  mapPreviewValues?: (values: FormValuesType<Values>) => FormValuesType<Values>;
  top?: ReactElement;
  caseUuid?: string;
  primaryButtonLabelKey?: string;
}
