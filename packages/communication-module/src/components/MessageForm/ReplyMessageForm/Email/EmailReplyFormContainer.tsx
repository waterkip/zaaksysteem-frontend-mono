import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import {
  PartialFormValuesType,
  NestedFormValue,
} from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { AJAX_STATE_PENDING } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import {
  saveEmailMessage,
  setAddCommunicationPending,
} from '../../../../store/add/communication.add.actions';
import { CommunicationRootStateType } from '../../../../store/communication.reducer';
import {
  SaveMessageFormValuesType,
  ExternalMessageType,
  ExternalMessageParticipantsType,
} from '../../../../types/Message.types';
import {
  mapToParticipant,
  createReplyParticipants,
} from '../../../../store/library/participants';
import { resolveMagicStrings } from '../../../../library/resolveMagicStrings';
import EmailReplyForm, { EmailReplyFormPropsType } from './EmailReplyForm';

type PropsFromStateType = Pick<
  EmailReplyFormPropsType,
  'busy' | 'initialValues' | 'threadUuid' | 'enablePreview'
>;

type PropsFromDispatchType = Pick<EmailReplyFormPropsType, 'doSave'>;

const createParticipantsValues = (
  participants: ExternalMessageParticipantsType
): PartialFormValuesType<EmailReplyFormPropsType> => {
  return Object.entries(participants).reduce((acc, [key, value]) => {
    return {
      ...acc,
      [key]:
        value && Array.isArray(value)
          ? value.map<NestedFormValue>(({ email, name }) => ({
              value: email,
              label: name || email,
            }))
          : value,
    };
  }, {});
};

const mapStateToProps = ({
  communication: {
    thread: { messages, id },
    add: { state },
    context: { context },
  },
}: CommunicationRootStateType): PropsFromStateType => {
  if (!messages) {
    return {
      threadUuid: id,
      busy: true,
      initialValues: {},
    };
  }

  const firstMessage = messages[0];
  const lastMessage = messages[messages.length - 1] as ExternalMessageType;
  const replyParticipants = lastMessage.participants
    ? createParticipantsValues(
        createReplyParticipants(lastMessage.participants)
      )
    : {};
  const initialValues: PartialFormValuesType<SaveMessageFormValuesType> = {
    subject: (firstMessage as ExternalMessageType).subject
      ? `RE: ${(firstMessage as ExternalMessageType).subject}`
      : '',
    ...replyParticipants,
  };

  return {
    initialValues,
    threadUuid: id,
    enablePreview: context !== 'pip',
    busy: state === AJAX_STATE_PENDING,
  };
};

const mapDispatchToProps = (dispatch: Dispatch): PropsFromDispatchType => ({
  async doSave(caseUuid, threadUuid, values) {
    /*
     * --- START TEMPORARY CODE ---
     * This is temporary until Python can resolve magic string
     */
    dispatch(setAddCommunicationPending());

    const resolvedValues = await resolveMagicStrings<SaveMessageFormValuesType>(
      values,
      values.case_uuid
    );

    /* --- END TEMPORARY CODE --- */

    // Todo: Fix Nullable type in FormValues shape
    const { to, cc, bcc, ...restValues } = resolvedValues as any;
    const action = saveEmailMessage({
      ...restValues,
      to: to.map(mapToParticipant),
      cc: cc.map(mapToParticipant),
      bcc: bcc.map(mapToParticipant),
      message_type: 'email',
      thread_uuid: threadUuid,
      case_uuid: caseUuid,
    });
    dispatch(action as any);
  },
});

export default connect<
  PropsFromStateType,
  PropsFromDispatchType,
  {},
  CommunicationRootStateType
>(
  mapStateToProps,
  mapDispatchToProps
)(EmailReplyForm);
