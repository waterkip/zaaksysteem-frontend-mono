import { connect } from 'react-redux';
import {
  addSourceFileToCase,
  deleteMessage,
} from '../../../../store/thread/communication.thread.actions';
import { CommunicationRootStateType } from '../../../../store/communication.reducer';
import { getNotRelatedOrRelatedCaseNotResolved } from '../../../../store/thread/communication.thread.selectors';
import {
  SubTypeOfMessageOrNone,
  ExternalMessageType,
  AnyMessageType,
} from '../../../../types/Message.types';
import MessageActions, { MessageActionsPropsType } from './MessageActions';

type PropsFromStateType = Pick<
  MessageActionsPropsType,
  'canAddSourceFileToCase' | 'canDeleteMessage' | 'messageType'
>;

const isExternalMessage = (
  message: AnyMessageType | undefined
): message is ExternalMessageType => {
  return Boolean((message as ExternalMessageType)?.messageType);
};

const mapStateToProps = (
  state: CommunicationRootStateType
): PropsFromStateType => {
  const {
    communication: {
      context: {
        capabilities: { canAddSourceFileToCase, canDeleteMessage },
      },
    },
  } = state;
  const notRelatedOrRelatedCaseNotResolved = getNotRelatedOrRelatedCaseNotResolved(
    state
  );
  const [firstMessage] = state.communication.thread.messages || [];
  const messageType = isExternalMessage(firstMessage)
    ? firstMessage.messageType
    : ('none' as SubTypeOfMessageOrNone);

  return {
    canAddSourceFileToCase:
      canAddSourceFileToCase && notRelatedOrRelatedCaseNotResolved,
    canDeleteMessage: canDeleteMessage && notRelatedOrRelatedCaseNotResolved,
    messageType,
  };
};

type PropsFromDispatch = Pick<
  MessageActionsPropsType,
  'onAddSourceFileToCaseAction' | 'onDeleteAction'
>;

const mapDispatchToProps: PropsFromDispatch = {
  onAddSourceFileToCaseAction: addSourceFileToCase,
  onDeleteAction: (messageUuid: string) => deleteMessage(messageUuid),
};

export default connect<
  PropsFromStateType,
  PropsFromDispatch,
  {},
  CommunicationRootStateType
>(
  mapStateToProps,
  mapDispatchToProps
)(MessageActions);
