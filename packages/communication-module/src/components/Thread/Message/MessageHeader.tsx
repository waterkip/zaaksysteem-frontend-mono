import React, { ReactElement } from 'react';
import fecha from 'fecha';
import { useTranslation } from 'react-i18next';
import withWidth, { WithWidth } from '@material-ui/core/withWidth';
//@ts-ignore
import { Body2, Body1, Caption } from '@mintlab/ui/App/Material/Typography';
// @ts-ignore
import Icon from '@mintlab/ui/App/Material/Icon';
import { useMessageHeaderStyle } from './MessageHeader.style';
import MessageActionContainer from './MessageActions/MessageActionsContainer';

const formatDate = (format: string, zulu: string) => {
  const newDate = new Date(zulu);
  return fecha.format(newDate, format);
};

export interface MessageHeaderPropsType {
  id: string;
  date: string;
  title: string;
  isUnread?: boolean;
  subTitle?: string;
  info?: string;
  hasSourceFile?: boolean;
  icon: ReactElement;
  hasAttachment?: boolean;
}

const MessageHeader: React.FunctionComponent<MessageHeaderPropsType &
  WithWidth> = props => {
  const {
    date,
    title,
    subTitle,
    info,
    icon,
    width,
    id,
    hasSourceFile = false,
    hasAttachment = false,
  } = props;
  const classes = useMessageHeaderStyle(props);
  const [t] = useTranslation('communication');
  const dateFormat =
    width === 'xs' ? t('thread.dateFormat') : t('thread.dateFormatLong');

  return (
    <div className={classes.header}>
      {width !== 'xs' && <div className={classes.icon}>{icon}</div>}
      <div className={classes.meta}>
        <div className={classes.title}>
          <Body1 classes={{ root: classes.titleMain }}>{title}</Body1>
          <Body1 classes={{ root: classes.titleSub }}>{subTitle}</Body1>
        </div>
        {Boolean(info) && (
          <Body2 classes={{ root: classes.info }}>{info}</Body2>
        )}
      </div>
      <div className={classes.right}>
        {hasAttachment && <Icon size="small">attachment</Icon>}
        <Caption classes={{ root: classes.date }}>
          {formatDate(dateFormat, date)}
        </Caption>
        <MessageActionContainer id={id} hasSourceFile={hasSourceFile} />
      </div>
    </div>
  );
};

export default withWidth()(MessageHeader);
