import React from 'react';
import { useTranslation } from 'react-i18next';
//@ts-ignore
import { get } from '@mintlab/kitchen-sink/source';
import { NoteMessageType } from '../../../types/Message.types';
import ThreadTitle, { ThreadTitlePropsType } from './ThreadTitle';

type NoteThreadTitlePropsType = {
  message: NoteMessageType;
} & Pick<ThreadTitlePropsType, 'context' | 'showLinkToCase'>;

const NoteThreadTitle: React.FunctionComponent<NoteThreadTitlePropsType> = ({
  message,
  ...rest
}) => {
  const [t] = useTranslation('communication');
  const createdByName = get(message, 'sender.name');
  const title = t('thread.note.title', { createdByName });

  return <ThreadTitle {...rest} title={title} />;
};

export default NoteThreadTitle;
