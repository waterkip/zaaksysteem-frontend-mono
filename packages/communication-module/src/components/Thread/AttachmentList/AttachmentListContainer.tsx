import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { CommunicationRootStateType } from '../../../store/communication.reducer';
import { addAttachmentToCaseAction } from '../../../store/thread/communication.thread.actions';
import { getNotRelatedOrRelatedCaseNotResolved } from '../../../store/thread/communication.thread.selectors';
import { AttachmentPropsType } from './Attachment/Attachment';
import AttachmentList, { AttachmentListPropsType } from './AttachmentList';

type PropsFromStateType = Pick<AttachmentPropsType, 'canBeAddedToCase'> & {
  canOpenPDFPreview: boolean;
};

const mapStateToProps = (
  state: CommunicationRootStateType
): PropsFromStateType => {
  const {
    communication: {
      context: {
        capabilities: { canAddAttachmentToCase, canOpenPDFPreview },
      },
    },
  } = state;

  return {
    canBeAddedToCase:
      canAddAttachmentToCase && getNotRelatedOrRelatedCaseNotResolved(state),
    canOpenPDFPreview,
  };
};

type PropsFromDispatchType = Pick<AttachmentListPropsType, 'addToCase'>;

const mapDispatchToProps = (dispatch: Dispatch): PropsFromDispatchType => ({
  addToCase(payload: string) {
    const action = addAttachmentToCaseAction(payload);

    dispatch(action as any);
  },
});

const AttachmentListContainer = connect<
  PropsFromStateType,
  PropsFromDispatchType,
  {},
  CommunicationRootStateType
>(
  mapStateToProps,
  mapDispatchToProps
)(AttachmentList);

export default AttachmentListContainer;
