import React, { Fragment } from 'react';
import { useTranslation } from 'react-i18next';
//@ts-ignore
import Button from '@mintlab/ui/App/Material/Button';
//@ts-ignore
import Icon from '@mintlab/ui/App/Material/Icon';
import { AttachmentType } from '../../../../types/Message.types';
import ActionMenu, {
  ActionGroupsType,
} from '../../../shared/ActionMenu/ActionMenu';
import { useAttachmentStyle } from './Attachment.style';

export type AttachmentPropsType = AttachmentType & {
  canBeAddedToCase: boolean;
  onAddToCase: () => void;
  handlePreviewAction?: () => void;
};

const Attachment: React.FunctionComponent<AttachmentPropsType> = ({
  name,
  size,
  downloadUrl,
  canBeAddedToCase,
  onAddToCase,
  handlePreviewAction,
}) => {
  const [t] = useTranslation('communication');
  const classes = useAttachmentStyle();
  const addAttachmentAction = canBeAddedToCase
    ? [
        {
          action: onAddToCase,
          label: t('attachments.actions.addToCaseDocuments'),
        },
      ]
    : [];
  const actions: ActionGroupsType = [
    [
      {
        action: () => window.location.assign(downloadUrl),
        label: t('attachments.actions.download'),
      },
      ...addAttachmentAction,
    ],
  ];
  const KB = Math.max(Math.round(size / 1024), 1);
  const MB = Math.max(Math.round(size / 1024 / 1024), 1);
  const fileSize = KB >= 1024 ? `${MB}MB` : `${KB}KB`;

  return (
    <div className={classes.wrapper}>
      <div className={classes.metaWrapper}>
        <Icon classes={{ root: classes.icon }}>attachment</Icon>
        <div className={classes.name}>
          {handlePreviewAction ? (
            <Button className={classes.link} action={handlePreviewAction}>
              {name}
            </Button>
          ) : (
            <Fragment>{name}</Fragment>
          )}
        </div>
        <div className={classes.size}>{fileSize}</div>
      </div>
      <ActionMenu actions={actions} scope="attachment" />
    </div>
  );
};

export default Attachment;
