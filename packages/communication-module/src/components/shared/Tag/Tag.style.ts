import { makeStyles } from '@material-ui/core';

export const useTagStyle = makeStyles(
  //@ts-ignore
  ({ breakpoints, mintlab: { shadows } }) => ({
    wrapper: {
      backgroundColor: '#F5F7F7',
      color: '#637081',
      boxShadow: shadows.flat,
      fontWeight: 'bold',
      padding: '3px 6px',
      width: 'min-content',
      fontSize: 10,
      textTransform: 'uppercase',
      borderRadius: 18,
      whiteSpace: 'nowrap',
      [breakpoints.up('sm')]: {
        fontSize: 12,
        padding: '4px 8px',
      },
    },
  })
);
