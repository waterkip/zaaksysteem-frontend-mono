import { makeStyles } from '@material-ui/core';

export const useThreadPlaceholderStyle = makeStyles(
  ({ mintlab: { greyscale } }: any) => ({
    placeholder: {
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      height: '100%',
      color: greyscale.offBlack,
    },
  })
);
