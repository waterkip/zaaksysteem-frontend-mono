import React from 'react';
import { useThreadPlaceholderStyle } from './ThreadPlaceholder.style';

const ThreadPlaceholder: React.FunctionComponent<{}> = ({ children }) => {
  const classes = useThreadPlaceholderStyle();
  return <div className={classes.placeholder}>{children}</div>;
};

export default ThreadPlaceholder;
