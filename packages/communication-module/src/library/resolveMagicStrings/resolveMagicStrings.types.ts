export type FieldMapType = [string, string][];
export type FlatFormValues<Values> = { [key in keyof Values]?: string };
