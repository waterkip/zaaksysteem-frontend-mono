import { CommunicationContextContextType } from '../types/Context.types';

const getPathToCaseForContext = (
  context: CommunicationContextContextType,
  caseNumber: number
): string => {
  if (context === 'pip') {
    return `/pip/zaak/${caseNumber}`;
  }

  return `/intern/zaak/${caseNumber}`;
};

export default getPathToCaseForContext;
