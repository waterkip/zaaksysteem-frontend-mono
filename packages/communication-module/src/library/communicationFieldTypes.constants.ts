export const CONTACT_FINDER = 'contactFinder';
export const CASE_FINDER = 'caseFinder';
export const CASE_SELECTOR = 'caseSelector';
export const FILE_SELECT = 'fileSelect';
export const EMAIL_RECIPIENT = 'emailRecipient';
