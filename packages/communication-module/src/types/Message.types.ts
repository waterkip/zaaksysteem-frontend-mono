import { APICommunication } from '@zaaksysteem/generated';
import { NestedFormValue } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';

type Direction = 'incoming' | 'outgoing';

export type TypeOfMessage = 'contact_moment' | 'note' | 'external_message';
export type SubTypeOfMessage = 'pip' | 'email';
export type SubTypeOfMessageOrNone = SubTypeOfMessage | 'none';

type NameObject = {
  name: string;
  type: 'person' | 'organization' | 'employee';
};

export type AttachmentType = {
  downloadUrl: string;
  previewUrl?: string;
  id: string;
  name: string;
  size: number;
};

export type ParticipantType = {
  email: string;
  name: string;
};

export type ExternalMessageParticipantsType = {
  from?: ParticipantType;
  to: ParticipantType[];
  cc: ParticipantType[];
  bcc: ParticipantType[];
};

export interface MessageType {
  id: string;
  type: TypeOfMessage;
  content: string;
  createdDate: string;
  summary: string;
  threadUuid: string;
}

export interface ContactMomentMessageType extends MessageType {
  channel: string;
  direction: Direction;
  recipient: NameObject;
  sender: NameObject;
}

export interface NoteMessageType extends MessageType {
  sender: NameObject;
}

export interface ExternalMessageType extends MessageType {
  subject: string;
  attachments: AttachmentType[];
  messageType: SubTypeOfMessage;
  participants?: ExternalMessageParticipantsType;
  sender: NameObject | null;
  hasSourceFile?: boolean;
  readPip: string | null;
  readEmployee: string | null;
}

export type AnyMessageType =
  | ContactMomentMessageType
  | NoteMessageType
  | ExternalMessageType;

export type CaseChoiceType = {
  value: string;
  label: string;
  subLabel: string | null;
};

export type SaveMessageFormValuesType = Pick<
  APICommunication.CreateExternalMessageRequestBody,
  'content' | 'subject' | 'case_uuid'
> &
  Partial<
    Pick<APICommunication.CreateExternalMessageRequestBody, 'thread_uuid'>
  > & {
    to: NestedFormValue[];
    cc: NestedFormValue[];
    bcc: NestedFormValue[];
    attachments: NestedFormValue | NestedFormValue[];
    requestor: NestedFormValue;
  };

export type SaveContactMomentFormValuesType = Pick<
  APICommunication.CreateContactMomentRequestBody,
  'content' | 'channel' | 'direction' | 'case_uuid'
> & { contact_uuid: NestedFormValue };

export type SaveNoteFormValuesType = Pick<
  APICommunication.CreateNoteRequestBody,
  'content'
> &
  ({ case_uuid: string } | { contact_uuid: string });

export type ImportMessageFormFields = Pick<
  APICommunication.ImportEmailMessageRequestBody,
  'file_uuid'
>;
