import { getItemsToRender } from './functions';

const THREE = 3;

describe('The `getItemsToRender` function', () => {
  const items = [
    {
      label: 'Home',
      path: '/',
    },
    {
      label: 'Level 1',
      path: '/level',
    },
    {
      label: 'Level 2',
      path: '/level/level',
    },
    {
      label: 'Level 3',
      path: '/level/level/level',
    },
    {
      label: 'Level 4',
      path: '/level/level/level/level',
    },
  ];

  test('limits the amount of items to 3', () => {
    expect(getItemsToRender(items, THREE)).toHaveLength(THREE);
  });

  test('changes the label of the middle item to `...`', () => {
    const [, middle] = getItemsToRender(items, THREE);
    expect(middle.label).toEqual('...');
  });
});
