import { makeStyles } from '@material-ui/core';

export const useStepperStyles = makeStyles(({ mintlab: { shadows } }: any) => ({
  root: {
    justifyContent: 'space-between',
    borderRadius: 50,
    padding: 12,
    boxShadow: shadows.flat,
  },
}));
