import React from 'react';
import { default as MUIStepper, StepperProps } from '@material-ui/core/Stepper';
import Step, { StepProps } from '@material-ui/core/Step';
import StepLabel, { StepLabelProps } from '@material-ui/core/StepLabel';
import { useStepperStyles } from './Stepper.style';

export type StepType = Pick<StepProps, 'active' | 'completed' | 'disabled'> &
  Pick<StepLabelProps, 'error'> & {
    name: string;
  };

export type StepperPropsType = Omit<StepperProps, 'children'> & {
  steps: StepType[];
};

export const Stepper: React.ComponentType<StepperPropsType> = ({
  activeStep,
  steps,
}) => {
  const classes = useStepperStyles();

  return (
    <MUIStepper
      activeStep={activeStep}
      connector={null}
      classes={{ root: classes.root }}
    >
      {steps.map(step => {
        return (
          <Step
            key={step.name}
            active={step.active}
            completed={step.completed}
            disabled={step.disabled}
          >
            <StepLabel error={step.error}>{step.name}</StepLabel>
          </Step>
        );
      })}
    </MUIStepper>
  );
};
