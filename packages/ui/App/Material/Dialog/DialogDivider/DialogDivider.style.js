/**
 * @return {JSS}
 */

export const dialogDividerStyleSheet = () => ({
  root: {
    margin: '1px 0px',
  },
});
