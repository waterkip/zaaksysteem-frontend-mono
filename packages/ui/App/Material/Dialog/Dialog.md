# 🔌 `Dialog` component

> *Material Design* **Dialog**.

## See also

- [`Dialog` stories](/npm-mintlab-ui/storybook/?selectedKind=Material/Dialog)
- [`Dialog` API reference](/npm-mintlab-ui/documentation/consumer/identifiers.html#material-dialog)

## External resources

- [*Material Guidelines* for Dialogs](https://material.io/guidelines/components/dialogs.html)
- [*Material-UI* `Dialog` API](https://material-ui.com/api/dialog/)
