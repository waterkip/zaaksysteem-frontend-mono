import React from 'react';
import MUIDialogContent from '@material-ui/core/DialogContent';
import { withStyles } from '@material-ui/styles';
import { dialogContentStyleSheet } from './DialogContent.style';

/**
 * *Material Design* **DialogContent**.
 * - facade for *Material-UI* `DialogActions`
 *
 * @param {Object} props
 * @param {boolean} [props.padded]
 * @param {Array} [props.children]
 * @return {ReactElement}
 */
export const DialogContent = ({ children, classes, padded = true }) => (
  <MUIDialogContent
    classes={{
      root: padded ? classes.padded : classes.root,
    }}
  >
    {children}
  </MUIDialogContent>
);

export default withStyles(dialogContentStyleSheet)(DialogContent);
