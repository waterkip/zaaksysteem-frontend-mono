import { React, stories } from '../../story';
import { H1, H3, Caption, Body1 } from '../Typography';
import { palette } from './library/theme';

stories(module, __dirname, {
  ColorPalette() {
    const ColorPaletteItem = ({ name, color }) => (
      <li
        style={{
          display: 'flex',
          alignItems: 'center',
        }}
      >
        <span
          style={{
            backgroundColor: color,
            width: 80,
            height: 55,
            marginRight: 10,
          }}
        />
        <span>
          <Body1>{name}</Body1>
          <Caption>{color}</Caption>
        </span>
      </li>
    );
    return (
      <div>
        <H1>Color palette</H1>
        <div
          style={{
            display: 'flex',
            flexWrap: 'wrap',
          }}
        >
          {Object.entries(palette).map(([name, value]) => {
            return (
              <div key={name} style={{ padding: 20, minWidth: 150 }}>
                <H3>{name}</H3>
                <ul
                  style={{
                    listStyle: 'none',
                    padding: 0,
                    margin: 0,
                  }}
                >
                  {Object.entries(value).map(([colorName, color]) => {
                    return (
                      <ColorPaletteItem
                        key={`${name}.${colorName}`}
                        name={colorName}
                        color={color}
                      />
                    );
                  })}
                </ul>
              </div>
            );
          })}
        </div>
      </div>
    );
  },
});
