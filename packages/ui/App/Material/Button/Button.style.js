/**
 * Style Sheet for the {@link Button} component custom `semiContained` variant.
 *
 * @param {Object} theme
 * @return {JSS}
 */
export const buttonSemiContained = ({
  palette: { common, primary, secondary },
}) => ({
  contained: {
    color: common.black,
    background: common.white,
    '&:hover': {
      background: common.white,
    },
  },
  containedPrimary: {
    color: primary.main,
    background: common.white,
  },
  containedSecondary: {
    color: secondary.main,
    background: common.white,
  },
});
