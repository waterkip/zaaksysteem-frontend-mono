import React from 'react';
import Typography from '@material-ui/core/Typography';

/**
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/Typography&selectedStory=Subtitle1
 *
 * @param {Object} props
 * @param {*} props.children
 * @param {Object} [props.classes]
 * @return {ReactElement}
 */
export const Subtitle1 = ({ children, classes }) => (
  <Typography variant="subtitle1" classes={classes}>
    {children}
  </Typography>
);
