import React from 'react';
import toJson from 'enzyme-to-json';
import { shallow } from 'enzyme';
import { Fab } from './Fab';

/**
 * @test {Fab}
 */
describe('The `Fab` component', () => {
  test('renders correctly', () => {
    const component = shallow(
      <Fab
        aria-label={'ariaLabel'}
        color="primary"
        action={() => {}}
        scope={'fab'}
      >
        add
      </Fab>
    );

    expect(toJson(component)).toMatchSnapshot();
  });
});
