# 🔌 `TextField` component

> *Material Design* **Text field**.

## Variants

### Form

TextField for use in Forms, with the Material UI look.

The default export from `TextField`.

### Generic

A simplified TextField with generic styling.

Exports as `GenericTextField` from `TextField`.

## See also

- [`TextField` stories](/npm-mintlab-ui/storybook/?selectedKind=Material/TextField)
- [`TextField` API reference](/npm-mintlab-ui/documentation/consumer/identifiers.html#material-textfield)

## External resources

- [*Material Guidelines*: Text fields](https://material.io/design/components/text-fields.html)
- [*Material-UI* `TextField` API](https://material-ui.com/api/text-field/) 
