import React from 'react';
import { addScopeAttribute } from '../../../library/addScope';
import style from './Pulse.module.css';

/**
 * Spinner type for the {@link Loader} component
 * @param {Object} props
 * @param {string} props.color
 * @return {ReactElement}
 */
export const Pulse = ({ color, scope }) => (
  <div
    className={style['pulse']}
    style={{
      backgroundColor: color,
    }}
    {...addScopeAttribute(scope)}
  />
);
