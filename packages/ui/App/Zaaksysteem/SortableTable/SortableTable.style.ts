import { makeStyles } from '@material-ui/core';

const overflowEllipsis = {
  'white-space': 'nowrap',
  overflow: 'hidden',
  textOverflow: 'ellipsis',
};

export const useSortableTableStyles = makeStyles(
  ({
    palette: { primary, basalt },
    typography,
    mintlab: { greyscale },
  }: any) => ({
    flexContainer: {
      display: 'flex',
      alignItems: 'center',
      boxSizing: 'border-box',
    },
    tableHeader: {
      fontWeight: typography.fontWeightBold,
      color: basalt.lightest,
    },
    tableRow: {
      borderBottom: `1px solid ${greyscale.dark}`,
      paddingLeft: 10,
    },
    tableRowHover: {
      '&:hover': {
        cursor: 'pointer',
        backgroundColor: primary.lightest,
      },
    },
    tableCell: {
      padding: 10,
    },
    tableCellHeader: {
      display: 'flex',
      alignItems: 'center',
      '& >* svg': {
        marginLeft: 10,
      },
      '&:hover': {
        cursor: 'pointer',
      },
      textTransform: 'none',
    },
    tableCellFixedHeight: {
      ...overflowEllipsis,
      '& *': {
        ...overflowEllipsis,
      },
    },
    tableCellFirst: {
      paddingLeft: 0,
    },
    sortAsc: {
      transform: 'rotate(-90deg)',
    },
    sortDesc: {
      transform: 'rotate(90deg)',
    },
    noRowsMessage: {
      padding: 20,
    },
    selectCell: {
      paddingLeft: 12,
      overflow: 'visible',
    },
    rowSelected: {
      backgroundColor: primary.lightest,
    },
    loader: {
      marginLeft: 20,
    },
  })
);
