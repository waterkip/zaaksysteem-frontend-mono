/**
 * Style Sheet for the {@link ChoiceChips} component
 *
 * @param {Object} theme
 * @return {JSS}
 */
export const choiceChipsStylesheet = () => ({
  list: {
    display: 'flex',
    padding: 0,
    margin: 0,
    flexWrap: 'wrap',
    marginTop: -20,
  },
  listItem: {
    padding: 0,
    listStyleType: 'none',
    margin: '20px 20px 0px 0px',
  },
});
