import React, { Fragment } from 'react';
import MuiButton from '@material-ui/core/Button';
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import { withStyles } from '@material-ui/styles';
import Icon from '../../../Material/Icon';
import { addScopeAttribute } from '../../../library/addScope';
import { dropdownMenuButtonStylesheet } from './DropdownMenuButton.style';

/**
 * @param {Action} props
 * @param {Function} props.action
 * @param {string} props.icon
 * @param {string} props.label
 * @param {Object} props.classes
 * @param {string} props.scope
 * @return {ReactElement}
 */
const DropdownMenuButton = React.forwardRef(
  ({ action, icon, label, classes, scope, key }, ref) => {
    const labelComponent = icon ? (
      <Fragment>
        <Icon size="small">{icon}</Icon>
        {label}
      </Fragment>
    ) : (
      label
    );

    return (
      <MuiButton
        key={key}
        ref={ref}
        onClick={action}
        classes={cloneWithout(classes, 'wrapper')}
        fullWidth={false}
        {...addScopeAttribute(scope, 'button')}
      >
        {labelComponent}
      </MuiButton>
    );
  }
);

DropdownMenuButton.displayName = 'DropdownMenuButton';

export default withStyles(dropdownMenuButtonStylesheet)(DropdownMenuButton);
