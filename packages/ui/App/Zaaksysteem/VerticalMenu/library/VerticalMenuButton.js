import React, { Fragment } from 'react';
import classNames from 'classnames';
import MuiButton from '@material-ui/core/Button';
import { withStyles } from '@material-ui/styles';
import Icon from '../../../Material/Icon';
import { addScopeAttribute } from '../../../library/addScope';
import { VerticalButtonStylesheet } from './VerticalMenuButton.style';

/**
 * @param {Action} props
 * @param {Object} props.classes
 * @param {Function} props.action
 * @param {string} props.icon
 * @param {string} props.label
 * @param {boolean} props.active
 * @return {ReactElement}
 */
export const VerticalMenuButton = ({
  classes,
  action,
  icon,
  label,
  active,
  scope,
}) => {
  const children = icon ? (
    <Fragment>
      <Icon
        size="small"
        classes={{
          root: classes.icon,
        }}
      >
        {icon}
      </Icon>
      {label}
    </Fragment>
  ) : (
    label
  );

  return (
    <div>
      <MuiButton
        onClick={action}
        classes={{
          root: classNames(classes.root, {
            [classes.active]: active,
          }),
          label: classes.label,
        }}
        {...addScopeAttribute(scope, 'button')}
      >
        {children}
      </MuiButton>
    </div>
  );
};

export default withStyles(VerticalButtonStylesheet)(VerticalMenuButton);
