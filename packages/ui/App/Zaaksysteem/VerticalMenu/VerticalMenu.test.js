import React from 'react';
import toJson from 'enzyme-to-json';
import { shallow } from 'enzyme';
import { VerticalMenu } from './VerticalMenu';

/**
 * @test {VerticalMenu}
 */
describe('The `VerticalMenu` component', () => {
  test('renders correctly', () => {
    const items = [{ label: 'foo' }, { label: 'bar' }];
    const component = shallow(<VerticalMenu items={items} classes={{}} />);

    expect(toJson(component)).toMatchSnapshot();
  });
});
