import React from 'react';
import classNames from 'classnames';
import ButtonBase from '@material-ui/core/ButtonBase';
import { withStyles } from '@material-ui/styles';
import { addScopeProp } from '../../../library/addScope';
import { bannerButtonStylesheet } from './BannerButton.style';

/**
 * @param {Object} props
 * @param {Object} props.classes
 * @param {Function} props.action
 * @param {ReactElement} props.children
 * @param {string} props.variant
 * @param {string} [props.scope]
 * @return {ReactElement}
 */
export const BannerButton = ({ classes, action, children, variant, scope }) => (
  <ButtonBase
    classes={{
      root: classNames(classes.root, { [classes[`variant-${variant}`]]: true }),
    }}
    onClick={action}
    {...addScopeProp(scope, 'button')}
  >
    {children}
  </ButtonBase>
);

export default withStyles(bannerButtonStylesheet)(BannerButton);
