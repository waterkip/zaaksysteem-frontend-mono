// TO-DO get backgroundColors from theme, once the grid palette is implemented
const getPresets = ({
  palette: { primary, secondary, vivid, error, bluebird, coral, lizard },
}) => {
  const defaultFile = {
    name: 'insert_drive_file',
    color: primary.main,
  };

  return {
    channel: {
      frontdesk: {
        name: 'room_service',
        backgroundColor: '#EFC16A',
      },
      assignee: {
        name: 'person',
        backgroundColor: '#FF0000',
      },
      email: {
        name: 'alternate_email',
        backgroundColor: '#B2749F',
      },
      mail: {
        name: 'drafts',
        backgroundColor: '#6635C1',
      },
      phone: {
        name: 'phone',
        backgroundColor: '#23BA86',
      },
      social_media: {
        name: 'thumb_up',
        backgroundColor: '#3B5998',
      },
      webform: {
        name: 'format_list_bulleted',
        backgroundColor: '#337FCB',
      },
    },
    threadType: {
      contactMoment: {
        name: 'chat_bubble',
        backgroundColor: 'green',
      },
      email: {
        name: 'email',
        backgroundColor: primary.main,
      },
      emailThread: {
        name: 'email_conversation',
        backgroundColor: primary.main,
      },
      note: {
        name: 'notes',
        backgroundColor: '#EBC32F',
        color: '#000000',
      },
      pipMessage: {
        name: 'email',
        backgroundColor: primary.main,
      },
      pipMessageThread: {
        name: 'email_conversation',
        backgroundColor: primary.main,
      },
    },
    entityType: {
      person: {
        name: 'person',
        backgroundColor: secondary.main,
      },
      organization: {
        name: 'domain',
        backgroundColor: primary.dark,
      },
      employee: {
        name: 'domain',
        backgroundColor: primary.dark,
      },
    },
    file: {
      error: {
        name: 'insert_drive_file',
        color: error.main,
      },
      pdf: {
        name: 'insert_drive_file',
        color: coral.dark,
      },
      png: {
        name: 'image',
        color: primary.light,
      },
      jpg: {
        name: 'image',
        color: primary.light,
      },
      doc: {
        name: 'insert_drive_file',
        color: bluebird.main,
      },
      docx: {
        name: 'insert_drive_file',
        color: bluebird.main,
      },
      ppt: {
        name: 'insert_drive_file',
        color: coral.main,
      },
      pptx: {
        name: 'insert_drive_file',
        color: coral.main,
      },
      xls: {
        name: 'insert_drive_file',
        color: lizard.main,
      },
      xlsx: {
        name: 'insert_drive_file',
        color: lizard.main,
      },
      default: defaultFile,
    },
    itemType: {
      folder: {
        name: 'folder',
        color: vivid[2],
      },
      file: defaultFile,
    },
  };
};

export default getPresets;
