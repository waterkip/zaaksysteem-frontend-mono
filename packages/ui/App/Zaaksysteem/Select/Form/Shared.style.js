import backgroundColorOption from '../library/backgroundColorOption';

/**
 * @param {Object} parameters
 * @param {Object} parameters.theme
 * @param {string} parameters.error
 * @param {boolean} parameters.focus
 * @return {JSS}
 */
export const selectStyleSheet = ({
  theme: {
    palette: { primary, common, text, error: errorPalette },
    typography: { fontFamily, body2 },
    zIndex,
    mintlab: { greyscale, radius },
  },
  error,
  focus,
}) => {
  const borderBottom = () => {
    if (focus) {
      return `2px solid ${error ? errorPalette.main : primary.main}`;
    }
  };

  const backgroundColor = () => {
    if (error && !focus) {
      return errorPalette.light;
    }

    return 'transparent';
  };

  const colorRules = {
    color: greyscale.offBlack,
    '&:hover': {
      color: greyscale.offBlack,
    },
  };

  return {
    container(base) {
      return {
        ...base,
        width: '100%',
        fontFamily,
      };
    },
    control(base) {
      return {
        ...base,
        boxShadow: 'none',
        border: 'none',
        borderBottom: borderBottom(),
        '&:hover': {
          borderBottom: borderBottom(),
        },
        backgroundColor: backgroundColor(),
        borderColor: 'transparent',
      };
    },
    valueContainer(base) {
      return {
        ...base,
        padding: '11px 14px',
        backgroundColor: backgroundColor(),
      };
    },
    option(base, state) {
      return {
        ...base,
        backgroundColor: backgroundColorOption({
          state,
          primary,
          common,
          greyscale,
        }),
        color: text.primary,
      };
    },
    input(base) {
      return {
        ...base,
        input: {
          fontFamily,
        },
      };
    },
    multiValue(base) {
      return {
        ...base,
        borderRadius: radius.chip,
        padding: '2px',
      };
    },
    multiValueRemove(base) {
      return {
        ...base,
        color: greyscale.darkest,
        backgroundColor: greyscale.darker,
        borderRadius: radius.chipRemove,
        transform: 'scale(0.85)',
      };
    },
    menu(base) {
      return {
        ...base,
        ...body2,
        zIndex: zIndex.options,
      };
    },
    // Indicators
    indicatorsContainer(base) {
      return {
        ...base,
        padding: '6px',
      };
    },
    dropdownIndicator(base) {
      return {
        ...base,
        ...colorRules,
        padding: 0,
      };
    },
    clearIndicator(base) {
      return {
        ...base,
        ...colorRules,
        backgroundColor: backgroundColor(),
        padding: 0,
      };
    },
    indicatorSeparator(base) {
      return {
        ...base,
        display: 'none',
      };
    },
  };
};

export default selectStyleSheet;
