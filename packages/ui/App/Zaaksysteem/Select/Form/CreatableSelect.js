import React, { Component } from 'react';
import ReactSelectCreatable from 'react-select/lib/Creatable';
import { withTheme } from '@material-ui/styles';
import {
  bind,
  callOrNothingAtAll,
  cloneWithout,
} from '@mintlab/kitchen-sink/source';
import ClearIndicator from '../library/ClearIndicator';
import filterOptionFunction from '../library/filterOption';
import selectStyleSheet from './Shared.style';

const MSG_CREATABLE = 'Typ, en <ENTER> om te bevestigen.';
const MSG_CREATE = 'Aanmaken:';

/**
 * Facade for Creatable React Select v2.
 * - additional props will be passed to that component
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Zaaksysteem/CreatableSelect
 * @see /npm-mintlab-ui/documentation/consumer/manual/CreatableSelect.html
 * @see https://react-select.com/home
 *
 * @reactProps {string} error
 * @reactProps {Function} filterOption
 * @reactProps {string} name
 * @reactProps {Function} onChange
 * @reactProps {Object} translations
 * @reactProps {Object|Array} value
 * @reactProps {Object} styles
 * @reactProps {Object} components
 * @reactProps {Function} [onChange]
 * @reactProps {boolean} [disabled=false]
 * @reactProps {boolean} [createOnBlur=false]
 */
export class CreatableSelect extends Component {
  /**
   * @ignore
   */
  static get defaultProps() {
    return {
      translations: {
        'form:creatable': MSG_CREATABLE,
        'form:create': MSG_CREATE,
      },
      disabled: false,
      filterOption: filterOptionFunction,
    };
  }

  /**
   * @param {Object} props
   */
  constructor(props) {
    super(props);
    this.state = { focus: false };
    bind(this, 'handleBlur', 'handleChange', 'handleFocus');
  }

  // Lifecycle methods:

  /**
   * @see https://reactjs.org/docs/react-component.html#render
   *
   * @return {ReactElement}
   */
  render() {
    const {
      props: {
        error,
        translations,
        theme,
        styles,
        disabled,
        value,
        filterOption,
        ...rest
      },
      state: { focus },
      handleChange,
      handleBlur,
      handleFocus,
    } = this;

    const createLabel = input =>
      [translations['form:create'], ' ', input].join('');

    return (
      <ReactSelectCreatable
        error={Boolean(error)}
        isDisabled={disabled}
        isMulti={true}
        components={{
          DropdownIndicator: null,
          ClearIndicator,
        }}
        value={value}
        onChange={handleChange}
        onBlur={handleBlur}
        onFocus={handleFocus}
        placeholder={translations['form:creatable']}
        formatCreateLabel={createLabel}
        noOptionsMessage={() => null}
        styles={
          styles ||
          selectStyleSheet({
            theme,
            error,
            focus,
          })
        }
        filterOption={filterOption}
        {...cloneWithout(
          rest,
          'components',
          'config',
          'name',
          'createOnBlur',
          'value',
          'onBlur',
          'onChange',
          'onFocus',
          'onBlur'
        )}
      />
    );
  }

  // Custom methods:

  /**
   * Call form change function, if provided
   * @param {*} value
   */
  handleChange(value) {
    const { onChange, name } = this.props;

    callOrNothingAtAll(onChange, () => [
      {
        target: {
          name,
          value,
          type: 'creatable',
        },
      },
    ]);
  }

  handleBlur(event) {
    const currentValue = event.target.value;
    const { onBlur, name } = this.props;

    if (currentValue && this.props.createOnBlur) {
      this.setValueAfterBlur(currentValue);
    }

    this.setState({ focus: false });
    callOrNothingAtAll(onBlur, [
      {
        target: {
          name,
        },
      },
    ]);
  }

  handleFocus() {
    this.setState({ focus: true });
  }

  setValueAfterBlur(value) {
    const itemToSet = {
      label: value,
      value,
    };
    const newValue = this.props.isMulti
      ? [...this.props.value, itemToSet]
      : itemToSet;

    this.handleChange(newValue);
  }
}

export default withTheme(CreatableSelect);
