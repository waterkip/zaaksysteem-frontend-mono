import React, { Suspense } from 'react';
import Loader from '../Loader';
/**
 * Create a single `react-select` based chunk.
 * Choosing the actual variant is delegated to `SelectStrategy`.
 *
 * Depends on {@link LazyLoader}.
 *
 * @param {Object} props
 * @return {ReactElement}
 */
const LazySelect = React.lazy(() =>
  import(
    // https://webpack.js.org/api/module-methods/#import
    /* webpackChunkName: "ui.select" */
    './library/SelectStrategy'
  )
);

export const Select = props => (
  <Suspense fallback={<Loader />}>
    <LazySelect {...props} />
  </Suspense>
);

export default Select;
