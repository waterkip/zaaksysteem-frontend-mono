import React from 'react';
import { components } from 'react-select';
import CloseIndicator from '../../../Shared/CloseIndicator/CloseIndicator';

/**
 * @param {*} props
 * @return {ReactElement}
 */
const ClearIndicator = props => (
  <components.ClearIndicator {...props}>
    <CloseIndicator />
  </components.ClearIndicator>
);

export default ClearIndicator;
