import React from 'react';
import Dropzone from 'react-dropzone';
import classnames from 'classnames';
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import { withStyles } from '@material-ui/styles';
import Render from '../../../Abstract/Render/Render';
import Button from '../../../Material/Button/Button';
import { Subtitle1, Caption } from '../../../Material/Typography';
import { addScopeProp } from '../../../library/addScope';
import { fileSelectStylesheet } from './FileSelect.style';

/* eslint complexity: [2, 5] */
export const FileSelect = ({
  selectInstructions,
  dragInstructions,
  dropInstructions,
  orLabel,
  onDrop,
  classes,
  fileList,
  hasFiles,
  error,
  scope,
  accept,
  multiValue = true,
}) => (
  <Dropzone onDrop={onDrop} accept={accept}>
    {({ getRootProps, getInputProps, isDragActive, open }) => {
      const inputProps = getInputProps({
        multiple: multiValue,
      });
      const rootProps = getRootProps();

      return (
        <div
          className={classnames(
            classes.wrapper,
            Boolean(error) && classes.error
          )}
          {...cloneWithout(rootProps, 'onClick', 'tabIndex')}
        >
          <input {...inputProps} />

          <div className={classes.fileList}>{fileList}</div>

          <div>
            <div className={classes.form}>
              <Render condition={!hasFiles}>
                <Subtitle1>
                  {isDragActive ? dropInstructions : dragInstructions}
                </Subtitle1>
                <div className={classes.orLabel}>
                  <Caption>- {orLabel} -</Caption>
                </div>
              </Render>
              <Render condition={!hasFiles || multiValue}>
                <Button
                  presets={['primary', 'semiContained']}
                  action={open}
                  {...addScopeProp(scope, 'file-select', 'select')}
                >
                  {selectInstructions}
                </Button>
              </Render>
            </div>
          </div>
        </div>
      );
    }}
  </Dropzone>
);

export default withStyles(fileSelectStylesheet)(FileSelect);
