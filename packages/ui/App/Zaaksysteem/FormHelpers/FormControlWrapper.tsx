import React, { Fragment } from 'react';
import { addScopeAttribute } from '../../library/addScope';
import { useFormControlWrapperStylesheet } from './FormControlWrapper.style';
import Tooltip from '../../Material/Tooltip/Tooltip';
import Render from '../../Abstract/Render/Render';
import InfoIcon from './library/InfoIcon';
import ErrorLabel from './library/ErrorLabel';
//@ts-ignore
import { Subtitle1, Caption } from '../../Material/Typography';
import classNames from 'classnames';

/**
 * Wraps children (usually an input component) with:
 *
 * - A title label
 * - A help tooltip (if provided)
 * - A hint label (if provided)
 * - An error label (if provided)
 *
 * Displays this in either a wide (3 column, default) or
 * compact (stacked) layout.
 */
export type FormControlWrapperPropsType = {
  error?: string;
  help?: string;
  hint?: string;
  label?: string;
  required?: boolean;
  compact?: boolean;
  scope?: string;
  touched?: boolean;
  applyBackgroundColor?: boolean;
};

export const FormControlWrapper: React.ComponentType<FormControlWrapperPropsType> = ({
  children,
  error,
  help,
  scope,
  hint = '',
  label = '',
  required = false,
  compact = false,
  touched = false,
  applyBackgroundColor = true,
}) => {
  const classes = useFormControlWrapperStylesheet();
  const elHelp = (
    <div className={classes.help}>
      <Render condition={Boolean(help)}>
        <Tooltip title={help} type="info">
          <InfoIcon />
        </Tooltip>
      </Render>
    </div>
  );

  const elLabel = label && (
    <div className={compact ? classes.labelCompact : classes.label}>
      <Subtitle1>
        {label}
        <Render condition={required}>
          <sup>﹡</sup>
        </Render>
      </Subtitle1>
    </div>
  );

  const elHint = (
    <Render condition={Boolean(hint)}>
      <div className={classes.hint}>
        <Caption
          classes={{
            root: classes.hint,
          }}
        >
          {hint}
        </Caption>
      </div>
    </Render>
  );

  const elError = (
    <Render condition={Boolean(error && touched)}>
      <div className={classes.error}>
        <ErrorLabel label={error as string} />
      </div>
    </Render>
  );

  const elControl = (
    <div
      className={classNames(classes.control, {
        [classes.controlBackgroundColor]: applyBackgroundColor,
      })}
    >
      {children}
    </div>
  );

  const wrap = (content: React.ReactNode) => (
    <div
      className={classes.wrapper}
      {...addScopeAttribute(scope, 'formControlWrapper')}
    >
      {content}
    </div>
  );

  if (compact) {
    return wrap(
      <Fragment>
        {elLabel}
        {elHelp}
        {elHint}
        {elControl}
        {elError}
      </Fragment>
    );
  }

  return wrap(
    <Fragment>
      <div className={classes.colLabels}>
        {elLabel} {elHint}
      </div>
      <div className={classes.colContent}>
        {elControl}
        {elError}
      </div>
      <div className={classes.colHelp}>{elHelp}</div>
    </Fragment>
  );
};

export default FormControlWrapper;
