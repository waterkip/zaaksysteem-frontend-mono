import React from 'react';
import classNames from 'classnames';
import MuiAppBar from '@material-ui/core/AppBar';
import MuiToolbar from '@material-ui/core/Toolbar';
import Divider from '@material-ui/core/Divider';
import { withStyles } from '@material-ui/styles';
import Button from '../../../Material/Button';
import { Subtitle2 } from '../../../Material/Typography';
import { addScopeProp, addScopeAttribute } from '../../../library/addScope';
import DropdownMenu, { DropdownMenuList } from '../../DropdownMenu';
import { appBarStyleSheet } from './AppBar.style';

/**
 * *Material Design* **AppBar**.
 * - facade for *Material-UI* `AppBar` and `ToolBar`
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/AppBar
 * @see /npm-mintlab-ui/documentation/consumer/manual/AppBar.html
 *
 * @param {Object} props
 * @param {Object} props.classes
 * @param {*} props.children
 * @param {string} props.className
 * @param {number} props.gutter
 *   Horizontal icon button gutter
 * @param {string} props.menuLabel
 * @param {Function} props.onMenuClick
 * @param {string} props.userLabel
 * @param {Array} props.userActions
 * @param {Array} [props.searchComponent]
 * @return {ReactElement}
 */
export const AppBar = ({
  classes,
  children,
  className,
  gutter,
  menuLabel,
  onMenuClick,
  userLabel,
  userName,
  userActions,
  scope,
  searchComponent,
}) => (
  <MuiAppBar
    className={className}
    color="inherit"
    position="static"
    classes={{
      root: classNames(className, classes.appBar),
    }}
    {...addScopeAttribute(scope, 'app-bar')}
  >
    <MuiToolbar
      disableGutters={true}
      classes={{
        root: classes.toolbar,
      }}
    >
      <div
        style={{
          padding: `0 ${gutter}px`,
        }}
      >
        <Button
          action={onMenuClick}
          label={menuLabel}
          presets={['icon', 'small']}
          {...addScopeProp(scope, 'app-bar', 'menu-toggle')}
        >
          menu
        </Button>
      </div>
      <div className={classes.content}>
        <Subtitle2>{children}</Subtitle2>
        <div className={classes.searchContainer}>{searchComponent}</div>
      </div>
      <div
        style={{
          padding: `0 ${gutter}px`,
        }}
      >
        <DropdownMenu
          trigger={
            <Button
              label={userLabel}
              presets={['icon', 'small']}
              {...addScopeProp(scope, 'app-bar', 'user-menu')}
            >
              face
            </Button>
          }
        >
          <Subtitle2
            classes={{
              root: classes.userName,
            }}
          >
            {userName}
          </Subtitle2>
          <Divider />
          <DropdownMenuList
            items={userActions}
            {...addScopeProp(scope, 'app-bar', 'user-menu')}
          />
        </DropdownMenu>
      </div>
    </MuiToolbar>
  </MuiAppBar>
);

export default withStyles(appBarStyleSheet)(AppBar);
