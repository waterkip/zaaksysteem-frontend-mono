import React from 'react';

const Foo = ({ title }) => <h1>{title}</h1>;

export default Foo;
