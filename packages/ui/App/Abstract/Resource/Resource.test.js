import React from 'react';
import toJson from 'enzyme-to-json';
import { shallow } from 'enzyme';
import Resource from './Resource';

describe('The `Resource` component', () => {
  test('renders correctly', () => {
    const component = shallow(
      <Resource
        id="foo"
        payload={{
          foo: null,
        }}
        resolve={() => {}}
      >
        <div />
      </Resource>
    );

    expect(toJson(component)).toMatchSnapshot();
  });
});
