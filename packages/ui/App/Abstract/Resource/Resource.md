# 🔌 `Resource` component

> Declarative resource component.

## Usage

- provide a *resource ID* or an array of *resource ID*s
- wrap with a component that connects the `payload` data 
  and the `resolve` method to a store
- passes the resolved data with a `resource` property 
  to the child component

## Example

    const ResultView = ({
      resource: {
        foo,
      },
    }) => (
      <ul>
        {foo.map(item, index) => (
          <li key={index}>
            {item}
          </li>
        )}
      </ul>
    );
    
    // ...
    
    <Resource
     id="foo"
     resolve={someApi.getData}
    >
      <ResultView/>
    </Resource>
    

## See also 

- [`Resource` stories](/npm-mintlab-ui/storybook/?selectedKind=Abstract/Resource)
- [`Resource` API reference](/npm-mintlab-ui/documentation/consumer/identifiers.html#abstract-resource)
