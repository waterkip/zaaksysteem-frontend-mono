import React from 'react';
import { useErrorDialog } from '@zaaksysteem/common/src/hooks/useErrorDialog';
import { TranslatedErrorMessage } from '@zaaksysteem/common/src/library/request/TranslatedErrorMessage';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';

type ReturnType = [React.ReactChild, (errorObj: V2ServerErrorsType) => void];

const useServerErrorDialog = (): ReturnType => {
  const [errorDialog, openErrorDialog] = useErrorDialog();

  const openServerErrorDialog = (errorObj: V2ServerErrorsType) => {
    openErrorDialog({
      message: <TranslatedErrorMessage errorObj={errorObj} />,
    });
  };

  return [errorDialog, openServerErrorDialog];
};

export default useServerErrorDialog;
