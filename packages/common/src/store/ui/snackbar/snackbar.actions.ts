import { Action } from 'redux';
import { ActionWithPayload } from '@zaaksysteem/common/src/types/ActionWithPayload';
import { SHOW_SNACKBAR, HIDE_SNACKBAR } from './snackbar.constants';

export interface ShowSnackbarActionPayload {
  messageId: string;
}

export interface ShowSnackbarAction
  extends ActionWithPayload<ShowSnackbarActionPayload> {}

type ShowSnackbar = (messageId: string) => ShowSnackbarAction;

export const showSnackbar: ShowSnackbar = messageId => ({
  type: SHOW_SNACKBAR,
  payload: {
    messageId,
  },
});

export interface HideSnackbarAction extends Action<string> {}

type HideSnackbar = () => HideSnackbarAction;

export const hideSnackbar: HideSnackbar = () => ({
  type: HIDE_SNACKBAR,
});
