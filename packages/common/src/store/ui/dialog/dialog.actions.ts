import { ActionWithPayload } from '@zaaksysteem/common/src/types/ActionWithPayload';
import { SHOW_DIALOG, HIDE_DIALOG } from './dialog.constants';

export interface DialogActionPayload {
  dialogType: string;
  [key: string]: any;
}

export interface DialogAction extends ActionWithPayload<DialogActionPayload> {}

type ShowDialogOptions = {
  dialogType: string;
  [key: string]: any;
};

type ShowDialog = (data: ShowDialogOptions) => DialogAction;

export const showDialog: ShowDialog = ({ dialogType, type, ...rest }) => ({
  type: SHOW_DIALOG,
  payload: {
    dialogType: dialogType || type,
    ...rest,
  },
});

type HideDialog = (dialogType: string) => DialogAction;

export const hideDialog: HideDialog = dialogType => ({
  type: HIDE_DIALOG,
  payload: {
    dialogType,
  },
});
