import errorMiddleware, {
  ErrorMiddlewareOptionsType,
} from './error.middleware';

export const getErrorModule = (options: ErrorMiddlewareOptionsType) => ({
  id: 'error',
  middlewares: [errorMiddleware(options)],
});

export default getErrorModule;
