import { ThunkAction } from 'redux-thunk';
import { ActionWithPayload } from './ActionWithPayload';

export type ThunkActionCreator<State, Payload, ExtraArgument = {}> = (
  payload: Payload
) => ThunkAction<void, State, ExtraArgument, ActionWithPayload<Payload>>;
