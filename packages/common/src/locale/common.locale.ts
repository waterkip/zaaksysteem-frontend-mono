export default {
  nl: {
    dates: {
      dayNamesShort: ['Zo', 'Ma', 'Di', 'Wo', 'Do', 'Vr', 'Za'],
      dayNames: [
        'Zondag',
        'Maandag',
        'Dinsdag',
        'Woensdag',
        'Donderdag',
        'Vrijdag',
        'Zaterdag',
      ],
      monthNamesShort: [
        'jan',
        'feb',
        'mrt',
        'apr',
        'mei',
        'jun',
        'jul',
        'aug',
        'sep',
        'okt',
        'nov',
        'dec',
      ],
      monthNames: [
        'januari',
        'februari',
        'maart',
        'april',
        'mei',
        'juni',
        'juli',
        'augustus',
        'september',
        'oktober',
        'november',
        'december',
      ],
      DoFn: (month: string): string => month,
      dateFormatText: 'DD MMMM YYYY',
      dateFormatTextShort: 'D MMM YYYY',
      dateFormat: 'DD-MM-YYYY',
      timeFormat: 'HH:mm',
      timeFormatFull: 'HH:mm:ss',
    },
    case: {
      status: {
        new: 'Nieuw',
        open: 'In behandeling',
        stalled: 'Opgeschort',
        resolved: 'Afgehandeld',
      },
    },
    filePreview: {
      previousPage: 'Vorige pagina',
      nextPage: 'Volgende pagina',
      zoomIn: 'Inzoomen',
      zoomOut: 'Uitzoomen',
      rotateRight: 'Kloksgewijs 90 graden draaien',
      iframeDescription: 'Frame voor PDF weergave',
      downloadDocument: 'Document downloaden',
    },
    forms: {
      add: 'Toevoegen',
      ok: 'OK',
      cancel: 'Annuleren',
      delete: 'Verwijderen',
    },
    snack: {
      close: 'Sluiten',
    },
    dialog: {
      error: {
        title: 'Fout',
        details: 'Details',
        body:
          'Er is een fout op de server opgetreden. Probeer het later opnieuw.',
      },
      ok: 'OK',
      cancel: 'Annuleren',
      save: 'Opslaan',
    },
    serverErrors: {
      status: {
        '401': 'U bent niet geauthoriseerd om deze actie uit te voeren.',
        '403': 'U heeft niet genoeg rechten om deze actie uit te voeren.',
        '404':
          'De opgevraagde pagina of gegevensbron kon niet worden gevonden. Probeer het later opnieuw.',
      },
      'invalid/syntax':
        'Uit het antwoord van de server kon geen geldige JSON worden opgebouwd.',
    },
    clientErrors: {
      render:
        'Er is helaas iets fout gegaan bij het weergeven van dit onderdeel.',
    },
    validations: {
      yup: {
        mixed: {
          required: 'Vul een geldige waarde in.',
        },
        string: {
          email:
            "'${value}' is geen geldig e-mailadres. Suggestie: naam@example.com",
        },
        number: {
          lessThan: 'Waarde moet kleiner zijn dan ${less}',
          moreThan: 'Waarde moet groter zijn dan ${more}',
        },
        array: {
          min: 'Minimaal ${min} items',
          max: 'Maximaal ${max} items',
        },
      },
      custom: {
        array: {
          email: {
            min: 'Voer minimaal ${min} e-mailadres(sen) in.',
            max: 'Voer maximaal ${max} e-mailadres(sen) in.',
          },
        },
        string: {
          emailOrMagicString:
            "'${value}' is geen geldig e-mailadres of magic string. Suggestie: naam@example.com, [[ magic_string ]]",
        },
        file: {
          noFile: 'Geen bestand(en) geüpload. Voeg een bestand toe.',
          invalidFile: 'Ongeldig(e) bestand(en) geüpload. Probeer het opnieuw.',
        },
      },
    },
    fileSelect: {
      addFile: 'Bestanden toevoegen',
      selectInstructions: 'Selecteer een bestand',
      dragInstructions: 'Sleep uw bestand(en) hierheen',
      dropInstructions: 'Sleep uw bestand(en) hierheen',
      orLabel: 'of',
    },
    caseRequestor: {
      error: 'De aanvrager van de zaak is niet beschikbaar.',
      loading: 'Bezig met laden…',
    },
  },
};
