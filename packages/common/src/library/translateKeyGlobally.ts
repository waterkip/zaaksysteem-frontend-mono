import i18next from 'i18next';

export const translateKeyGlobally = (
  t: i18next.TFunction,
  code: string
): string | null => {
  const language = i18next.language;
  //@ts-ignore
  const namespaces = Object.keys(i18next.store.data[language]);
  return namespaces.reduce((acc, ns) => {
    const key = `${ns}:${code}`;
    if (i18next.exists(key)) {
      acc = t(key);
    }
    return acc;
  }, null);
};
