import { APIDocument } from '@zaaksysteem/generated';
import { request } from '../fetch';

export type ResponseBodyType = APIDocument.CreateFileResponseBody;

export const sendFile = (file: File) => {
  let formData = new FormData();

  formData.append('file', file);

  return request('POST', `/api/v2/file/create_file`, formData, 'formdata')
    .then(requestPromise => requestPromise.json())
    .then((response: ResponseBodyType) => response)
    .catch((response: ResponseBodyType) => Promise.reject(response));
};
