import { buildUrl } from '@mintlab/kitchen-sink/source';
import { APICommunication } from '@zaaksysteem/generated';
import { request } from './../fetch';

export type CaseStatusType = 'new' | 'open' | 'stalled' | 'resolved';

export type SearchCaseType = (
  input: string,
  filter?: {
    status?: CaseStatusType[];
  }
) => Promise<APICommunication.SearchCaseResponseBody>;

export const searchCase: SearchCaseType = (input, filter) =>
  request(
    'GET',
    buildUrl<APICommunication.SearchCaseRequestParams>(
      `/api/v2/communication/search_case`,
      {
        ...(filter && { filter }),
        search_term: input,
        minimum_permission: 'read',
        limit: 25,
      }
    )
  )
    .then(requestPromise => requestPromise.json())
    .catch(response => Promise.reject(response));
