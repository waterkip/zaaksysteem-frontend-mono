import i18next from 'i18next';

type TraverseType = string | any[] | null | { [key: string]: any };

export function applyTranslations<T = any>(
  inputObj: any,
  t: i18next.TFunction,
  blacklist: string[] = []
): T {
  function traverse(current: TraverseType): TraverseType {
    if (!current) {
      return current;
    }

    if (typeof current === 'string') {
      return t(current) || current;
    }

    if (Array.isArray(current)) {
      return current.map(traverse);
    }

    if (typeof current === 'object') {
      return Object.entries(current).reduce(
        (acc, [key, value]) => ({
          ...acc,
          [key]: blacklist.includes(key) ? value : traverse(value),
        }),
        {}
      );
    }

    return current;
  }
  return traverse(inputObj) as T;
}
