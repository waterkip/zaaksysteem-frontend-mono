import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(({ mintlab: { greyscale } }: any) => ({
  dialogRoot: {
    width: 500,
  },
  wrapper: {
    '&:not(:last-child)': {
      marginBottom: '20px',
    },
  },
  dialogTitle: {
    '&>*:nth-child(1)': {
      color: greyscale.darkest,
    },
  },
}));
