export const isPdfSupported = () => {
  function hasAcrobatInstalled() {
    function getActiveXObject(name) {
      try {
        return new window.ActiveXObject(name);
      } catch (error) {
        // ignore errors
      }
    }

    return getActiveXObject('AcroPDF.PDF') || getActiveXObject('PDF.PdfCtrl');
  }

  return navigator.mimeTypes['application/pdf'] || hasAcrobatInstalled();
};
