import React, { Suspense } from 'react';
// @ts-ignore
import Loader from '@mintlab/ui/App/Zaaksysteem/Loader';
import {
  DocumentPreviewPropsType,
  DocumentPreviewViewerType,
} from './DocumentPreview.types';
import { DocumentPreviewHeader } from './DocumentPreviewHeader/DocumentPreviewHeader';
import { Native } from './Native/Native';
import { isPdfSupported } from './library/featureDetection';
import { useDocumentPreviewStyles } from './DocumentPreview.style';

const Viewer = React.lazy(() =>
  import(/* webpackChunkName: "pdf-viewer" */ './Viewer/Viewer')
);

const getViewerType = (): DocumentPreviewViewerType =>
  isPdfSupported() ? 'native' : 'viewer';

export const DocumentPreview = React.forwardRef<
  HTMLDivElement,
  DocumentPreviewPropsType
>((props, ref) => {
  const classes = useDocumentPreviewStyles();
  const { forceView } = props;

  const view = forceView ? forceView : getViewerType();
  const displayHeader = [props.title, props.onClose, props.downloadUrl].some(
    item => typeof item !== 'undefined'
  );
  const viewerProps = {
    ...props,
    className: classes.viewer,
  };

  return (
    <div ref={ref} tabIndex={-1} className={classes.wrapper}>
      {displayHeader && (
        <DocumentPreviewHeader
          {...props}
          downloadUrl={props.downloadUrl || props.url}
        />
      )}
      <Suspense fallback={<Loader delay={200} />}>
        {view === 'viewer' && <Viewer {...viewerProps} />}
        {view === 'native' && <Native {...viewerProps} />}
      </Suspense>
    </div>
  );
});

DocumentPreview.displayName = 'DocumentPreview';

export default DocumentPreview;
