import React from 'react';
import { useState } from '@storybook/addons';
//@ts-ignore
import { stories, text, boolean, select } from '@mintlab/ui/App/story';
import { DocumentPreview } from './DocumentPreview';
import { DocumentPreviewModal } from './DocumentPreviewModal';
import { DocumentPreviewPropsType } from './DocumentPreview.types';

const getProps = (): DocumentPreviewPropsType => ({
  title: text('Title', 'Lorum-ipsum.pdf'),
  url: text(
    'PDF url',
    'https://s1.q4cdn.com/806093406/files/doc_downloads/test.pdf'
  ),
  onClose: boolean('Closeable', true) ? () => {} : undefined,
});

const views = ['viewer', 'native'];

stories(module, __dirname, {
  Default() {
    return (
      <div style={{ height: 1000 }}>
        <DocumentPreview
          {...getProps()}
          forceView={select('Viewer', views, 'viewer')}
        />
      </div>
    );
  },

  Native() {
    const props: DocumentPreviewPropsType = {
      ...getProps(),
      forceView: 'native',
    };

    return (
      <div style={{ height: 1000 }}>
        <DocumentPreview {...props} />
      </div>
    );
  },

  Viewer() {
    const props: DocumentPreviewPropsType = {
      ...getProps(),
      forceView: 'viewer',
      pagination: boolean('Pagination', true),
    };

    return (
      <div style={{ height: 1000 }}>
        <DocumentPreview {...props} />
      </div>
    );
  },

  WithoutHeader() {
    return (
      <div style={{ height: 1000 }}>
        <DocumentPreview
          url={text(
            'PDF url',
            'https://s1.q4cdn.com/806093406/files/doc_downloads/test.pdf'
          )}
        />
      </div>
    );
  },

  Modal() {
    const [isOpen, setOpen] = useState(false);
    return (
      <React.Fragment>
        <button onClick={() => setOpen(true)} type="button">
          Open modal
        </button>
        <DocumentPreviewModal
          open={isOpen}
          {...getProps()}
          onClose={() => setOpen(false)}
          forceView={select('Viewer', views, 'viewer')}
        />
      </React.Fragment>
    );
  },
});
