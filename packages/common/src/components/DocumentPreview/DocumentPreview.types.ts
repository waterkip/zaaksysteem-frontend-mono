export type DocumentPreviewViewerType = 'viewer' | 'native';

export interface DocumentPreviewPropsType {
  /**
   * Document preview title
   */
  title?: string;

  /**
   * If provided renders a close cross in the header
   */
  onClose?: () => void;

  /**
   * URL to PDF file
   */
  url: string;

  /**
   * Defaults to false, only relevant for Viewer
   */
  pagination?: boolean;

  /**
   * Optional file download url. If not provided, url will be used
   */
  downloadUrl?: string;

  forceView?: DocumentPreviewViewerType;
}
