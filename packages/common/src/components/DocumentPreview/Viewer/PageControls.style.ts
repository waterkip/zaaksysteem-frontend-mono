import { makeStyles } from '@material-ui/core/styles';

export const usePageControlStyles = makeStyles(() => ({
  wrapper: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    color: 'white',
    backgroundColor: 'rgba(0,0,0,0.6)',
  },
  buttonWrapper: {
    display: 'flex',
    flexDirection: 'row',
  },
}));
