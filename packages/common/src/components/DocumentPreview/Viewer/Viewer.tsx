import React, { useState } from 'react';
import { Document, Page, pdfjs } from 'react-pdf';
import classnames from 'classnames';
//@ts-ignore
import Loader from '@mintlab/ui/App/Zaaksysteem/Loader';
import { DocumentPreviewPropsType } from '../DocumentPreview.types';
import { ViewerControls } from './ViewerControls';
import { PageControls } from './PageControls';
import { useViewerStyles } from './Viewer.style';

pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;

export interface ViewerPropsType extends DocumentPreviewPropsType {
  className?: string;
}

export const Viewer: React.ComponentType<ViewerPropsType> = ({
  url,
  className,
  pagination = false,
}) => {
  const classes = useViewerStyles();
  const [scale, setScale] = useState(1);
  const [page, setPage] = useState(1);
  const [rotation, setRotation] = useState(0);
  const [numPages, setNumPages] = useState(0);

  const loader = <Loader delay={200} />;

  const getSinglePage = (pageNumber: number) => (
    <Page
      scale={scale}
      rotate={rotation}
      className={classes.page}
      pageNumber={pageNumber}
      key={pageNumber}
      loading={loader}
    />
  );

  const getAllPages = () => {
    return Array(numPages)
      .fill(false)
      .map((val, index) => getSinglePage(index + 1));
  };

  const getPages = () => (pagination ? getSinglePage(page) : getAllPages());

  return (
    <div className={classnames(classes.wrapper, className && className)}>
      <ViewerControls
        className={classes.controls}
        scale={scale}
        rotation={rotation}
        onScaleChange={setScale}
        onRotationChange={setRotation}
      />
      {pagination && (
        <PageControls page={page} numPages={numPages} onPageChange={setPage} />
      )}
      <div className={classes.pageWrapper}>
        <Document
          file={url}
          onLoadSuccess={({ numPages }) => setNumPages(numPages)}
          loading={loader}
        >
          {numPages > 0 ? getPages() : null}
        </Document>
      </div>
    </div>
  );
};

export default Viewer;
