import { makeStyles } from '@material-ui/core';

export const usePlusButtonSpaceWrapperStyle = makeStyles({
  root: {
    width: 'inherit',
    '&:after': {
      content: '""',
      height: 80,
      display: 'block',
    },
  },
});
