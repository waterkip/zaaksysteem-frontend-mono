import { FormDefinition } from '../types/formDefinition.types';
import { createValidation, ValidationMap } from './createValidation';

describe('createValidation', () => {
  describe('Custom rules', () => {
    const validationMap: ValidationMap = {
      custom: jest.fn(),
    };
    const formDefinition: FormDefinition<{ custom_field: string }> = [
      {
        name: 'custom_field',
        format: 'text',
        required: true,
        value: '',
        type: 'custom',
      },
    ];
    const values = {
      custom_field: '',
    };

    test('Should be allowed', async () => {
      const schema = createValidation({
        formDefinition,
        validationMap,
        t: jest.fn().mockReturnValue({}),
      });
      await schema.validate(values);

      expect(validationMap.custom).toBeCalled();
    });
  });
});
