import * as yup from 'yup';
import i18next from 'i18next';
import { AnyFormDefinitionField } from '../types/formDefinition.types';
import {
  createNumberSchema,
  createEmailSchema,
  createTextSchema,
  createArraySchema,
  createMixedSchema,
  createFileSchema,
} from './createSchema';

/**
 * Validation rules for basic component types.
 */

type ValidationRuleOptions = {
  t: i18next.TFunction;
  field: AnyFormDefinitionField;
};

type AnySchema =
  | yup.StringSchema
  | yup.NumberSchema
  | yup.MixedSchema
  | yup.ArraySchema<string | number>;

export type ValidationRule<S = yup.Schema<any>> = (
  options: ValidationRuleOptions
) => S;

export function getSchemaFromFormat(
  field: AnyFormDefinitionField,
  t: i18next.TFunction
): AnySchema {
  switch (field.format) {
    case 'number':
      return createNumberSchema(field);

    case 'email':
      return createEmailSchema(field, t);

    case 'mixed':
      return createMixedSchema();

    case 'file':
      return createFileSchema(t);

    default:
      return createTextSchema(field);
  }
}

export function setRequired(
  field: AnyFormDefinitionField,
  schema: AnySchema
): AnySchema {
  return field.required ? schema.required() : schema;
}

export function handleMultiValue(
  field: AnyFormDefinitionField,
  schema: AnySchema,
  t: i18next.TFunction
): yup.ArraySchema<string | number> | AnySchema {
  return field.multiValue
    ? createArraySchema<string | number>(field, t)
        .ensure()
        .of(schema)
    : schema;
}

export const createValidationRule = (
  field: AnyFormDefinitionField,
  t: i18next.TFunction
): AnySchema => {
  const [rule] = [getSchemaFromFormat(field, t)]
    .map(schema => handleMultiValue(field, schema, t))
    .map(schema => setRequired(field, schema));

  return rule;
};
