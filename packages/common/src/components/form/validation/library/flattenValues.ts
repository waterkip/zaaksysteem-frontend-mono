import { asArray } from '@mintlab/kitchen-sink/source';
import {
  FormValue,
  FlatFormValue,
  FormDefinition,
  FormValuesType,
} from '../../types/formDefinition.types';
import { ValidationMap } from '../createValidation';
import getFieldByName from '../../library/getFieldByName';

/**
 * Flattens a object of values, so it can be handled
 * by the default schemas.
 */

export function flattenValues<Values = any>(
  values: FormValuesType<Values>,
  formDefinition: FormDefinition,
  validationMap?: ValidationMap
): { [key: string]: FlatFormValue | FlatFormValue[] } {
  return Object.entries(values).reduce((acc, [key, value]) => {
    const field = getFieldByName<Values>(formDefinition, key as keyof Values);
    const hasCustomValidation = validationMap
      ? Boolean(validationMap[field.name])
      : false;

    const { format, multiValue } = field;

    const transformed = asArray(value).map(thisValue =>
      transformFormValueToFlatValue(thisValue, format, hasCustomValidation)
    );

    return {
      ...acc,
      [key]: multiValue ? transformed : transformed[0],
    };
  }, {});
}

/* eslint complexity: [2, 7] */
function transformFormValueToFlatValue(
  value: FormValue | null,
  format: string | undefined,
  hasCustomValidation: boolean
): FlatFormValue | any {
  if (hasCustomValidation || format === 'mixed' || format === 'file')
    return value;

  if (
    value &&
    typeof value === 'object' &&
    Object.prototype.hasOwnProperty.call(value, 'value')
  )
    return value.value;

  return value;
}
