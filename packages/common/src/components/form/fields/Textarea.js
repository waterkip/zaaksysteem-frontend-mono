import React from 'react';
import { withStyles } from '@material-ui/styles';
import { TextField } from '@mintlab/ui/App/Material/TextField';
import { textareaStyleSheet } from './Textarea.style';

export const Textarea = ({ classes, ...restProps }) => (
  <TextField
    {...restProps}
    classes={{ inputRoot: classes.inputRoot, input: classes.textarea }}
  />
);

export default withStyles(textareaStyleSheet)(Textarea);
