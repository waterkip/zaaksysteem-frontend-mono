import React from 'react';
//@ts-ignore
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { FormFieldPropsType } from '../types/formDefinition.types';

export interface MultiValueTextPropsType extends FormFieldPropsType {
  createOnBlur?: boolean;
}

/*
 * Wraps Select and configures it as multi value textfield
 */
const MultiValueText: React.ComponentType<MultiValueTextPropsType> = props => {
  const { multiValue, createOnBlur = true, ...rest } = props;

  return (
    <Select
      {...rest}
      isMulti={multiValue}
      isClearable={true}
      createOnBlur={createOnBlur}
      creatable={true}
      formatCreateLabel={(input: string) => input}
    />
  );
};

export default MultiValueText;
