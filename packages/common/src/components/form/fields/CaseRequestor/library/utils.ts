import {
  fetchCaseRoles,
  createDisplayName,
} from '../../../../../library/requests/fetchCaseRoles';
import {
  CaseRequestorConfigType,
  CaseRequestorType,
} from '../CaseRequestor.types';

export const defaultItemFilter = () => true;

export const fetchRequestor = (
  config?: CaseRequestorConfigType
): Promise<CaseRequestorType | void> =>
  config
    ? fetchCaseRoles(config.caseUuid)
        .then(response => {
          const [requestor] = response
            .filter(config.itemFilter || defaultItemFilter)
            .filter(role => role.role.attributes.role === 'Aanvrager')
            .map<CaseRequestorType>(role => {
              return config.valueResolver
                ? config.valueResolver(role)
                : {
                    value: role.role.id,
                    label: createDisplayName(role),
                  };
            });

          return requestor;
        })
        .catch(() => {})
    : Promise.resolve();
