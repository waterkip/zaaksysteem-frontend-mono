import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(({ mintlab: { greyscale } }: any) => ({
  wrapper: {
    padding: 14,
  },
  error: {
    color: 'red',
  },
}));
