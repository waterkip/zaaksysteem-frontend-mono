import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import i18next from 'i18next';
//@ts-ignore
import { DataProvider } from '@mintlab/ui/App/Abstract/DataProvider';
//@ts-ignore
import MultilineOption from '@mintlab/ui/App/Zaaksysteem/Select/Option/MultilineOption';
import FlatValueSelect from '@zaaksysteem/common/src/components/form/fields/FlatValueSelect';
import {
  searchCase,
  CaseStatusType,
} from '../../../library/requests/searchCase';

type CaseChoiceType = {
  value: string;
  label: string;
  subLabel: string | null;
};

const fetchCase = (
  input: string,
  t: i18next.TFunction,
  filter?: {
    status?: CaseStatusType[];
  }
): Promise<CaseChoiceType[] | void> =>
  searchCase(input, filter)
    .then(response =>
      response.data.map<CaseChoiceType>(
        ({
          id,
          attributes: { display_id, case_type_name, status, description },
        }) => {
          const label = `${display_id}: ${case_type_name}`;
          const subLabel = [t(`case.status.${status}`), description]
            .filter(item => item)
            .join(' - ');

          return {
            value: id,
            label,
            subLabel,
          };
        }
      )
    )
    .catch(() => {});

type ChoiceType = {
  value: string;
  label: string;
};

export type CaseFinderPropsType = {
  choices: ChoiceType[];
  filter?: {
    status?: CaseStatusType[];
  };
};

const CaseFinder: React.FunctionComponent<CaseFinderPropsType> = ({
  ...restProps
}) => {
  const [input, setInput] = useState('');
  const { choices, filter } = restProps;
  const [t] = useTranslation('common');

  return (
    <DataProvider
      providerArguments={[input]}
      autoProvide={input !== ''}
      provider={(searchTerm: string) => fetchCase(searchTerm, t, filter)}
    >
      {({ data, busy }: { data: CaseChoiceType[] | null; busy: boolean }) => {
        const normalizedChoices = data || choices;

        return (
          <FlatValueSelect
            {...restProps}
            choices={normalizedChoices}
            isClearable={true}
            loading={busy}
            getChoices={setInput}
            components={{
              Option: MultilineOption,
            }}
            filterOption={() => true}
          />
        );
      }}
    </DataProvider>
  );
};

export default CaseFinder;
