export interface FileObject {
  value: string | null;
  status: 'init' | 'pending' | 'failed';
  key: string;
  name: any;
}
