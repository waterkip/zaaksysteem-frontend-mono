import React from 'react';
import { useTranslation } from 'react-i18next';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  DialogDivider,
  //@ts-ignore
} from '@mintlab/ui/App/Material/Dialog';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { FileObject } from '../types/FileSelect.types';
import createDialogActions from '../../../../dialogs/library/createDialogActions';
import {
  FormDefinition,
  FormRendererRenderProps,
} from '../../../types/formDefinition.types';
import FormRenderer from '../../../FormRenderer';

export interface FileSelectDialogProps {
  open: boolean;
  scope?: string;
  onClose: Function;
  onConfirm: (files: FileObject[]) => void;
  classes: any;
  accept?: string[];
  multiValue: boolean;
  [key: string]: any;
}

const getDialogActions = createDialogActions({
  primaryPresets: ['primary', 'text'],
  secondaryPresets: ['default', 'text'],
});

export const FileSelectDialog: React.ComponentType<FileSelectDialogProps> = ({
  open = false,
  scope,
  onClose,
  onConfirm,
  classes,
  accept,
  multiValue,
  ...rest
}) => {
  const [t] = useTranslation();
  const title = t('formRenderer:fileSelect.addFile');
  const formDefinition: FormDefinition = [
    {
      name: 'selectDialogAttachments',
      type: fieldTypes.FILE_SELECT,
      value: null,
      format: 'file',
      required: true,
      uploadDialog: false,
      multiValue,
      accept,
    },
  ];

  return (
    <Dialog aria-label={title} open={open} {...rest} classes={classes}>
      <DialogTitle title={title} scope={scope} />
      <FormRenderer formDefinition={formDefinition} isInitialValid={false}>
        {({ fields, values, isValid }: FormRendererRenderProps) => {
          const dialogActions = getDialogActions(
            {
              disabled: !isValid,
              text: t('common:forms.add'),
              action() {
                onConfirm(values.selectDialogAttachments as FileObject[]);
                onClose();
              },
            },
            {
              text: t('common:forms.cancel'),
              action: onClose,
            },
            'file-select-dialog'
          );
          return (
            <React.Fragment>
              <DialogContent padded={true}>
                {fields.map(({ FieldComponent, ...restProps }) => {
                  return (
                    <div key={restProps.key}>
                      <FieldComponent {...restProps} />
                    </div>
                  );
                })}
              </DialogContent>
              <React.Fragment>
                <DialogDivider />
                <DialogActions>{dialogActions}</DialogActions>
              </React.Fragment>
            </React.Fragment>
          );
        }}
      </FormRenderer>
    </Dialog>
  );
};

export default FileSelectDialog;
