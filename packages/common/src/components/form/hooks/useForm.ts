import { useRef, useEffect } from 'react';
import { useFormik, FormikProps } from 'formik';
import { FormValuesType, UseFormType } from '../types/formDefinition.types';
import useValidation from '../validation/hooks/useValidation';
import getValuesFromDefinition from '../library/getValuesFromFormDefinition';
import useRuleEngine from '../rules/hooks/useRuleEngine';
import { useFormFields, UseFormFieldsReturnType } from './useFormFields';
import { useFormSteps, UseFormStepsReturnType } from './useFormSteps';

export type UseFormReturnType<Values> = {
  fields: UseFormFieldsReturnType<Values>;
  formik: FormikProps<FormValuesType<Values>>;
} & Omit<UseFormStepsReturnType<Values>, 'activeFields'>;

export function useForm<Values>({
  formDefinition,
  onSubmit,
  onChange,
  enableReinitialize = false,
  isInitialValid = false,
  validationMap = {},
  fieldComponents = {},
  rules = [],
}: UseFormType<Values>): UseFormReturnType<Values> {
  const validate = useValidation(formDefinition, validationMap || {});

  const formik = useFormik<FormValuesType<Values>>({
    validate,
    onSubmit,
    enableReinitialize,
    initialValues: getValuesFromDefinition<Values>(formDefinition),
    validateOnMount: !isInitialValid,
  });

  const [updatedFormDefinition, updatedValues] = useRuleEngine<Values>(
    formDefinition,
    formik.values,
    rules
  );

  const { activeFields, ...restFormSteps } = useFormSteps<Values>({
    formDefinition: updatedFormDefinition,
    errors: formik.errors,
    touched: formik.touched,
  });

  const fields = useFormFields<Values>(
    activeFields,
    updatedValues,
    formik,
    fieldComponents
  );

  const valueRef = useRef(updatedValues);

  useEffect(() => {
    if (valueRef.current !== updatedValues) {
      formik.setValues(updatedValues);

      if (onChange) {
        onChange(updatedValues);
      }
    }

    valueRef.current = updatedValues;
  });

  return { fields, formik, ...restFormSteps };
}
