export type FlatFormValue = string | number | boolean;
export type NestedFormValue = {
  value: FlatFormValue;
  label?: FormValue;
  [key: string]: any;
};
export type FormValue = FlatFormValue | NestedFormValue;

export type AnyFormValueType = FormValue | FormValue[] | null;

export type FormDefinitionRefValueResolved =
  | FormValue
  | FormValue[]
  | null
  | { [key: string]: FormValue | FormValue[] | null };

export interface GenericFormDefinitionFieldAttributes<
  Values,
  Config,
  ValueType
> {
  type: string;
  name: keyof Values & string;
  hidden?: boolean;
  required?: boolean;
  disabled?: boolean;
  value: ValueType;
  placeholder?: string;
  label?: string;
  config?: Config;
  multiValue?: boolean;
  choices?: any[];
  applyBackgroundColor?: boolean;
  [key: string]: any;
}

export type FormDefinitionRefValue<Values = any> =
  | keyof Values
  | { [key: string]: keyof Values };

export type PartialFormValuesType<Values> = Partial<FormValuesType<Values>> & {
  [key: string]: any;
};

export type FormValuesType<Values = any> = {
  [key in keyof Values]: AnyFormValueType;
};
