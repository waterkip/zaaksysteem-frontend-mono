import { AnyFormDefinitionField } from './fieldDefinition.types';

// These exports are placed here for backwards compatibility purposes
export * from './generic.types';
export * from './fieldComponent.types';
export * from './fieldDefinition.types';
export * from './formRenderer.types';

export type FormDefinitionStep<Values> = {
  title: string;
  description?: string;
  fields: AnyFormDefinitionField<Values>[];
};

export type SingleStepFormDefinition<Values> = AnyFormDefinitionField<Values>[];
export type MultiStepFormDefinition<Values> = FormDefinitionStep<Values>[];

export type FormDefinition<Values = any> =
  | SingleStepFormDefinition<Values>
  | MultiStepFormDefinition<Values>;
