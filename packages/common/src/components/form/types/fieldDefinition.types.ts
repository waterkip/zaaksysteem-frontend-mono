import {
  AnyFormValueType,
  GenericFormDefinitionFieldAttributes,
  FormDefinitionRefValue,
} from './generic.types';

export interface FormDefinitionField<
  Values = any,
  Config = any,
  ValueType = AnyFormValueType
> extends GenericFormDefinitionFieldAttributes<Values, Config, ValueType> {
  minItems?: number;
  maxItems?: number;
  refValue?: FormDefinitionRefValue<Values>;
}

export interface FormDefinitionNumberField<Values = any>
  extends FormDefinitionField<Values> {
  format: 'number';
  value: AnyFormValueType;
  min?: number;
  max?: number;
  lessThan?: number;
  moreThan?: number;
}

export interface FormDefinitionTextField<Values = any>
  extends FormDefinitionField<Values> {
  format: 'text';
  value: AnyFormValueType;
  min?: number;
  max?: number;
}

export interface FormDefinitionMixedField<Values = any>
  extends FormDefinitionField<Values> {
  format: 'mixed';
}

export interface FormDefinitionEmailField<Values = any>
  extends FormDefinitionField<Values> {
  format: 'email';
  value: AnyFormValueType;
  allowMagicString?: boolean;
}

export interface FormDefinitionStringField<Values = any>
  extends FormDefinitionField<Values> {
  format?: '';
}

export interface FormDefinitionFileField<Values = any>
  extends FormDefinitionField<Values> {
  format: 'file';
  accept?: string[];
}

export type AnyFormDefinitionField<Values = any> =
  | FormDefinitionStringField<Values>
  | FormDefinitionNumberField<Values>
  | FormDefinitionTextField<Values>
  | FormDefinitionEmailField<Values>
  | FormDefinitionMixedField<Values>
  | FormDefinitionFileField<Values>;
