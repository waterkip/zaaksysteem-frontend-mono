import i18next from 'i18next';
import { FormDefinition } from '../types/formDefinition.types';
import { applyTranslations } from '../../../library/applyTranslations';

export default function translateFormDefinition<Values = any>(
  formDefinition: FormDefinition<Values>,
  t: i18next.TFunction
): FormDefinition<Values> {
  return applyTranslations<FormDefinition<Values>>(formDefinition, t, [
    'value',
    'name',
    'type',
  ]);
}
