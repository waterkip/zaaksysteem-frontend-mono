import {
  FormDefinition,
  FormValuesType,
  AnyFormDefinitionField,
} from '../types/formDefinition.types';
import { isSteppedForm } from './formHelpers';

function getValuesFromFields<Values>(fields: AnyFormDefinitionField<Values>[]) {
  return fields.reduce<FormValuesType<Values>>(
    (values, field) => ({
      ...values,
      [field.name]: field.value,
    }),
    {} as FormValuesType<Values>
  );
}

export default function getValuesFromDefinition<Values = any>(
  formDefinition: FormDefinition<Values>
): FormValuesType<Values> {
  return isSteppedForm(formDefinition)
    ? formDefinition.reduce(
        (acc, step) => ({
          ...acc,
          ...getValuesFromFields(step.fields),
        }),
        {} as FormValuesType<Values>
      )
    : getValuesFromFields(formDefinition);
}
