import { PropsWithChildren, ReactElement } from 'react';
import {
  FormRendererRenderProps,
  UseFormType,
} from './types/formDefinition.types';
import { useForm } from './hooks/useForm';

export type FormRendererProps<Values = any> = Omit<
  UseFormType<Values>,
  'onSubmit'
> &
  Partial<Pick<UseFormType<Values>, 'onSubmit'>> & {
    children: (computedProps: FormRendererRenderProps) => any;
  };

function FormRenderer<Values = any>({
  children,
  onSubmit = () => {},
  ...restProps
}: PropsWithChildren<FormRendererProps<Values>>): ReactElement {
  const { formik, ...rest } = useForm<Values>({ onSubmit, ...restProps });

  return children({
    ...formik,
    ...rest,
  });
}

export default FormRenderer;
