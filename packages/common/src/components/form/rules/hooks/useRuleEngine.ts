import { useMemo } from 'react';
import {
  FormDefinition,
  FormValuesType,
  PartialFormValuesType,
} from '../../types/formDefinition.types';
import Rule from '../library/Rule';
import getValuesFromDefinition from '../../library/getValuesFromFormDefinition';
import {
  getFormDefinitionAfterRules,
  getValuesDiff,
  isFormDefinitionUpdated,
} from './helpers';

export function useRuleEngine<Values = any>(
  formDefinition: FormDefinition<Values>,
  values: FormValuesType<Values>,
  rules: Rule[] = []
): [FormDefinition<Values>, any] {
  const updatedFormDefinition = useMemo(
    () => getFormDefinitionAfterRules(rules, formDefinition, values),
    [values]
  );

  const updatedValues = useMemo(
    () => getValuesFromDefinition<Values>(updatedFormDefinition),
    [updatedFormDefinition]
  );

  const valueDiff = getValuesDiff<Values>(
    values as PartialFormValuesType<Values>,
    updatedValues as PartialFormValuesType<Values>
  );

  const hasUpdatedValues = Boolean(Object.keys(valueDiff).length);

  const hasUpdatedFormDefinition = isFormDefinitionUpdated<Values>(
    formDefinition,
    updatedFormDefinition,
    values
  );

  return [
    hasUpdatedFormDefinition ? updatedFormDefinition : formDefinition,
    hasUpdatedValues
      ? {
          ...values,
          ...valueDiff,
        }
      : values,
  ];
}

export default useRuleEngine;
