import {
  FormValue,
  AnyFormDefinitionField,
} from '../../types/formDefinition.types';

const updateFields = (fieldNames: string[], update: any) => (
  field: AnyFormDefinitionField
): AnyFormDefinitionField => ({
  ...field,
  ...(fieldNames.includes(field.name) ? update : {}),
});

export type RuleEngineActionExecution = (
  fields: AnyFormDefinitionField[]
) => AnyFormDefinitionField[];
export type RuleEngineMultiFieldAction = (
  fieldNames: string[],
  value?: any
) => RuleEngineActionExecution;
export type RuleEngineSingleFieldAction = (
  fieldName: string,
  value: { [key: string]: FormValue }
) => RuleEngineActionExecution;

export type RuleEngineAction =
  | RuleEngineSingleFieldAction
  | RuleEngineMultiFieldAction;

export const hideFields: RuleEngineMultiFieldAction = fieldNames => fields =>
  fields.map(updateFields(fieldNames, { hidden: true }));

export const showFields: RuleEngineMultiFieldAction = fieldNames => fields =>
  fields.map(updateFields(fieldNames, { hidden: false }));

export const setFieldValue: RuleEngineSingleFieldAction = (
  fieldName,
  value
) => fields => fields.map(updateFields([fieldName], { value }));

export const setRequired: RuleEngineMultiFieldAction = fieldNames => fields =>
  fields.map(updateFields(fieldNames, { required: true }));

export const setOptional: RuleEngineMultiFieldAction = fieldNames => fields =>
  fields.map(updateFields(fieldNames, { required: false }));

export const updateField: RuleEngineSingleFieldAction = (
  fieldName,
  update
) => fields => fields.map(updateFields([fieldName], update));
