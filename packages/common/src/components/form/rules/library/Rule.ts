import { FormValue, FormDefinition } from '../../types/formDefinition.types';
import getFieldByName from '../../library/getFieldByName';
import { RuleEngineCondition } from './conditions';
import { RuleEngineActionExecution } from './actions';
import triggerActions from './triggerActions';

export class Rule<Fields = any> {
  private ifFn: RuleEngineCondition | undefined;
  private thenFn: RuleEngineActionExecution[] = [];
  private elseFn: RuleEngineActionExecution[] = [];
  private activeAction: string | null = null;

  constructor(private fieldName: keyof Fields) {}

  when(fn: RuleEngineCondition) {
    this.ifFn = fn;
    return this;
  }

  then(fn: RuleEngineActionExecution) {
    this.activeAction = 'then';
    this.thenFn.push(fn);
    return this;
  }

  else(fn: RuleEngineActionExecution) {
    this.activeAction = 'else';
    this.elseFn.push(fn);
    return this;
  }

  and(fn: RuleEngineActionExecution) {
    switch (this.activeAction) {
      case 'then':
        return this.then(fn);

      case 'else':
        return this.else(fn);

      default:
        throw new Error(
          'Cannot call and(...) before calling then(...) or when(...)'
        );
    }
  }

  execute(formDefinition: FormDefinition<Fields>) {
    const field = getFieldByName<Fields>(formDefinition, this.fieldName);

    if (field) {
      if (this.ifFn && this.ifFn(field.value as FormValue)) {
        return triggerActions(this.thenFn, formDefinition);
      } else if (this.elseFn) {
        return triggerActions(this.elseFn, formDefinition);
      }
    }

    return formDefinition;
  }
}

export default Rule;
