import React, { useState } from 'react';
import { stories, boolean } from '@mintlab/ui/App/story';
import { NumericField } from './NumericField';

stories(module, __dirname, {
  NumericFieldMultiple() {
    const [value, setValue] = useState([123, 10000]);

    return (
      <NumericField
        multiValue={true}
        disabled={boolean('disabled', false)}
        value={value}
        onChange={event => setValue(event.target.value)}
      />
    );
  },

  NumericFieldSingle() {
    const [value, setValue] = useState(101);

    return (
      <NumericField
        multiValue={false}
        disabled={boolean('disabled', false)}
        value={value}
        onChange={event => setValue(event.target.value)}
      />
    );
  },
});
