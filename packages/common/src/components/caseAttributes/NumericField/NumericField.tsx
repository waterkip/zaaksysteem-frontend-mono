import React from 'react';
import { useTranslation } from 'react-i18next';
//@ts-ignore
import { TextField as MuiTextField } from '@mintlab/ui/App/Material/TextField';
//@ts-ignore
import Button from '@mintlab/ui/App/Material/Button';
import {
  CaseAttributeFieldChangeEventType,
  useMultiFieldHandlers,
} from '../../../library/caseAttributes';
import { FormFieldPropsType } from '../../form/types/formDefinition.types';
import { useNumericFieldStyles } from './NumericField.style';

const maxLength = Number.MAX_SAFE_INTEGER.toString().length - 1;

export const NumericField: React.ComponentType<FormFieldPropsType> = ({
  multiValue,
  disabled,
  value,
  name,
  placeholder,
  onChange,
}) => {
  const classes = useNumericFieldStyles();
  const [t] = useTranslation('common');

  const processValue = (inputValue: string | null) => {
    const parsed = inputValue && parseInt(inputValue, 10);
    return isNaN(parsed || NaN) ? null : parsed;
  };

  const displayValue = (inputValue: typeof value | null) => {
    return inputValue === null ? '' : inputValue;
  };

  const {
    isMultiple,
    updateField,
    addField,
    removeOrClearFirstField,
  } = useMultiFieldHandlers({
    multiValue,
    value,
    onChange,
    processValue,
    defaultValue: null,
  });

  return (
    <React.Fragment>
      {isMultiple(value) ? (
        value.map((field, index) => {
          return (
            <div className={classes.fieldContainer} key={index}>
              <MuiTextField
                value={displayValue(field)}
                disabled={disabled}
                name={name + index}
                inputProps={{ maxLength }}
                onChange={(event: CaseAttributeFieldChangeEventType) => {
                  updateField(event.target.value, index);
                }}
              />
              {!disabled && (
                <Button
                  action={() => removeOrClearFirstField(index)}
                  presets={['icon', 'small']}
                >
                  close
                </Button>
              )}
            </div>
          );
        })
      ) : (
        <MuiTextField
          value={value}
          placeholder={placeholder}
          name={name}
          disabled={disabled}
          inputProps={{ maxLength }}
          onChange={(event: CaseAttributeFieldChangeEventType) => {
            updateField(event.target.value);
          }}
        />
      )}
      {multiValue && !disabled && (
        <Button action={addField}>{t('addNewField')}</Button>
      )}
    </React.Fragment>
  );
};

export default NumericField;
