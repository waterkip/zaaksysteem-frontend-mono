import React from 'react';
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import { stories } from '@mintlab/ui/App/story';
import FormRenderer from '../form/FormRenderer';
import TextField from './TextField/TextField';

const AttributeFormFields = {
  attr_text: TextField,
};

const formDefinition = [
  {
    name: 'first_name',
    type: 'attr_text',
    value: '',
    required: true,
    placeholder: 'Enter your first name',
  },
];

const renderField = ({ FieldComponent, name, ...rest }, errors, touched) => {
  const props = {
    ...cloneWithout(rest, 'type', 'mode'),
    compact: true,
    name,
    scope: name,
  };

  return (
    <div key={name} style={{ padding: 5 }}>
      <FieldComponent {...props} />
      {errors[name] && touched[name] && <p>{errors[name]}</p>}
    </div>
  );
};

stories(module, __dirname, {
  Default() {
    return (
      <FormRenderer
        FieldComponents={AttributeFormFields}
        formDefinition={formDefinition}
        rules={[]}
      >
        {({ fields, errors, touched }) => {
          return fields.map(field => renderField(field, errors, touched));
        }}
      </FormRenderer>
    );
  },
});
