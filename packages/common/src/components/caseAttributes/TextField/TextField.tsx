import React from 'react';
import { useTranslation } from 'react-i18next';
//@ts-ignore
import { TextField as MuiTextField } from '@mintlab/ui/App/Material/TextField';
//@ts-ignore
import Button from '@mintlab/ui/App/Material/Button';
import {
  CaseAttributeFieldChangeEventType,
  useMultiFieldHandlers,
} from '../../../library/caseAttributes';
import { FormFieldPropsType } from '../../form/types/formDefinition.types';
import { useTextFieldStyles } from './TextField.style';

const TextField: React.ComponentType<FormFieldPropsType> = ({
  multiValue,
  disabled,
  value,
  name,
  placeholder,
  onChange,
}) => {
  const classes = useTextFieldStyles();
  const [t] = useTranslation('common');

  const {
    isMultiple,
    updateField,
    addField,
    removeOrClearFirstField,
  } = useMultiFieldHandlers({
    multiValue,
    value,
    onChange,
    defaultValue: '',
  });

  return (
    <React.Fragment>
      {isMultiple(value) ? (
        value.map((field, index) => {
          return (
            <div className={classes.fieldContainer} key={index}>
              <MuiTextField
                value={field}
                disabled={disabled}
                name={name + index}
                onChange={(event: CaseAttributeFieldChangeEventType) =>
                  updateField(event.target.value, index)
                }
              />
              {!disabled && (
                <Button
                  action={() => removeOrClearFirstField(index)}
                  presets={['icon', 'small']}
                >
                  close
                </Button>
              )}
            </div>
          );
        })
      ) : (
        <MuiTextField
          value={value}
          placeholder={placeholder}
          name={name}
          disabled={disabled}
          onChange={(event: CaseAttributeFieldChangeEventType) =>
            updateField(event.target.value)
          }
        />
      )}
      {multiValue && !disabled && (
        <Button action={addField}>{t('addNewField')}</Button>
      )}
    </React.Fragment>
  );
};

export default TextField;
