import React, { useState } from 'react';
import { stories, boolean } from '@mintlab/ui/App/story';
import TextField from './TextField';

stories(module, __dirname, {
  TextFieldMultiple() {
    const [value, setValue] = useState(['xyz', 'abc']);

    return (
      <TextField
        multiValue={true}
        disabled={boolean('disabled', false)}
        value={value}
        onChange={event => setValue(event.target.value)}
      />
    );
  },
  TextFieldSingle() {
    const [value, setValue] = useState('xyz');

    return (
      <TextField
        multiValue={false}
        disabled={boolean('disabled', false)}
        value={value}
        onChange={event => setValue(event.target.value)}
      />
    );
  },
});
