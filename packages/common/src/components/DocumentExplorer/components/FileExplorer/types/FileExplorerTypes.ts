import {
  ColumnType,
  RowType,
} from '@mintlab/ui/App/Zaaksysteem/SortableTable/types/SortableTableTypes';

export type FileExplorerPropsType = {
  items: CommonItemType[];
  columns: ColumnType[];
  onFolderOpen: (folder: DirectoryItemType) => void;
  onFileOpen: (file: DocumentItemType) => void;
  loading?: boolean;
  noRowsMessage: string;
  [key: string]: any;
};

export type RowClickEvent = {
  event: React.MouseEvent;
  rowData: DocumentItemType | DirectoryItemType;
};

export interface CommonItemType extends RowType {
  modified?: Date;
  type: string;
}

export interface DocumentItemType extends CommonItemType {
  type: 'document';
  mimeType: string;
  extension?: string;
  pending?: boolean;
  description?: string;
  accepted: boolean;
  beingVirusScanned: boolean;
  selected: boolean;
  preview?: string;
  download?: string;
}

export interface DirectoryItemType extends CommonItemType {
  type: 'directory';
}

export type ItemType = DocumentItemType | DirectoryItemType;
