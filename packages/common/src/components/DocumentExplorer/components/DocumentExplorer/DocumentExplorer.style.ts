import { makeStyles } from '@material-ui/core';

export const useDocumentExplorerStyles = makeStyles(
  ({ palette: { basalt, primary }, mintlab: { greyscale } }: any) => {
    const section = {
      borderBottom: `1px solid ${greyscale.dark}`,
      alignItems: 'center',
      paddingTop: 10,
      paddingBottom: 10,
    };

    return {
      wrapper: {
        width: '100%',
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
      },
      search: {
        ...section,
        flex: 0,
      },
      table: {
        flex: 1,
        overflow: 'auto',
      },
      toolbar: {
        ...section,
        display: 'flex',
        flex: 0,
        height: 60,
        paddingLeft: 20,
        justifyContent: 'flex-end',
      },
      actionButtons: {
        display: 'flex',
        flexGrow: 1,
        justifyContent: 'flex-end',
      },
      iconWrapper: {
        marginRight: 10,
      },
      nameCell: {
        display: 'flex',
        overflow: 'visible',
        alignItems: 'center',
      },
      metaCell: {
        color: basalt.lightest,
      },
      actionButtonLabel: {
        fontSize: 17,
      },
      notAcceptedLabel: {
        color: basalt.lightest,
      },
      link: {
        color: primary.main,
        textDecoration: 'underline',
      },
      breadCrumbs: {
        flex: 1,
        display: 'flex',
        alignItems: 'center',
      },
    };
  }
);
