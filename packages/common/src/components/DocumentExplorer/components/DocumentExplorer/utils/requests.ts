import i18next from 'i18next';
import { APIDocument } from '@zaaksysteem/generated';
import { request } from '@zaaksysteem/common/src/library/request/request';
import {
  BreadCrumbItemType,
  PresetLocations,
  ValidLocation,
} from '../types/types';
import { ItemType } from '../../FileExplorer/types/FileExplorerTypes';

type FetchDataType = {
  items: ItemType[];
  path: BreadCrumbItemType[];
};
type IncludedType = APIDocument.GetDirectoryEntriesForCaseResponseBody['included'];
type DataType = APIDocument.GetDirectoryEntriesForCaseResponseBody['data'];

export const fetchData = async ({
  url,
  location,
  t,
}: {
  location: ValidLocation;
  search?: string;
  t: i18next.TFunction;
  url: string;
}): Promise<FetchDataType> => {
  const { data, included } = await request<{
    data: DataType;
    included: IncludedType;
  }>('GET', url);

  const items = data
    ? data.map(
        ({
          attributes: {
            name,
            entry_type,
            last_modified_date_time,
            description,
            mimetype,
            accepted,
          },
          links: { download, preview },
          id,
        }: APIDocument.GetDirectoryEntriesForCaseResponseBody): ItemType => {
          return entry_type === 'document'
            ? {
                uuid: id,
                name,
                type: 'document',
                modified: last_modified_date_time,
                description,
                mimeType: mimetype,
                beingVirusScanned: false,
                selected: false,
                accepted,
                download,
                preview,
              }
            : { uuid: id, name, type: 'directory' };
        }
      )
    : [];

  const path = buildPath({ t, location, included });

  return {
    items,
    path,
  };
};

const buildPath = ({
  t,
  location,
  included,
}: {
  t: i18next.TFunction;
  location: ValidLocation;
  included: IncludedType;
}): BreadCrumbItemType[] => {
  const path: BreadCrumbItemType[] = [
    { id: PresetLocations.Home, label: t('DocumentExplorer:breadCrumb.home') },
  ];

  if (location === PresetLocations.Home) return path;

  if (location === PresetLocations.Search) {
    path.push({
      id: PresetLocations.Search,
      label: t('DocumentExplorer:breadCrumb.search'),
    });
  } else if (included && included.length) {
    const start = included.find(thisFind => thisFind.id === location);
    if (!start) return path;

    const includedInOrder = included.reduce(
      acc => {
        const [current] = acc;
        const parent = included.find(
          thisFind => current?.relationships?.parent?.data?.id === thisFind.id
        );

        return parent ? [parent, ...acc] : acc;
      },
      [start]
    );

    path.push(
      ...includedInOrder.map(thisIncluded => {
        return {
          id: thisIncluded.id,
          label: thisIncluded.attributes.name,
        };
      })
    );
  }
  return path;
};
