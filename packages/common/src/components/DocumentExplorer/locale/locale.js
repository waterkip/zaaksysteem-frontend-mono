const locale = {
  nl: {
    columns: {
      modified: {
        label: 'Gewijzigd',
      },
      download: {
        label: '',
        toolTip: 'Download bestand',
      },
      name: {
        label: 'Naam',
        notAccepted: '(in behandeling)',
      },
      description: {
        label: 'Omschrijving',
      },
      type: {
        label: 'Type',
      },
    },
    search: {
      placeholder: 'Zoek een bestand of map...',
    },
    addFiles: {
      button: 'Toevoegen',
    },
    breadCrumb: {
      home: 'Bestanden',
      search: 'Zoekresultaten',
    },
    table: {
      noRows: 'Er zijn geen bestanden of mappen om weer te geven.',
    },
    fileTypes: {
      default: 'Bestand',
      folder: 'Map',
      image: 'Afbeelding',
      spreadsheet: 'Spreadsheet',
      document: 'Document',
      presentation: 'Presentatie',
      pdf: 'PDF-Document',
    },
  },
};

export default locale;
