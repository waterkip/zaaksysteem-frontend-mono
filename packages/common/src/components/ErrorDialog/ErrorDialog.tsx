import React from 'react';
// @ts-ignore
import Alert from '../dialogs/Alert/Alert';

export interface ErrorDialogPropsType {
  errorCode: string;
  onClose?: () => void;
  title: string;
  body: string;
  primaryButtonText: string;
  open: boolean;
  [key: string]: any;
}

const ErrorDialog: React.FunctionComponent<ErrorDialogPropsType> = ({
  errorCode,
  onClose,
  title,
  body,
  primaryButtonText,
  open = true,
}) => (
  <Alert
    primaryButton={{ text: primaryButtonText, action: onClose }}
    key={errorCode}
    onClose={onClose}
    title={title}
    open={open}
  >
    {body}
  </Alert>
);

export default ErrorDialog;
