const test = require('tape');
const {
  cloneWithout,
  purge,
  extract,
  get,
  filterProperties,
  filterPropertiesOnValues,
  performGetOnProperties,
} = require('./object');

const { assign } = Object;

const sourceObject = () => ({
  foo: 'FOO',
  bar: 'BAR',
  quux: 'QUUX',
});

// cloneWithout
test('cloneWithout()', assert => {
  {
    const actual = cloneWithout(sourceObject(), 'foo');
    const expected = {
      bar: 'BAR',
      quux: 'QUUX',
    };
    const message = 'can clone an object with one excluded property';

    assert.deepEqual(actual, expected, message);
  }
  {
    const actual = cloneWithout(sourceObject(), 'foo', 'quux');
    const expected = {
      bar: 'BAR',
    };
    const message = 'can clone an object with multiple excluded properties';

    assert.deepEqual(actual, expected, message);
  }
  assert.end();
});

// purge
test('purge()', assert => {
  const clone = assign({}, sourceObject());
  const actual = purge('foo', clone, 'quux');
  const expected = {
    bar: 'BAR',
  };

  const message =
    'accepts its arguments in any order and returns an object without the supplied properties';

  assert.deepEqual(actual, expected, message);
  assert.end();
});

// extract
test('extract()', assert => {
  {
    const actual = extract('bar', sourceObject());
    const expected = [
      'BAR',
      {
        foo: 'FOO',
        quux: 'QUUX',
      },
    ];

    const message =
      'can extract a single property with the key as first argument';

    assert.deepEqual(actual, expected, message);
  }
  {
    const actual = extract(sourceObject(), 'bar');
    const expected = [
      {
        foo: 'FOO',
        quux: 'QUUX',
      },
      'BAR',
    ];

    const message =
      'an extract a single property with the key as last argument';

    assert.deepEqual(actual, expected, message);
  }
  {
    const actual = extract('quux', sourceObject(), 'foo');
    const expected = [
      'QUUX',
      {
        bar: 'BAR',
      },
      'FOO',
    ];

    const message = 'can extract multiple properties in any argument order';

    assert.deepEqual(actual, expected, message);
  }
  {
    const func = () => {
      extract('foo', 'bar');
    };

    const message = 'throws if no argument is an object';
    assert.throws(func, null, message);
  }
  {
    const func = () => {
      extract('quux', sourceObject(), 'foo', sourceObject());
    };

    const message = 'throws if more than one argument is an object';
    assert.throws(func, null, message);
  }

  assert.end();
});

// get
test('get()', assert => {
  {
    const actual = get(sourceObject(), 'foo');
    const expected = 'FOO';

    const message = 'returns the value of an object property';

    assert.deepEqual(actual, expected, message);
  }
  {
    const object = {
      foo: {
        bar: 'quux',
      },
    };
    const actual = get(object, 'foo.bar');
    const expected = 'quux';

    const message = 'returns the value of a deep object property';

    assert.deepEqual(actual, expected, message);
  }
  {
    const object = {
      foo: {
        bar: 'quux',
      },
    };
    const actual = get(object, 'foo[bar]');
    const expected = 'quux';

    const message = 'supports bracket property accessors';

    assert.deepEqual(actual, expected, message);
  }
  {
    const object = {};
    const actual = get(object, 'nil');
    let expected;

    const message =
      'returns `undefined` for an object property that does not exist';

    assert.deepEqual(actual, expected, message);
  }
  {
    const object = {};
    const actual = get(object, 'foo.bar');
    let expected;

    const message =
      'returns `undefined` for a deep object property that does not exist';

    assert.deepEqual(actual, expected, message);
  }

  assert.end();
});

// performGetOnProperties
test('performGetOnProperties()', assert => {
  const getters = {
    one: 'one',
    two: 'two.second',
    three: 'three.third.tripple',
  };
  const object = {
    one: 'first',
    two: {
      second: 'double',
    },
    three: {
      third: {
        tripple: 'trio',
      },
    },
  };
  const actual = performGetOnProperties(object, getters);
  const expected = {
    one: 'first',
    two: 'double',
    three: 'trio',
  };
  const message = 'can perform a get on all the properties';

  assert.deepEqual(actual, expected, message);
  assert.end();
});

//filterProperties
test('filterProperties()', assert => {
  {
    const clone = assign({}, sourceObject());
    const actual = filterProperties(clone, 'foo', 'bar');
    const expected = {
      foo: 'FOO',
      bar: 'BAR',
    };
    const message = 'returns the object with only the given properties';

    assert.deepEqual(actual, expected, message);
  }
  {
    const clone = assign({}, sourceObject());
    const actual = filterProperties(clone, 'foo', 'bar', 'mismatch');
    const expected = {
      foo: 'FOO',
      bar: 'BAR',
    };
    const message =
      'returns the object with only the given properties ignoring non-matching properties';

    assert.deepEqual(actual, expected, message);
  }
  {
    const clone = assign({}, sourceObject());
    const actual = filterProperties(clone, 'mismatch', 'different');
    const expected = {};
    const message =
      'and returns an empty object when none of the properties match';

    assert.deepEqual(actual, expected, message);
  }

  assert.end();
});

//filterPropertiesOnValues
test('filterPropertiesOnValues()', assert => {
  {
    const clone = assign({}, sourceObject());
    const actual = filterPropertiesOnValues(clone, value => value !== 'FOO');
    const expected = {
      bar: 'BAR',
      quux: 'QUUX',
    };
    const message =
      'and returns an object without the values for which the given function does not return true';

    assert.deepEqual(actual, expected, message);
  }
  {
    const clone = assign({}, sourceObject());
    const actual = filterPropertiesOnValues(
      clone,
      value => value !== 'mismatch'
    );
    const expected = {
      foo: 'FOO',
      bar: 'BAR',
      quux: 'QUUX',
    };
    const message =
      'and returns an object without the values for which the given function does not return true';

    assert.deepEqual(actual, expected, message);
  }

  assert.end();
});
