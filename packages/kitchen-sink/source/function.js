/**
 * Call a value with optional arguments if it is a function.
 *
 * @param {*} callback
 *   A value that is called if it is a function.
 * @param {Array|Function} [argumentList=[]]
 *   The optional arguments for the callback.
 * @returns {*}
 *   The return value of the function if it is called or undefined.
 */
export function callOrNothingAtAll(callback, argumentList = []) {
  if (typeof callback === 'function') {
    return callback(
      ...(typeof argumentList === 'function' ? argumentList() : argumentList)
    );
  }
}
