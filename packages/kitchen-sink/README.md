# @mintlab/kitchen-sink  [![npm version][npm-image]][npm-url]

> Everything but: miscellaneous Mintlab utility functions.

## Installation

    $ npm install @mintlab/kitchen-sink

## Usage

### Examples

Public API entry point:

    const kitchenSink = require('@mintlab/kitchen-sink');

Destructuring:

    const { dictionary } = require('@mintlab/kitchen-sink');

Single module:

    const dictionary = require('@mintlab/kitchen-sink/dictionary');

## Development

### Docker

Start an interactive `ash` shell for development:

    $ docker-compose exec sink sh

If you edit `.*rc` files in `./root`, you must manually copy 
them to your current working directory in the container.

The `package*.json` files are symlinked because they can be
mutated both on the host and in the container.

### Publication

The package is published with
[semantic-release-gitlab](https://www.npmjs.com/package/semantic-release-gitlab)
on every merge to the master branch. Mind your commit messages.

Files in `./source` are copied to the root and removed after publication.
See the `prepublishOnly` and `postpublish` lifecycle scripts in `package.json`.

#### Manual emergency publication

In edge cases when semantic release cannot increment:

- if you are not in the container, copy `root/.npmrc` 
  and `root/package.json` to the working copy root
- update the `version` field in `package.json`
- run `npm login` if needed
- run `npm publish --access public`
- create a matching version tag in gitlab
- do **not** commit your changes

### Infrastructure

The modules in this package export *Node.js modules* and 
work natively on the server. There is no transpilation and 
distribution build included, to consume them in the browser
use your bundler of choice.

#### See also

- [Node.js ECMAScript support](https://node.green/)

### Unit testing

Unit tests use
[tape](https://github.com/substack/tape), a 
[TAP](https://testanything.org) 
producing test harness for node.

#### Async

No special libraries or plugins are needed for async tests:

1. make the test callback `async`
2. `plan` the number of assertions
3. `await` the async value
4. proceed as with any other test

##### Example

    test('The async answer', async assert => {
      assert.plan(1);
    
      const answer = await new Promise(resolve => {
        setTimeout(() => resolve(42), 1000);
      });
    
      assert.equal(answer, 42, 'is always 42');
      assert.end();
    });

## License

Copyright 2017 Mintlab B.V.

Licensed under the EUPL, Version 1.1 or – as soon they will be approved by the European Commission - 
subsequent versions of the EUPL (the "Licence").

You may not use this work except in compliance with the Licence.

You may obtain a copy of the Licence at: 
[https://joinup.ec.europa.eu/software/page/eupl](https://joinup.ec.europa.eu/software/page/eupl)

[npm-image]: https://img.shields.io/npm/v/@mintlab/kitchen-sink.svg?style=flat-square
[npm-url]: https://www.npmjs.com/package/@mintlab/kitchen-sink
